:: cbsubgen workshop configuration

:: In case you break something, the default configuration file is available in workshop-config.bat.default

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set CONFIG_xxx=value is good
:: set CONFIG_xxx = value is NOT good

:: Available constants :
:: %WORKSHOP_DIR% : Directory of the cbsubgen workshop, without the trailing antislash (ex : c:\path\cbsubgen\workshop)
:: %WORKSHOP_CONFIG_DIR% : Directory of the workshop configuration file (this file), without the trailing antislash (ex : c:\users\username\Documents\subliminals\subname)
:: + all default Windows constants

::::::::::::
:: Layers ::
::::::::::::

:: Affirmation file of the first affirmations layer.
:: It can either be a text file (.txt), or an audio file (.wav or .mp3).
:: Example : file.txt
:: Default value : %WORKSHOP_CONFIG_DIR%\affirmations01.txt
set CONFIG_aff1=%WORKSHOP_CONFIG_DIR%\affirmations01.txt

:: Voice of the first affirmations layer.
:: Either the name of a voice installed on your computer, find it under your Windows TTS settings.
:: Example : Zira
:: Either an online voice written as bal4web:<service>:<language>:<gender>:<voicename>
:: Services are google, microsoft and amazon.
:: Languages are under the form en-US, en-GB, fr-FR, fr-CA, etc.
:: Genders are male and female.
:: Some values can be omitted, do not write anything between the colons (:).
:: Example : bal4web:google:en-US:female
:: Example : bal4web:microsoft:en-US:female:JennyNeural
:: Example : bal4web:amazon:en-GB:male:Brian
:: Use the list-local-voices.bat and list-bal4web-voices.bat scripts in the workshop folder to get a list of the voices available
:: Default value : (empty, use system default)
set CONFIG_affvoice1=

:: Voice speed multiplier of the first affirmations layer.
:: It changes both the speed and the pitch of the voice.
:: Example : 2
:: Default value : (empty, means no change)
set CONFIG_affspeed1=

:: Voice tempo multiplier of the first affirmations layer.
:: It changes the speed but keeps the pitch of the voice.
:: Example : 2
:: Default value : 1.3
set CONFIG_afftempo1=1.3

:: Voice pitch of the first affirmations layer.
:: Can be a positive or negative number, in that case it represents the number of 1/100th of semitones that get added or removed.
:: Can also be auto, in that case it will be automatically calculated so multiple layers with the same voice have different pitches.
:: Can also be auto followed by a number (ex : auto100), in that case it will be automatically calculated with the given interval between pitches.
:: Example : 400 (add 4 semitones)
:: Example : auto (automatically adds and removes semitones if needed)
:: Example : auto200 (automatically adds and removes semitones if needed, by interval of 2 semitones)
:: Default value : auto
set CONFIG_affpitch1=auto

:: Frequency range of the bandpass filter of the first affirmations layer.
:: Must be under the form lowfreq highfreq, for example 3500 10000 for a filter that only keeps audio in the 3500hz-10000hz range.
:: Values range from 0 to 24000.
:: Example : 1000 5000
:: Default value : (empty, means no change)
set CONFIG_affbandpass1=

:: Frequency range of the bandreject filter of the first affirmations layer
:: Must be under the form lowfreq highfreq, for example 12000 24000 for a filter that deletes audio in the 12000hz-24000hz range.
:: Values range from 0 to 24000.
:: Example : 1000 5000
:: Default value : (empty, means no change)
set CONFIG_affbandreject1=

:: Voice normalization (volume from 0 to -100) of the first affirmations layer.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of CONFIG_normwith.
:: Example : -50
:: Default value : -60
set CONFIG_affnorm1=-60

:: Number of times the affirmations of the first layer will be repeated.
:: Example : 3
:: Default value : (empty, means 0)
set CONFIG_affloop1=

:: Carrier frequency (hz) used if the first layer is ultrasonic.
:: Must be between 15000 and 20000.
:: Leave empty or set to 0 if it is not an ultrasonic layer.
:: Example : 17500
:: Default value : (empty, means non-ultrasonic layer)
set CONFIG_affultrasonic1=

:: Name identifying the group whose the first layer belongs to.
:: Layers in the same group are panned together (if /pan is provided), and inherit the settings of the first layer of the group if they aren't provided.
:: Leave empty to use the default group.
:: Example : groupname
:: Default value : (empty, means the default group)
set CONFIG_affgroup1=

:: Settings for the 2nd affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff2=%WORKSHOP_CONFIG_DIR%\affirmations02.txt
set CONFIG_affvoice2=
set CONFIG_affspeed2=
set CONFIG_afftempo2=
set CONFIG_affpitch2=
set CONFIG_affbandpass2=
set CONFIG_affbandreject2=
set CONFIG_affnorm2=
set CONFIG_affloop2=
set CONFIG_affultrasonic2=
set CONFIG_affgroup2=

:: Settings for the 3rd affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff3=%WORKSHOP_CONFIG_DIR%\affirmations03.txt
set CONFIG_affvoice3=
set CONFIG_affspeed3=
set CONFIG_afftempo3=
set CONFIG_affpitch3=
set CONFIG_affbandpass3=
set CONFIG_affbandreject3=
set CONFIG_affnorm3=
set CONFIG_affloop3=
set CONFIG_affultrasonic3=
set CONFIG_affgroup3=

:: Settings for the 4th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff4=%WORKSHOP_CONFIG_DIR%\affirmations04.txt
set CONFIG_affvoice4=
set CONFIG_affspeed4=
set CONFIG_afftempo4=
set CONFIG_affpitch4=
set CONFIG_affbandpass4=
set CONFIG_affbandreject4=
set CONFIG_affnorm4=
set CONFIG_affloop4=
set CONFIG_affultrasonic4=
set CONFIG_affgroup4=

:: Settings for the 5th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff5=%WORKSHOP_CONFIG_DIR%\affirmations05.txt
set CONFIG_affvoice5=
set CONFIG_affspeed5=
set CONFIG_afftempo5=
set CONFIG_affpitch5=
set CONFIG_affbandpass5=
set CONFIG_affbandreject5=
set CONFIG_affnorm5=
set CONFIG_affloop5=
set CONFIG_affultrasonic5=
set CONFIG_affgroup5=

:: Settings for the 6th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff6=%WORKSHOP_CONFIG_DIR%\affirmations06.txt
set CONFIG_affvoice6=
set CONFIG_affspeed6=
set CONFIG_afftempo6=
set CONFIG_affpitch6=
set CONFIG_affbandpass6=
set CONFIG_affbandreject6=
set CONFIG_affnorm6=
set CONFIG_affloop6=
set CONFIG_affultrasonic6=
set CONFIG_affgroup6=

:: Settings for the 7th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff7=%WORKSHOP_CONFIG_DIR%\affirmations07.txt
set CONFIG_affvoice7=
set CONFIG_affspeed7=
set CONFIG_afftempo7=
set CONFIG_affpitch7=
set CONFIG_affbandpass7=
set CONFIG_affbandreject7=
set CONFIG_affnorm7=
set CONFIG_affloop7=
set CONFIG_affultrasonic7=
set CONFIG_affgroup7=

:: Settings for the 8th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff8=%WORKSHOP_CONFIG_DIR%\affirmations08.txt
set CONFIG_affvoice8=
set CONFIG_affspeed8=
set CONFIG_afftempo8=
set CONFIG_affpitch8=
set CONFIG_affbandpass8=
set CONFIG_affbandreject8=
set CONFIG_affnorm8=
set CONFIG_affloop8=
set CONFIG_affultrasonic8=
set CONFIG_affgroup8=

:: Settings for the 9th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff9=%WORKSHOP_CONFIG_DIR%\affirmations09.txt
set CONFIG_affvoice9=
set CONFIG_affspeed9=
set CONFIG_afftempo9=
set CONFIG_affpitch9=
set CONFIG_affbandpass9=
set CONFIG_affbandreject9=
set CONFIG_affnorm9=
set CONFIG_affloop9=
set CONFIG_affultrasonic9=
set CONFIG_affgroup9=

:: Settings for the 10th affirmations layer. Refer to the first layer settings for informations.
:: Except from the affirmations themselves, they will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty.
set CONFIG_aff10=%WORKSHOP_CONFIG_DIR%\affirmations10.txt
set CONFIG_affvoice10=
set CONFIG_affspeed10=
set CONFIG_afftempo10=
set CONFIG_affpitch10=
set CONFIG_affbandpass10=
set CONFIG_affbandreject10=
set CONFIG_affnorm10=
set CONFIG_affloop10=
set CONFIG_affultrasonic10=
set CONFIG_affgroup10=

:: The software supports up to 50 layers
:: For the sake of keeping the configuration file small enough, not all the settings for every possible layer were included
:: Feel free to duplicate and rename the existing settings to add more layers (CONFIG_aff11 etc)

:::::::::::::::
:: Frequency ::
:::::::::::::::

:: Frequency (hz) to be generated in the left channel.
:: Example : 100
:: Default value : (empty, means none)
set CONFIG_leftfreq=

:: Frequency (hz) to be generated in the right channel.
:: Example : 100
:: Default value : (empty, fallbacks to the left channel settings)
set CONFIG_rightfreq=

:: Normalization (volume from 0 to -100) of the frequency.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of CONFIG_normwith.
:: Example : -20
:: Default value : -20
set CONFIG_freqnorm=-20

:: Duration (s) of a pulse in the case of an intermittent frequency (isochronic tone).
:: Example : 2
:: Default value : (empty, means non-intermittent frequency)
set CONFIG_freqpulse=

:: Gap (s) between pulses in the case of an intermittent frequency (isochronic tone).
:: Example : 1
:: Default value : (empty, means non-intermittent frequency)
set CONFIG_freqgap=

:: Fade duration (s) applied to the beginning and the end of the frequency or of each pulse.
:: Example : 0.5
:: Default value : (empty, means no fade)
set CONFIG_freqfade=

:: Speed of the stereo rotation effect applied to the frequency.
:: Allowed values : normal|slow|fast
:: Default value : (empty, means no rotation effect)
set CONFIG_freqrotation=

:::::::::::
:: Noise ::
:::::::::::

:: Noise type to be generated.
:: A value of white will generate white noise.
:: A value of pink will generate pink noise.
:: A value of brown will generate brown noise.
:: Allowed values : white|pink|brown
:: Default value : (empty, means do not generate noise)
set CONFIG_noise=

:: Normalization (volume from 0 to -100) of the generated noise.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Example : -20
:: Default value : -20
set CONFIG_noisenorm=-20

:: Duration (seconds) for the fade effect at the beginning and the end of the generated noise.
:: Example : 3
:: Default value : (empty, which means no fade)
set CONFIG_noisefade=

::::::::::::::::
:: Background ::
::::::::::::::::

:: Background music file.
:: Example : file.wav
:: Default value : %WORKSHOP_CONFIG_DIR%\background.mp3
set CONFIG_background=%WORKSHOP_CONFIG_DIR%\background.mp3

:: Normalization (volume from 0 to -100) of the background music.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of CONFIG_normwith.
:: Example : -30
:: Default value : (empty, means no normalization)
set CONFIG_backgroundnorm=

:: Duration (seconds) for the fade effect at the beginning and the end of the background music.
:: Example : 3
:: Default value : (empty, which means no fade)
set CONFIG_fade=

:: Enable fade when the background music is looped ? Any value different than 0 means yes.
:: If empty, there will be no fade on loop.
:: Example : 1
:: Default value : (empty, which means do not fade on loop)
set CONFIG_fadeloop=

:: Speed of the stereo rotation effect applied to the background.
:: Allowed values : normal|slow|fast
:: Default value : (empty, means no rotation effect)
set CONFIG_backgroundrotation=

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Set how to loop the affirmations of the shorter layers so they are as long as the duration given in CONFIG_autofillduration.
:: A value of simple will use a very fast method which may cut in the middle of an affirmation
:: A value of safe (the default) will use a fast method which will not cut but may miss at most one repeat
:: A value of silencedetect will use a slower experimental silence detection method which will not cut and will not miss any repeat
:: Note that if a layer is based on an audio file instead of a text file, it will use the simple or the safe method.
:: Allowed values : simple|safe|silencedetect
:: Default value : (empty, which is the same as safe)
set CONFIG_autofill=

:: Duration (seconds) for the autofill function.
:: The layers shorter than the given duration will be looped so they are nearly as long.
:: If empty, the duration will be calculated from the longest layer.
:: Example : 120
:: Default value : (empty, which means the duration of the longest layer)
set CONFIG_autofillduration=

:: Add some silence before and after the affirmations. The first duration (seconds) is the silence before, and the other is the silence after. If the other is missing it will be the same as the first.
:: If empty, there will be no padding added.
:: Example : 5
:: Example : 5 0
:: Default value : (empty, which means no padding)
set CONFIG_pad=

:: Enable panning the layers from left to right so they do not overlap each other.
:: A value of panlaw-0 will use a neutral pan law that does not reduce the volume in the center.
:: A value of panlaw-3 will use a constant power pan law that reduces by 3dB the volume in the center.
:: A value of panlaw-4.5 will use a middleground pan law that reduces by 4.5dB the volume in the center.
:: A value of panlaw-6 will use a linear pan law that reduces by 6dB the volume in the center.
:: A value of rotation will use a stereo rotation effect.
:: A value of rotation-slow will use a slower stereo rotation effect.
:: A value of rotation-fast will use a faster stereo rotation effect.
:: If empty there will be no panning performed.
:: Allowed values : panlaw-0|panlaw-3|panlaw-4.5|panlaw-6|rotation|rotation-slow|rotation-fast
:: Default value : panlaw-0
set CONFIG_pan=panlaw-0

:: Set the method used to normalize the audio.
:: A value of peak will use peak normalization. The normalization values (in decibels full-scale dBFS) will not be representative of the overall level or loudness (quiet sound, no risk of clipping).
:: A value of rms will use RMS normalization. The normalization values (in decibels full-scale dBFS) will be representative of the overall level but not of the perceived loudness (beware of clipping).
:: A value of r128 (the default) will use loudness normalization. The normalization values (in loudness units full-scale LUFS) will be representative of the overall perceived loudness (beware of clipping).
:: Allowed values : peak|rms|r128
:: Default value : (empty, which is the same as r128)
set CONFIG_normwith=

:: Set the width used in the bandpass and bandreject filters (see CONFIG_affbandpass1, CONFIG_affbandreject1...)
:: Ranges from 1 to 10000, with 1 being very steep (cutoff-like) and 100000 a soft roll-off.
:: Example : 1
:: Default value : 1
set CONFIG_bandfilterwidth=1

:: Proxy host for online voices (bal4web), useful if you get rate limited
:: Example : 127.0.0.1
:: Default value : (empty, means no proxy)
set CONFIG_proxyhost=

:: Proxy port for online voices (bal4web), useful if you get rate limited
:: Example : 8080
:: Default value : (empty, means no proxy)
set CONFIG_proxyport=

:: Enable displaying audio statistics during the generation ? Any value different than 0 means yes.
:: Those are the duration, peak, RMS and R128 loudness of the files.
:: If empty, there will be no statistic displayed.
:: Example : 1
:: Default value : 1
set CONFIG_showstats=1

::::::::::::::::::
:: Audio output ::
::::::::::::::::::

:: Destination audio file containing both background and affirmations.
:: Example : subliminal.wav
:: Default value : %WORKSHOP_CONFIG_DIR%\subliminal-complete.wav
set CONFIG_outputfile_complete=%WORKSHOP_CONFIG_DIR%\subliminal-complete.wav

:: Destination audio file containing affirmations only.
:: Example : affsonly.wav
:: Default value : %WORKSHOP_CONFIG_DIR%\subliminal-affsonly.wav
set CONFIG_outputfile_nobg=%WORKSHOP_CONFIG_DIR%\subliminal-affsonly.wav

:: Destination audio file containing affirmations ultrasonics.
:: Uncomment (remove the ::) the line containing "set CONFIG_outputfile_ultrasonic=" to enable this feature.
:: Example : ultrasonic.wav
:: Default value : %WORKSHOP_CONFIG_DIR%\subliminal-ultrasonic.wav
::set CONFIG_outputfile_ultrasonic=%WORKSHOP_CONFIG_DIR%\subliminal-ultrasonic.wav

:: Carrier frequency (hz) used in the affirmations ultrasonic subliminal.
:: Must be between 15000 and 20000.
:: Example : 15000
:: Default value : 17500
set CONFIG_outputfile_ultrasonic_freq=17500

:: Normalization (volume from 0 to -100) of the affirmations ultrasonic subliminal.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of CONFIG_normwith.
:: Example : -60
:: Default value : (empty, which means no change in normalization)
set CONFIG_outputfile_ultrasonic_norm=

:: Destination audio file for testing the affirmations audio.
:: One audio file will be generated per layer, whose name will be the one set here plus the layer number.
:: It makes it easier to ensure the affirmations are pronounced correctly.
:: Uncomment (remove the ::) the line containing "set CONFIG_outputfile_testaffs=" to enable this feature.
:: Example : testaffs.wav
:: Default value : %WORKSHOP_CONFIG_DIR%\test-affirmations.wav
::set CONFIG_outputfile_testaffs=%WORKSHOP_CONFIG_DIR%\test-affirmations.wav

:: Normalization (volume from 0 to -100) of the affirmations test audio files.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of CONFIG_normwith.
:: Example : -60
:: Default value : (empty, which means no change in normalization)
set CONFIG_outputfile_testaffs_norm=

::::::::::::::::::
:: Video output ::
::::::::::::::::::

:: Destination video file (MP4 only) containing both background and affirmations.
:: Example : subliminal.mp4
:: Default value : %WORKSHOP_CONFIG_DIR%\subliminal-complete.mp4
set CONFIG_outputvideo_complete=%WORKSHOP_CONFIG_DIR%\subliminal-complete.mp4

:: Image(s) used in the video file of the complete subliminal.
:: Either the path to an image file, or to a folder containing multiple images.
:: If empty, a default black 1080p image will be used.
:: Example : video.jpg
:: Default value : %WORKSHOP_CONFIG_DIR%\video-images
set CONFIG_outputvideo_complete_image=%WORKSHOP_CONFIG_DIR%\video-images

:: Destination video file (MP4 only) containing affirmations only.
:: Uncomment (remove the ::) the line containing "set CONFIG_outputvideo_nobg=" to enable this feature.
:: Example : subliminal-affsonly.mp4
:: Default value : %WORKSHOP_CONFIG_DIR%\subliminal-affsonly.mp4
::set CONFIG_outputvideo_nobg=%WORKSHOP_CONFIG_DIR%\subliminal-affsonly.mp4

:: Image(s) used in the video file of the affirmations only subliminal.
:: Either the path to an image file, or to a folder containing multiple images.
:: If empty, the image of the complete subliminal will be used.
:: Example : video.jpg
:: Default value : (empty, means use the image of the complete subliminal video)
set CONFIG_outputvideo_nobg_image=

:: Destination video file (MP4 only) containing affirmations ultrasonics.
:: Uncomment (remove the ::) the line containing "set CONFIG_outputvideo_ultrasonic=" to enable this feature.
:: Example : subliminal-ultrasonic.mp4
:: Default value : %WORKSHOP_CONFIG_DIR%\subliminal-ultrasonic.mp4
::set CONFIG_outputvideo_ultrasonic=%WORKSHOP_CONFIG_DIR%\subliminal-ultrasonic.mp4

:: Image(s) used in the video file of the ultrasonic subliminal.
:: Either the path to an image file, or to a folder containing multiple images.
:: If empty, the image of the complete subliminal will be used.
:: Example : video.jpg
:: Default value : (empty, means use the image of the complete subliminal video)
set CONFIG_outputvideo_ultrasonic_image=

:: Set the size of the video.
:: A value of auto (the default) will give it a height and an aspect ratio based on the source image.
:: A value of image will give it a target size equal to that of the source image.
:: A value of 240p will give it a target size of 426x240.
:: A value of 360p will give it a target size of 640x360.
:: A value of 480p will give it a target size of 854x480.
:: A value of 720p will give it a target size of 1280x720.
:: A value of 1080p will give it a target size of 1920x1080.
:: A value of 1440p will give it a target size of 2560x1440.
:: A value of 2160p will give it a target size of 3840x2160.
:: Allowed values : auto|image|240p|360p|480p|720p|1080p|1440p|2160p
:: Default value : (empty, which is the same as auto)
set CONFIG_videosize=

:: Set the sizing mode of the video.
:: A value of auto (the default) will give it an aspect ratio similar to the source image.
:: A value of contain will fit it into the target aspect ratio and add black bars if necessary.
:: A value of cover will crop it into the target aspect ratio, potentially causing parts of the side to be left out.
:: A value of stretch will stretch it to the target aspect ratio.
:: Allowed values : auto|contain|cover|stretch
:: Default value : (empty, which is the same as auto)
set CONFIG_videosizemode=

:: Set the audio encoding of the video.
:: A value of aac (the default and web standard) will use AAC CBR 320 audio (lossy compression, compatible with everything).
:: A value of flac will use FLAC audio (lossless compression, good compatibility).
:: A value of alac will use ALAC audio (lossless compression, compatible with YouTube and VLC).
:: Allowed values : aac|alac|flac
:: Default value : (empty, which is the same as aac)
set CONFIG_videoaudioencoding=

:: Set the delay (in seconds) before moving to the next image in videos.
:: Must be greater than 0.02.
:: Example : 1
:: Default value : 5
set CONFIG_slideshowdelay=5

::::::::::::::
:: Internal ::
::::::::::::::

:: Directory where cbsubgen will put temporary generated files.
:: Warning : the content of this folder will be DELETED on every run, so be careful
:: Example : path\to\directory
:: Default value : %WORKSHOP_CONFIG_DIR%\tmp
set CONFIG_workdir=%WORKSHOP_CONFIG_DIR%\tmp

:: Keep the content of the work directory after running ?
:: Any value different than 0 means yes.
:: If empty, the directory content will not be kept.
:: Example : 1
:: Default value : (empty, which means do not keep)
set CONFIG_keepworkdir=
