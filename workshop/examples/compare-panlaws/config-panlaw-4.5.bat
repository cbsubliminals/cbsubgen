:: cbsubgen workshop example configuration

:: This example allows you to compare the different panning methods using 5 layers and audible affirmations.

:: Example configuration files do not include all the settings available and their documentation.
:: They only mention those that are relevant to the example itself.
:: For the full configuration settings have a look at workshop-config.bat in the workshop folder.

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set CONFIG_xxx=value is good
:: set CONFIG_xxx = value is NOT good

::::::::::::
:: Layers ::
::::::::::::

:: L1
set CONFIG_aff1=%WORKSHOP_CONFIG_DIR%\affirmations01.txt
set CONFIG_afftempo1=1.3
set CONFIG_affpitch1=auto
set CONFIG_affnorm1=-30
set CONFIG_affloop1=19

:: L2
set CONFIG_aff2=%WORKSHOP_CONFIG_DIR%\affirmations02.txt

:: L3
set CONFIG_aff3=%WORKSHOP_CONFIG_DIR%\affirmations03.txt

:: L4
set CONFIG_aff4=%WORKSHOP_CONFIG_DIR%\affirmations04.txt

:: L5
set CONFIG_aff5=%WORKSHOP_CONFIG_DIR%\affirmations05.txt

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Enable left to right panning of the layers using a pan law that reduces the center by 4.5dB
set CONFIG_pan=panlaw-4.5

:: Display audio statistics while the sub is generated
set CONFIG_showstats=1

::::::::::::
:: Output ::
::::::::::::

:: Output the affirmations-only subliminal in the same folder as the config file
set CONFIG_outputfile_nobg=%WORKSHOP_CONFIG_DIR%\affs-panlaw-4.5.wav
