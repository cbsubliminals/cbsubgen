This example allows you to compare the different panning methods using 5 layers and audible affirmations.
Use headphones, and notice how :
- (stereo) The center volume is higher than the rest using the panlaw-0 method
- (stereo) The center volume is progressively lower using the methods panlaw-3, panlaw-4.5, panlaw-6
- (stereo) The unpanned method generates completely centered audio

If you can, try also playing the generated files in mono instead of stereo.
You'll notice that :
- (mono) The center volume is way higher than the rest using the panlaw-0 method
- (mono) The center volume is the same as the rest using the panlaw-6 method, and gets progressively higher with panlaw-4.5 and panlaw-3

What's the ideal pan method for you ?

Run the example by using run-example.bat.
