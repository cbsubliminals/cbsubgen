This example shows how to make a subliminal using binaural beats as the sole background.
It features 1 layer played 10 times (once and looped 9 times) with a background binaural beat.
Binaural beats are made by using a different frequency in each ear, the difference between the frequencies being the value of the beat.
For example, if one ear gets a 250hz frequency and the other a 200hz frequency then there is a 50hz binaural beat.

The relevant configuration can be found in workshop-config.bat in the example folder.

Run the example by using run-example.bat.
