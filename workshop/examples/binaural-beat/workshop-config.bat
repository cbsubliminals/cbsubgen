:: cbsubgen workshop example configuration

:: This example shows how to make a subliminal using binaural beats as the sole background.
:: It features 1 layer played 10 times (once and looped 9 times) with a background binaural beat.
:: Binaural beats are made by using a different frequency in each ear, the difference between the frequencies being the value of the beat.
:: For example, if one ear gets a 250hz frequency and the other a 200hz frequency then there is a 50hz binaural beat.

:: Example configuration files do not include all the settings available and their documentation.
:: They only mention those that are relevant to the example itself.
:: For the full configuration settings have a look at workshop-config.bat in the workshop folder.

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set CONFIG_xxx=value is good
:: set CONFIG_xxx = value is NOT good

::::::::::::
:: Layers ::
::::::::::::

:: L1 : Use the given text file in the same folder as the config file
set CONFIG_aff1=%WORKSHOP_CONFIG_DIR%\affirmations01.txt
:: L1 : Use default voice
set CONFIG_affvoice1=
:: L1 : Do not change the speed
set CONFIG_affspeed1=
:: L1 : Multiply the tempo by 1.3
set CONFIG_afftempo1=1.3
:: L1 : Let the tool automatically calculate the pitch in case multiple layers use this voice
set CONFIG_affpitch1=auto
:: L1 : Normalize with a loudness at -70LUFS
set CONFIG_affnorm1=-70
:: L1 : Loop 9 times, so the layer will be played 10 times
set CONFIG_affloop1=9
:: L1 : This isn't an ultrasonic layer
set CONFIG_affultrasonic1=

:::::::::::::::
:: Frequency ::
:::::::::::::::

:: Use a frequency of 250hz in the left ear
set CONFIG_leftfreq=250

:: Use a frequency of 200hz in the right ear
set CONFIG_rightfreq=200

:: Normalize the frequency with a loudness at -10LUFS (since CONFIG_normwith is r128)
set CONFIG_freqnorm=-10

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Display audio statistics while the sub is generated
set CONFIG_showstats=1

::::::::::::
:: Output ::
::::::::::::

:: Output the complete subliminal in the same folder as the config file
set CONFIG_outputfile_complete=%WORKSHOP_CONFIG_DIR%\subliminal-complete.wav

:: Output the affirmations-only subliminal in the same folder as the config file
set CONFIG_outputfile_nobg=%WORKSHOP_CONFIG_DIR%\subliminal-affsonly.wav
