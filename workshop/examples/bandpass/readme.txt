This example shows how to use the bandpass filter in a given layer.

The relevant configuration can be found in workshop-config.bat in the example folder.

Run the example by using run-example.bat.
