This example allows you to compare the different rotation panning methods using 3 layers and audible affirmations.
Use headphones, and notice how the speed varies depending on the pan setting.

If you can, try also playing the generated files in mono instead of stereo.
You'll notice that the overall volume of the affirmations increases a little and the rotation effect disappears.

Run the example by using run-example.bat.
