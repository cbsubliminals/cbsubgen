:: cbsubgen workshop example configuration

:: This example shows how to make a subliminal with 3 layers.
:: It features 3 layers played at least 10 times and panned from left to right with a background audio file.

:: Example configuration files do not include all the settings available and their documentation.
:: They only mention those that are relevant to the example itself.
:: For the full configuration settings have a look at workshop-config.bat in the workshop folder.

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set CONFIG_xxx=value is good
:: set CONFIG_xxx = value is NOT good

::::::::::::
:: Layers ::
::::::::::::

:: L1 : Use the given text file in the same folder as the config file
set CONFIG_aff1=%WORKSHOP_CONFIG_DIR%\affirmations01.txt
:: L1 : Use default voice
set CONFIG_affvoice1=
:: L1 : Do not change the speed
set CONFIG_affspeed1=
:: L1 : Multiply the tempo by 1.3
set CONFIG_afftempo1=1.3
:: L1 : Let the tool automatically calculate the pitch in case multiple layers use this voice
set CONFIG_affpitch1=auto
:: L1 : Normalize with a loudness at -60LUFS
set CONFIG_affnorm1=-60
:: L1 : Loop 9 times, so the layer will be played 10 times
set CONFIG_affloop1=9
:: L1 : This isn't an ultrasonic layer
set CONFIG_affultrasonic1=

:: L2 : Use the given text file in the same folder as the config file
set CONFIG_aff2=%WORKSHOP_CONFIG_DIR%\affirmations02.txt
:: L2 : CONFIG_affvoice2=(empty) inherited from L1
:: L2 : CONFIG_affspeed2=(empty) inherited from L1
:: L2 : Multiply the tempo by 2
set CONFIG_afftempo2=2
:: L2 : CONFIG_affpitch2=auto inherited from L1
:: L2 : CONFIG_affnorm2=-60 inherited from L1
:: L2 : CONFIG_affloop2=9 inherited from L1
:: L2 : CONFIG_affultrasonic2=(empty) inherited from L1

:: L3 : Use the given text file in the same folder as the config file
set CONFIG_aff3=%WORKSHOP_CONFIG_DIR%\affirmations03.txt
:: L3 : CONFIG_affvoice3=(empty) inherited from L1
:: L3 : Multiply the speed by 2
set CONFIG_affspeed3=2
:: L3 : Do not change the tempo (it'd have inherited 1.3 from L1 if we left it empty)
set CONFIG_afftempo3=1
:: L3 : CONFIG_affpitch3=auto inherited from L1
:: L3 : CONFIG_affnorm3=-60 inherited from L1
:: L3 : CONFIG_affloop3=9 inherited from L1
:: L3 : CONFIG_affultrasonic3=(empty) inherited from L1

::::::::::::::::
:: Background ::
::::::::::::::::

:: Use the given audio file in the same folder as the config file
set CONFIG_background=%WORKSHOP_CONFIG_DIR%\background.mp3

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Enable panning the layers from left to right in order to make them easier to distinguish
set CONFIG_pan=panlaw-0

:: Display audio statistics while the sub is generated
set CONFIG_showstats=1

::::::::::::
:: Output ::
::::::::::::

:: Output the complete subliminal in the same folder as the config file
set CONFIG_outputfile_complete=%WORKSHOP_CONFIG_DIR%\subliminal-complete.wav

:: Output the affirmations-only subliminal in the same folder as the config file
set CONFIG_outputfile_nobg=%WORKSHOP_CONFIG_DIR%\subliminal-affsonly.wav
