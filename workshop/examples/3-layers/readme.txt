This example shows how to make a subliminal with 3 layers.
It features 3 layers played at least 10 times and panned from left to right with a background audio file.

The relevant configuration can be found in workshop-config.bat in the example folder.

Run the example by using run-example.bat.
