@echo off
setlocal EnableDelayedExpansion

::::::::::::::::::::::::::::::::::::::
:: cbsubgen-workshop example runner ::
::::::::::::::::::::::::::::::::::::::

:: Script path
set EXAMPLE_DIR=%~dp0
if "%EXAMPLE_DIR:~-1%"=="\" set EXAMPLE_DIR=%EXAMPLE_DIR:~0,-1%

:: Detect double-click vs command line
set RUN_FROM_DOUBLE_CLICK=0
IF /I %0 EQU "%~dpnx0" set RUN_FROM_DOUBLE_CLICK=1

:: Executables path
set BAT_WORKSHOP=%EXAMPLE_DIR%\..\..\cbsubgen-workshop.bat

:: Run the workshop with the example configurations
call "%BAT_WORKSHOP%" "%EXAMPLE_DIR%\config-nonoise.bat"
call "%BAT_WORKSHOP%" "%EXAMPLE_DIR%\config-whitenoise.bat"
call "%BAT_WORKSHOP%" "%EXAMPLE_DIR%\config-pinknoise.bat"
call "%BAT_WORKSHOP%" "%EXAMPLE_DIR%\config-brownnoise.bat"

:: Pause if we are run from double click, so the user can look at the text
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
  echo:
)

:: That's all folks
goto :eof
