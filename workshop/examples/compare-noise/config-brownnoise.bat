:: cbsubgen workshop example configuration

:: This example allows you to compare the different noise types.

:: Example configuration files do not include all the settings available and their documentation.
:: They only mention those that are relevant to the example itself.
:: For the full configuration settings have a look at workshop-config.bat in the workshop folder.

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set CONFIG_xxx=value is good
:: set CONFIG_xxx = value is NOT good

::::::::::::
:: Layers ::
::::::::::::

:: L1
set CONFIG_aff1=%WORKSHOP_CONFIG_DIR%\affirmations01.txt
set CONFIG_afftempo1=1.3
set CONFIG_affpitch1=auto
set CONFIG_affnorm1=-60
set CONFIG_affloop1=9

::::::::::::::::::::
:: Noise settings ::
::::::::::::::::::::

:: Generate some noise to hide the affirmations
set CONFIG_noise=brown

:: Normalize the noise with a loudness at -20LUFS (since CONFIG_normwith is r128)
set CONFIG_noisenorm=-20

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Display audio statistics while the sub is generated
set CONFIG_showstats=1

::::::::::::
:: Output ::
::::::::::::

:: Output the complete subliminal in the same folder as the config file
set CONFIG_outputfile_complete=%WORKSHOP_CONFIG_DIR%\subliminal-brownnoise.wav
