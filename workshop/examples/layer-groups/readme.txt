This example shows how to use layer groups.
It features 9 layers :
- 3 are panned left  / center / right and are slightly sped using tempo (L1 to L3)
- 3 are panned left / center / right are are oversped using tempo (L4 to L6)
- 3 are panned left / center / right and are oversped using speed (L7 to L9)

The setup looks like this :
|   LEFT   |  CENTER  |  RIGHT   |
| L1+L4+L7   L2+L5+L8   L3+L6+L9 |

The relevant configuration can be found in workshop-config.bat in the example folder.

Run the example by using run-example.bat.
