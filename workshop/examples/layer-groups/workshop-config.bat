:: cbsubgen workshop example configuration

:: This example shows how to use layer groups.
:: It features 9 layers :
:: - 3 are panned left  / center / right and are slightly sped using tempo (L1 to L3)
:: - 3 are panned left / center / right are are oversped using tempo (L4 to L6)
:: - 3 are panned left / center / right and are oversped using speed (L7 to L9)

:: Example configuration files do not include all the settings available and their documentation.
:: They only mention those that are relevant to the example itself.
:: For the full configuration settings have a look at workshop-config.bat in the workshop folder.

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set CONFIG_xxx=value is good
:: set CONFIG_xxx = value is NOT good

::::::::::::
:: Layers ::
::::::::::::

:: L1 : Use the given text file in the same folder as the config file
set CONFIG_aff1=%WORKSHOP_CONFIG_DIR%\affirmations01.txt
:: L1 : Use default voice
set CONFIG_affvoice1=
:: L1 : Do not change the speed
set CONFIG_affspeed1=
:: L1 : Multiply the tempo by 1.3
set CONFIG_afftempo1=1.3
:: L1 : Let the tool automatically calculate the pitch in case multiple layers use this voice
set CONFIG_affpitch1=auto
:: L1 : Normalize with a loudness at -60LUFS
set CONFIG_affnorm1=-60
:: L1 : Loop 9 times, so the layer will be played 10 times
set CONFIG_affloop1=9
:: L1 : This isn't an ultrasonic layer
set CONFIG_affultrasonic1=
:: L1 : This layer is part of the default (empty) group
set CONFIG_affgroup1=

:: L2 : Use the given text file in the same folder as the config file
set CONFIG_aff2=%WORKSHOP_CONFIG_DIR%\affirmations02.txt
:: L2 : This layer is part of the default (empty) group
set CONFIG_affgroup2=
:: L2 : Every other setting will be inherited from L1 (first layer of the group)

:: L3 : Use the given text file in the same folder as the config file
set CONFIG_aff3=%WORKSHOP_CONFIG_DIR%\affirmations03.txt
:: L3 : This layer is part of the default (empty) group
set CONFIG_affgroup3=
:: L3 : Every other setting will be inherited from L1 (first layer of the group)

:: L4 : Use the given text file in the same folder as the config file
set CONFIG_aff4=%WORKSHOP_CONFIG_DIR%\affirmations04.txt
:: L4 : Multiply the tempo by 2.5
set CONFIG_afftempo4=2.5
:: L4 : This layer is part of the hightempo group
set CONFIG_affgroup4=hightempo
:: L4 : Every other setting will be inherited from L1 (global first layer)

:: L5 : Use the given text file in the same folder as the config file
set CONFIG_aff5=%WORKSHOP_CONFIG_DIR%\affirmations05.txt
:: L5 : This layer is part of the hightempo group
set CONFIG_affgroup5=hightempo
:: L5 : Every other setting will be inherited from L4 (first layer of the group)

:: L6 : Use the given text file in the same folder as the config file
set CONFIG_aff6=%WORKSHOP_CONFIG_DIR%\affirmations06.txt
:: L6 : This layer is part of the hightempo group
set CONFIG_affgroup6=hightempo
:: L6 : Every other setting will be inherited from L4 (first layer of the group)

:: L7 : Use the given text file in the same folder as the config file
set CONFIG_aff7=%WORKSHOP_CONFIG_DIR%\affirmations07.txt
:: L7 : Multiply the speed by 2.5
set CONFIG_affspeed7=2.5
:: L7 : Do not change the tempo. If we didn't include this it would have been 1.3 inherited from L1.
set CONFIG_afftempo7=1
:: L7 : Normalize with a loudness at -70LUFS
set CONFIG_affnorm7=-70
:: L7 : This layer is part of the highspeed group
set CONFIG_affgroup7=highspeed
:: L7 : Every other setting will be inherited from L1 (global first layer)

:: L8 : Use the given text file in the same folder as the config file
set CONFIG_aff8=%WORKSHOP_CONFIG_DIR%\affirmations08.txt
:: L8 : This layer is part of the highspeed group
set CONFIG_affgroup8=highspeed
:: L8 : Every other setting will be inherited from L7 (first layer of the group)

:: L9 : Use the given text file in the same folder as the config file
set CONFIG_aff9=%WORKSHOP_CONFIG_DIR%\affirmations09.txt
:: L9 : This layer is part of the highspeed group
set CONFIG_affgroup9=highspeed
:: L9 : Every other setting will be inherited from L7 (first layer of the group)

::::::::::::::::
:: Background ::
::::::::::::::::

:: Use the given audio file in the same folder as the config file
set CONFIG_background=%WORKSHOP_CONFIG_DIR%\background.mp3

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Enable panning the layers from left to right in order to make them easier to distinguish
set CONFIG_pan=panlaw-0

:: Display audio statistics while the sub is generated
set CONFIG_showstats=1

::::::::::::
:: Output ::
::::::::::::

:: Output the complete subliminal in the same folder as the config file
set CONFIG_outputfile_complete=%WORKSHOP_CONFIG_DIR%\subliminal-complete.wav

:: Output the affirmations-only subliminal in the same folder as the config file
set CONFIG_outputfile_nobg=%WORKSHOP_CONFIG_DIR%\subliminal-affsonly.wav
