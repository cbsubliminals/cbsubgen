This example shows how to make a subliminal using computer generated noise as the sole background.
It features 1 layer played 10 times (once and looped 9 times) with a background pink noise.

The relevant configuration can be found in workshop-config.bat in the example folder.

Run the example by using run-example.bat.
