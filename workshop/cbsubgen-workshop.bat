@echo off
setlocal EnableDelayedExpansion

::::::::::::::::::::::::::::::::::::::::::::::::
:: cbsubgen workshop v0.19 by ChloeB          ::
:: Distributed according to the GPLv3 License ::
::::::::::::::::::::::::::::::::::::::::::::::::

:: Script path
set WORKSHOP_DIR=%~dp0
if "%WORKSHOP_DIR:~-1%"=="\" set WORKSHOP_DIR=%WORKSHOP_DIR:~0,-1%

:: Detect double-click vs command line
set RUN_FROM_DOUBLE_CLICK=0
IF /I %0 EQU "%~dpnx0" set RUN_FROM_DOUBLE_CLICK=1

:: Executables path
set BAT_CBSUBGEN=%WORKSHOP_DIR%\..\cbsubgen.bat

:: Other constants
set MAX_LAYER_COUNT=50

:: Apply a custom color if we have been run by double click
if "%RUN_FROM_DOUBLE_CLICK%"=="1" color 5F

:: Show a title so the user doesn't get lost
echo :::::::::::::::::::::::::::::::::
echo :: cbsubgen workshop by ChloeB ::
echo :::::::::::::::::::::::::::::::::
echo:

:: Load configuration
set WORKSHOP_CONFIG=%~1
if not defined WORKSHOP_CONFIG set WORKSHOP_CONFIG=%WORKSHOP_DIR%\workshop-config.bat
if not exist "%WORKSHOP_CONFIG%" (
  echo:
  echo FATAL ERROR : Workshop configuration file not found at %WORKSHOP_CONFIG%
  echo:
  goto maybe_pause_and_exit
) else (
  echo:
  echo * Using configuration file : %WORKSHOP_CONFIG%
  echo:
)
for %%i in ("%WORKSHOP_CONFIG%") do set WORKSHOP_CONFIG_DIR=%%~dpi
if "%WORKSHOP_CONFIG_DIR:~-1%"=="\" set WORKSHOP_CONFIG_DIR=%WORKSHOP_CONFIG_DIR:~0,-1%
call "%WORKSHOP_CONFIG%"

:: Fix some parameters
for /L %%i in (1,1,%MAX_LAYER_COUNT%) do (
  set filesize=
  for /f "tokens=* usebackq" %%j in ('!CONFIG_aff%%i!') do set filesize=%%~zj
  if "0"=="!filesize!" set CONFIG_aff%%i=
  if ""=="!filesize!" set CONFIG_aff%%i=
)
if "%CONFIG_fadeloop%"=="0" set CONFIG_fadeloop=
if "%CONFIG_showstats%"=="0" set CONFIG_showstats=
if "%CONFIG_keepworkdir%"=="0" set CONFIG_keepworkdir=

:: Building the call to cbsubgen
:: The executable
set makeWavCommand="%BAT_CBSUBGEN%"
:: The layer settings
set layerCount=0
for /L %%i in (1,1,%MAX_LAYER_COUNT%) do (
  if not "!CONFIG_aff%%i!" == "" (
    set /a "layerCount=!layerCount!+1"
    set makeWavCommand=!makeWavCommand! /aff%%i "!CONFIG_aff%%i!"
  )
  if not "!CONFIG_affvoice%%i!"=="" set makeWavCommand=!makeWavCommand! /affvoice%%i "!CONFIG_affvoice%%i!"
  if not "!CONFIG_affspeed%%i!"=="" set makeWavCommand=!makeWavCommand! /affspeed%%i "!CONFIG_affspeed%%i!"
  if not "!CONFIG_afftempo%%i!"=="" set makeWavCommand=!makeWavCommand! /afftempo%%i "!CONFIG_afftempo%%i!"
  if not "!CONFIG_affpitch%%i!"=="" set makeWavCommand=!makeWavCommand! /affpitch%%i "!CONFIG_affpitch%%i!"
  if not "!CONFIG_affbandpass%%i!"=="" set makeWavCommand=!makeWavCommand! /affbandpass%%i "!CONFIG_affbandpass%%i!"
  if not "!CONFIG_affbandreject%%i!"=="" set makeWavCommand=!makeWavCommand! /affbandreject%%i "!CONFIG_affbandreject%%i!"
  if not "!CONFIG_affnorm%%i!"=="" set makeWavCommand=!makeWavCommand! /affnorm%%i "!CONFIG_affnorm%%i!"
  if not "!CONFIG_affloop%%i!"=="" set makeWavCommand=!makeWavCommand! /affloop%%i "!CONFIG_affloop%%i!"
  if not "!CONFIG_affultrasonic%%i!"=="" set makeWavCommand=!makeWavCommand! /affultrasonic%%i "!CONFIG_affultrasonic%%i!"
  if not "!CONFIG_affgroup%%i!"=="" set makeWavCommand=!makeWavCommand! /affgroup%%i "!CONFIG_affgroup%%i!"
)
:: The audio output settings
if not "%CONFIG_outputfile_complete%"=="" set makeWavCommand=%makeWavCommand% /outputfile "%CONFIG_outputfile_complete%"
if not "%CONFIG_outputfile_nobg%"=="" set makeWavCommand=%makeWavCommand% /outputfilenobg "%CONFIG_outputfile_nobg%"
if not "%CONFIG_outputfile_ultrasonic%"=="" set makeWavCommand=%makeWavCommand% /outputfileultrasonic "%CONFIG_outputfile_ultrasonic%"
if not "%CONFIG_outputfile_ultrasonic_freq%"=="" set makeWavCommand=%makeWavCommand% /outputfileultrasonicfreq "%CONFIG_outputfile_ultrasonic_freq%"
if not "%CONFIG_outputfile_ultrasonic_norm%"=="" set makeWavCommand=%makeWavCommand% /outputfileultrasonicnorm "%CONFIG_outputfile_ultrasonic_norm%"
if not "%CONFIG_outputfile_testaffs%"=="" set makeWavCommand=%makeWavCommand% /outputfiletestaffs "%CONFIG_outputfile_testaffs%"
if not "%CONFIG_outputfile_testaffs_norm%"=="" set makeWavCommand=%makeWavCommand% /outputfiletestaffsnorm "%CONFIG_outputfile_testaffs_norm%"
:: The video output settings
if not "%CONFIG_outputvideo_complete%"=="" set makeWavCommand=%makeWavCommand% /outputvideo "%CONFIG_outputvideo_complete%"
if not "%CONFIG_outputvideo_complete_image%"=="" set makeWavCommand=%makeWavCommand% /outputvideoimage "%CONFIG_outputvideo_complete_image%"
if not "%CONFIG_outputvideo_nobg%"=="" set makeWavCommand=%makeWavCommand% /outputvideonobg "%CONFIG_outputvideo_nobg%"
if not "%CONFIG_outputvideo_nobg_image%"=="" set makeWavCommand=%makeWavCommand% /outputvideonobgimage "%CONFIG_outputvideo_nobg_image%"
if not "%CONFIG_outputvideo_ultrasonic%"=="" set makeWavCommand=%makeWavCommand% /outputvideoultrasonic "%CONFIG_outputvideo_ultrasonic%"
if not "%CONFIG_outputvideo_ultrasonic_image%"=="" set makeWavCommand=%makeWavCommand% /outputvideoultrasonicimage "%CONFIG_outputvideo_ultrasonic_image%"
if not "%CONFIG_videosize%"=="" set makeWavCommand=%makeWavCommand% /videosize "%CONFIG_videosize%"
if not "%CONFIG_videosizemode%"=="" set makeWavCommand=%makeWavCommand% /videosizemode "%CONFIG_videosizemode%"
if not "%CONFIG_videoaudioencoding%"=="" set makeWavCommand=%makeWavCommand% /videoaudioencoding "%CONFIG_videoaudioencoding%"
if not "%CONFIG_slideshowdelay%"=="" set makeWavCommand=%makeWavCommand% /slideshowdelay "%CONFIG_slideshowdelay%"
:: The frequencies
if not "%CONFIG_leftfreq%"=="" set makeWavCommand=%makeWavCommand% /leftfreq "%CONFIG_leftfreq%"
if not "%CONFIG_rightfreq%"=="" set makeWavCommand=%makeWavCommand% /rightfreq "%CONFIG_rightfreq%"
if not "%CONFIG_freqnorm%"=="" set makeWavCommand=%makeWavCommand% /freqnorm "%CONFIG_freqnorm%"
if not "%CONFIG_freqpulse%"=="" set makeWavCommand=%makeWavCommand% /freqpulse "%CONFIG_freqpulse%"
if not "%CONFIG_freqgap%"=="" set makeWavCommand=%makeWavCommand% /freqgap "%CONFIG_freqgap%"
if not "%CONFIG_freqfade%"=="" set makeWavCommand=%makeWavCommand% /freqfade "%CONFIG_freqfade%"
if not "%CONFIG_freqrotation%"=="" set makeWavCommand=%makeWavCommand% /freqrotation "%CONFIG_freqrotation%"
:: The noise
if not "%CONFIG_noise%"=="" set makeWavCommand=%makeWavCommand% /noise "%CONFIG_noise%"
if not "%CONFIG_noisenorm%"=="" set makeWavCommand=%makeWavCommand% /noisenorm "%CONFIG_noisenorm%"
if not "%CONFIG_noisefade%"=="" set makeWavCommand=%makeWavCommand% /noisefade "%CONFIG_noisefade%"
:: The background
if exist "%CONFIG_background%" set makeWavCommand=%makeWavCommand% /background "%CONFIG_background%"
if not "%CONFIG_backgroundnorm%"=="" set makeWavCommand=%makeWavCommand% /backgroundnorm "%CONFIG_backgroundnorm%"
if not "%CONFIG_fade%"=="" set makeWavCommand=%makeWavCommand% /fade "%CONFIG_fade%"
if not "%CONFIG_fadeloop%"=="" set makeWavCommand=%makeWavCommand% /fadeloop
if not "%CONFIG_backgroundrotation%"=="" set makeWavCommand=%makeWavCommand% /backgroundrotation "%CONFIG_backgroundrotation%"
:: The other stuff
if not "%CONFIG_autofill%"=="" set makeWavCommand=%makeWavCommand% /autofill "%CONFIG_autofill%"
if not "%CONFIG_autofillduration%"=="" set makeWavCommand=%makeWavCommand% /autofillduration "%CONFIG_autofillduration%"
if not "%CONFIG_pad%"=="" set makeWavCommand=%makeWavCommand% /pad "%CONFIG_pad%"
if not "%CONFIG_pan%"=="" set makeWavCommand=%makeWavCommand% /pan "%CONFIG_pan%"
if not "%CONFIG_normwith%"=="" set makeWavCommand=%makeWavCommand% /normwith "%CONFIG_normwith%"
if not "%CONFIG_bandfilterwidth%"=="" set makeWavCommand=%makeWavCommand% /bandfilterwidth "%CONFIG_bandfilterwidth%"
if not "%CONFIG_proxyhost%"=="" set makeWavCommand=%makeWavCommand% /proxyhost "%CONFIG_proxyhost%"
if not "%CONFIG_proxyport%"=="" set makeWavCommand=%makeWavCommand% /proxyport "%CONFIG_proxyport%"
if not "%CONFIG_showstats%"=="" set makeWavCommand=%makeWavCommand% /showstats
if not "%CONFIG_workdir%"=="" set makeWavCommand=%makeWavCommand% /workdir "%CONFIG_workdir%"
if not "%CONFIG_keepworkdir%"=="" set makeWavCommand=%makeWavCommand% /keepworkdir

:: Show the layer count
echo:
echo * Workshop found %layerCount% layer(s)
echo:

:: Show targets info and delete old targets if needed
if not "%CONFIG_outputfile_nobg%"=="" (
  echo:
  echo =^> Affirmations-only audio subliminal : %CONFIG_outputfile_nobg%
  echo:
  if exist "%CONFIG_outputfile_nobg%" del "%CONFIG_outputfile_nobg%" /s/q >NUL
)
if not "%CONFIG_outputvideo_nobg%"=="" (
  echo:
  echo =^> Affirmations-only video subliminal : %CONFIG_outputvideo_nobg%
  echo:
  if exist "%CONFIG_outputvideo_nobg%" del "%CONFIG_outputvideo_nobg%" /s/q >NUL
)
if not "%CONFIG_outputfile_ultrasonic%"=="" (
  echo:
  echo =^> Ultrasonic audio subliminal : %CONFIG_outputfile_ultrasonic%
  echo:
  if exist "%CONFIG_outputfile_ultrasonic%" del "%CONFIG_outputfile_ultrasonic%" /s/q >NUL
)
if not "%CONFIG_outputvideo_ultrasonic%"=="" (
  echo:
  echo =^> Ultrasonic video subliminal : %CONFIG_outputvideo_ultrasonic%
  echo:
  if exist "%CONFIG_outputvideo_ultrasonic%" del "%CONFIG_outputvideo_ultrasonic%" /s/q >NUL
)
if not "%CONFIG_outputfile_complete%"=="" (
  echo:
  echo =^> Complete audio subliminal : %CONFIG_outputfile_complete%
  echo:
  if exist "%CONFIG_outputfile_complete%" del "%CONFIG_outputfile_complete%" /s/q >NUL
)
if not "%CONFIG_outputvideo_complete%"=="" (
  echo:
  echo =^> Complete video subliminal : %CONFIG_outputvideo_complete%
  echo:
  if exist "%CONFIG_outputvideo_complete%" del "%CONFIG_outputvideo_complete%" /s/q >NUL
)

:: Call cbsubgen
echo:
echo * Generating the subliminals, please wait...
echo:
echo:
echo =^> Running : %makeWavCommand%
echo:
call %makeWavCommand%

:: Workshop completed at this point
echo:
echo * Workshop completed
echo:

:: Pause if we are run from double click, so the user can look at the text
:maybe_pause_and_exit
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
  echo:
)

:: That's all folks
goto :eof
