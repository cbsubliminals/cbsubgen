@echo off
setlocal EnableDelayedExpansion

::::::::::::::::::::::::::::::::::::::::::::::::
:: cbsubgen workshop by ChloeB                ::
:: Distributed according to the GPLv3 License ::
::::::::::::::::::::::::::::::::::::::::::::::::

:: Script path
set WORKSHOP_DIR=%~dp0
if "%WORKSHOP_DIR:~-1%"=="\" set WORKSHOP_DIR=%WORKSHOP_DIR:~0,-1%

:: Detect double-click vs command line
set RUN_FROM_DOUBLE_CLICK=0
IF /I %0 EQU "%~dpnx0" set RUN_FROM_DOUBLE_CLICK=1

:: Executables path
set EXE_BAL4WEB=%WORKSHOP_DIR%\..\bin\bal4web\bal4web.exe

:: Show a title so the user doesn't get lost
echo :::::::::::::::::::::::::::::::::
echo :: cbsubgen workshop by ChloeB ::
echo :::::::::::::::::::::::::::::::::
echo:

:: Show some introduction
echo:
echo Here are the voices availables from various online sources.
echo:
echo To use them in the workshop, update workshop-config.bat with a voice name using the syntax bal4web:^<service^>:^<language^>:^<gender^>:^<voicename^> (without the ^<^>).
echo Services are google, microsoft and amazon.
echo Languages are under the form en-US, en-GB, fr-FR, fr-CA, etc.
echo Genders are male and female.
echo Some values can be omitted, do not write anything between the colons (:).
echo:
echo:

:: List the google voices
echo -------------------------------------
echo:
echo We are now going to list the voices available from Google.
echo:
echo Example (female US english voice) : set CONFIG_affvoice1=bal4web:google:en-US:female
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
)
"%EXE_BAL4WEB%" -s google -m
echo:
echo -------------------------------------
echo:
echo:

:: List the microsoft voices
echo -------------------------------------
echo:
echo We are now going to list the voices available from Microsoft.
echo:
echo Example (female US english voice) : set CONFIG_affvoice1=bal4web:microsoft:en-US:female:JennyNeural
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
)
"%EXE_BAL4WEB%" -s microsoft -m
echo:
echo -------------------------------------
echo:
echo:

:: List the amazon voices
echo -------------------------------------
echo:
echo We are now going to list the voices available from Amazon.
echo:
echo Example (male GB english voice) : set CONFIG_affvoice1=bal4web:amazon:en-GB:male:Brian
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
)
"%EXE_BAL4WEB%" -s amazon -m
echo:
echo -------------------------------------
echo:
echo:

:: Pause if we are run from double click, so the user can look at the text
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
  echo:
)

:: That's all folks
goto :eof
