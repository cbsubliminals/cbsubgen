@echo off
setlocal EnableDelayedExpansion

::::::::::::::::::::::::::::::::::::::::::::::::
:: cbsubgen workshop by ChloeB                ::
:: Distributed according to the GPLv3 License ::
::::::::::::::::::::::::::::::::::::::::::::::::

:: Script path
set WORKSHOP_DIR=%~dp0
if "%WORKSHOP_DIR:~-1%"=="\" set WORKSHOP_DIR=%WORKSHOP_DIR:~0,-1%

:: Detect double-click vs command line
set RUN_FROM_DOUBLE_CLICK=0
IF /I %0 EQU "%~dpnx0" set RUN_FROM_DOUBLE_CLICK=1

:: Executables path
set EXE_BALCON=%WORKSHOP_DIR%\..\bin\balcon\balcon.exe

:: Show a title so the user doesn't get lost
echo :::::::::::::::::::::::::::::::::
echo :: cbsubgen workshop by ChloeB ::
echo :::::::::::::::::::::::::::::::::
echo:

:: Show some introduction
echo:
echo Here are the voices installed on your computer.
echo To use them in the workshop, update workshop-config.bat with a unique part of their names.
echo:
echo For example, to use "Microsoft Zira Desktop" in the first layer you can write 
echo set CONFIG_affvoice1=Zira
echo:
echo Another example, to use "Microsoft Server Speech (Heather)" in the second layer you can write
echo set CONFIG_affvoice2=Heather
echo:
echo To install more voices, go to the Windows Text-to-Speech settings.
echo:
echo:

:: List the voices
echo -------------------------------------
"%EXE_BALCON%" -l
echo:
echo -------------------------------------
echo:

:: Pause if we are run from double click, so the user can look at the text
if "%RUN_FROM_DOUBLE_CLICK%"=="1" (
  echo:
  pause
  echo:
)

:: That's all folks
goto :eof
