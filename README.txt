************************************************
** cbsubgen v0.19 by ChloeB                   **
** Distributed according to the GPLv3 License **
************************************************

This file describes how to use cbsubgen and cbsubgen-workshop to create audio and video subliminals.

cbsubgen is the command line subliminal generator.
cbsubgen-workshop is a helper script to run the generator with specific settings without having to write the complete command manually.

In consequence, I recommend newbies and people non-used to the command line to use cbsubgen-workshop.
They can skip the cbsubgen part of this documentation and go straight to the cbsubgen-workshop section.

Programmers and people with a good grasp on how the workshop works are more than welcome to read the part about cbsubgen itself.

This has been tested on a laptop running Windows 10 family edition 64bits.
It will not work on macOS or Linux.


I. cbsubgen
***********

cbsubgen is a bat command line tool that can generate 1-to-50-layers subliminals, including silent ultrasonic ones.

It supports a wide range of options, in order to let you customize your sub as you like : voices, speed, background, frequencies / binaural beats / isochronic tones, noise generation, etc.

It uses locally installed SAPI5 / Microsoft Speech Platform voices, plus a few online providers like Google, Microsoft and Amazon.

It can output either audio files (great for listening on your devices), or slideshow video files (good for uploading to online services like YouTube).


I.1. Commandline usage of cbsubgen
----------------------------------

The syntax is : cbsubgen.bat <parameters>
Example : cbsubgen.bat /aff1 file.txt /outputfile file.wav


I.1.a. Required parameters :
------

/aff1 file.txt
Specifies a text/audio file with affirmations.
The affirmations will be used in the first layer.
For (optional) other layers, specify /aff2, /aff3 etc.

Additionally, at least one output parameter (ex : /outputfile) is required.
See below for a list of all the audio/video output parameters.


I.1.b. Optional audio output parameters :
------

/outputfile file.wav
Specifies the WAV/FLAC/MP3 file that will contain the generated subliminal.
Generated WAV/FLAC format is 48khz stereo 16bit.
Generated MP3 format is CBR 320 joint-stereo.
Note that WAV has a size limitation around 4gib. If it is reached, a FLAC file will be generated instead.

/outputfilenobg file.wav
Specifies the WAV/FLAC/MP3 file that will contain the generated subliminal without background audio.
Only the affirmations will be included, no frequency, binaural-beat or music.

/outputfileultrasonic ultrasonic.wav
Specifies the WAV/FLAC/MP3 file that will contain the generated ultrasonic subliminal.
Only the affirmations will be included, no frequency, binaural-beat or music.
If you use a MP3 file, be careful that MP3 conversion strips all the frequencies over 20.5khz, so set your carrier frequency accordingly.

/outputfileultrasonicfreq number
Sets the carrier frequency that will be used when generating the ultrasonic subliminal.
It must be between 15000 and 20000.

/outputfileultrasonicnorm number
Normalized volume (from 0 to -100) of the ultrasonic subliminal.
0 is full volume, -100 is very low volume.
The exact meaning and unit of this value depends of /normwith.
If missing, the volume will be the same as the one of the affirmations.

/outputfiletestaffs testaffs.wav
Specifies the WAV/FLAC/MP3 files for testing the affirmations audio.
One audio file will be generated per layer, whose name will be the one set here plus the layer number.
It makes it easier to ensure the affirmations are pronounced correctly.

/outputfiletestaffsnorm number
Normalized volume (from 0 to -100) of the affirmations test audio files.
0 is full volume, -100 is very low volume.
The exact meaning and unit of this value depends of /normwith.
If missing, the volume will be the same as the one of the layers.


I.1.c. Optional video output parameters :
------

/outputvideo video.mp4
Specifies the MP4 file that will contain the generated subliminal video.
Video format is H.264.
Audio format is either AAC CBR 320kbps, FLAC or ALAC, depending on the value of /videoaudioencoding.
The video can be uploaded as is to online services (ex : YouTube), or played locally.

/outputvideoimage image.jpg
Specifies the image(s) that will be used as background for the generated subliminal video.
Either the path to an image file, or to a folder containing multiple images.
If missing, a black 1080p image will be used.

/outputvideonobg video.mp4
Specifies the MP4 file that will contain the generated subliminal video without background audio.
Only the affirmations will be included, no frequency, binaural-beat or music.

/outputvideonobgimage image.jpg
Specifies the image(s) that will be used as background for the generated subliminal video without background audio.
Either the path to an image file, or to a folder containing multiple images.
Fallbacks to the value of /outputvideoimage.

/outputvideoultrasonic video.mp4
Specifies the MP4 file that will contain the generated ultrasonic subliminal video.
Only the affirmations will be included, no frequency, binaural-beat or music.
Note that most online services strip all the frequencies over 20.5khz, so set your carrier frequency accordingly.

/outputvideoultrasonicimage image.jpg
Specifies the image(s) that will be used as background for the generated ultrasonic subliminal video.
Either the path to an image file, or to a folder containing multiple images.
Fallbacks to the value of /outputvideoimage.

/videosize auto|image|240p|360p|480p|720p|1080p|1440p|2160p
Set the size of the video subliminals.
A value of auto (the default) will give it a target size calculated from the source image.
A value of image will give it a target size equal to that of the source image.
A value of 240p will give it a target size of 426x240.
A value of 360p will give it a target size of 640x360.
A value of 480p will give it a target size of 854x480.
A value of 720p will give it a target size of 1280x720.
A value of 1080p will give it a target size of 1920x1080.
A value of 1440p will give it a target size of 2560x1440.
A value of 2160p will give it a target size of 3840x2160.

/videosizemode auto|contain|cover|stretch
Set the sizing mode of the video subliminals.
A value of auto (the default) will give it an aspect ratio similar to the source image.
A value of contain will fit it into the target aspect ratio and add black bars if necessary.
A value of cover will crop it into the target aspect ratio, potentially causing parts of the side to be left out.
A value of stretch will stretch it to the target aspect ratio.

/videoaudioencoding aac|alac|flac
Set the audio encoding of the video subliminals.
A value of aac (the default and web standard) will use AAC CBR 320 audio (lossy compression, compatible with everything).
A value of flac will use FLAC audio (lossless compression, good compatibility).
A value of alac will use ALAC audio (lossless compression, compatible with YouTube and VLC).

/slideshowdelay duration
Delay (in seconds) before moving to the next image in videos.
Must be greater than 0.02.


I.1.d. Optional affirmations layers parameters :
------

Parameters are inherited from the first layer of the group the layer belongs to (defined with /affgroupN) if they are missing, or from the global first layer, except for /affN.
N varies between 1 and 50, for example /affvoice1 or /affspeed50.
Every layer in the same group will be positioned automatically from left to right, the first one being the left-most, and the last one the right-most.

/affN file.txt
Text/audio file with affirmations of the Nth layer.

/affvoiceN name
Voice used for the Nth layer.
Either the name of a voice installed on your computer, find it under your Windows TTS settings (ex : Zira, Hazel, Mark).
Either an online voice written as bal4web:<service>:<language>:<gender>:<voicename> without the <>
Services are google, microsoft and amazon.
Languages are under the form en-US, en-GB, fr-FR, fr-CA, etc.
Genders are male and female.
If missing, the default system voice will be used.

/affspeedN number
Speed multiplier of the voice used for the Nth layer.
It changes both the speed and the pitch, giving sort of a chipmunk voice effect.

/afftempoN number
Tempo multiplier of the voice used for the Nth layer.
It changes the speed but does not change the pitch, tho high values can cause words to be garbled.

/affpitchN number
Voice pitch of the Nth affirmations layer.
Can be a positive or negative number, in that case it represents the number of 1/100th of semitones that get added or removed (ex : 400 means adding 4 semitones, -400 removing 4 semitones).
Can also be auto, in that case it will be automatically calculated so multiple layers with the same voice have different pitches.
Can also be auto followed by a number (ex : auto100), in that case it will be automatically calculated with the given interval between pitches.

/affbandpassN number number
Apply a bandpass filter to the Nth layer, which will only keep the audio in the given frequency (hz) range (ex : 3500 10000).
Values must be between 0 and 24000.

/affbandrejectN number number
Apply a bandreject filter to the Nth layer, which will delete the audio in the given frequency (hz) range (ex : 12000 24000).
Values must be between 0 and 24000.

/affnormN number
Normalized volume (from 0 to -100) of the voice used for the Nth layer.
0 is full volume, -100 is very low volume.
The exact meaning and unit of this value depends of /normwith.

/affloopN number
Number of additional repeats of the affirmations of the Nth layer.
A value of 1 (for example) means that affirmations will be played twice (once + 1 additional repeat).

/affultrasonicN number
If the Nth layer is an ultrasonic layer, use this to set its carrier frequency (hz).
It must be between 15000 and 20000.
Not setting this parameter or setting it to 0 will make the Nth layer a non-ultrasonic layer.

/affgroupN groupName
Name identifying the group whose the Nth layer belongs to.
Layers in the same group are panned together (if /pan is provided), and inherit the settings of the first layer of the group if they aren't provided.
Not setting this parameter will cause the layer to be in the default group.
More informations on the group setting can be read later in this document.


I.1.e. Optional frequencies / binaural beats parameters :
------

/leftfreq number
Frequency (hz) to be generated in the left channel.

/rightfreq number
Frequency (hz) to be generated in the right channel.
Fallbacks to the value of /leftfreq.
To make a binaural beat, use a different setting in /leftfreq and /rightfreq.
For example, 100 in /leftfreq and 120 in /rightfreq will cause a binaural beat of 20hz.

/freqnorm number
Normalized volume (from 0 to -100) of both the left and right frequencies.
0 is full volume, -100 is very low volume.
The exact meaning and unit of this value depends of /normwith.

/freqpulse number
Duration (s) of a pulse in the case of an intermittent frequency (isochronic tone).

/freqgap number
Gap (s) between pulses in the case of an intermittent frequency (isochronic tone).

/freqfade duration
Fade duration (seconds) applied to the beginning and the end of the frequency, or of each pulse for isochronic tones.

/freqrotation normal|slow|fast
Apply a stereo rotation effect of the given speed to the frequency.


I.1.f. Optional noise generation parameters :
------

/noise white|pink|brown
Generate the given type of noise in order to hide the affirmations.
A value of white will generate white noise (random frequencies are used with same volume).
A value of pink will generate pink noise (random frequencies are used with louder volume for lower frequencies).
A value of brown will generate brown noise (random frequencies are used with even louder volume for lower frequencies).

/noisenorm number
Normalized volume (from 0 to -100) of the generated noise.
0 is full volume, -100 is very low volume.
The exact meaning and unit of this value depends of /normwith.

/noisefade duration
Fade duration (seconds) applied at the beginning and the end of the generated noise.


I.1.g. Optional background music parameters :
------

/background file.wav
Audio file which will be used as background.
It will be converted to stereo if necessary, and looped / trimmed to the same duration of the affirmations.

/backgroundnorm number
Normalized volume (from 0 to -100) of the background audio.
0 is full volume, -100 is very low volume.
The exact meaning and unit of this value depends of /normwith.

/fade duration
Duration (seconds) of the fade effect at the beginning and the end of the background music.
If missing there will be no fading.

/fadeloop
If present, the background music will also fade when it has to be repeated.
It happens when the duration of the affirmations is longer than the background music.

/backgroundrotation normal|slow|fast
Apply a stereo rotation effect of the given speed to the background music.


I.1.h. Optional other parameters :
------

/autofill simple|safe|silencedetect
Set how to loop the affirmations of the shorter layers so they are as long as the duration given in /autofillduration.
A value of simple will use a very fast method which may cut in the middle of an affirmation.
A value of safe (the default) will use a fast method which will not cut but may miss at most one repeat.
A value of silencedetect will use a slower experimental silence detection method which will not cut and will not miss any repeat.
Note that a layer based on an audio file will fallback to the safe method if the silencedetect method is chosen.

/autofillduration duration
Duration (seconds) for the autofill function.
The layers shorter than the given duration will be looped so they are nearly as long.
Not passing this parameter will cause the duration to be calculated from the longest layer.

/pad duration1 duration2
Add some silence before and after the affirmations.
The first duration (seconds) is the silence before, and the other is the silence after.
If the other duration is missing it will be the same as the first.
Not passing this parameter will cause no padding to be added.

/pan panlaw-0|panlaw-3|panlaw-4.5|panlaw-6|rotation|rotation-slow|rotation-fast
Enable panning the layers of the same group from left to right in the resulting stereo audio file so they do not overlap each other.
A value of panlaw-0 will use a neutral pan law that does not reduce the volume in the center.
A value of panlaw-3 will use a constant power pan law that reduces by 3dB the volume in the center.
A value of panlaw-4.5 will use a middleground pan law that reduces by 4.5dB the volume in the center.
A value of panlaw-6 will use a linear pan law that reduces by 6dB the volume in the center.
A value of rotation will use a stereo rotation effect.
A value of rotation-slow will use a slower stereo rotation effect.
A value of rotation-fast will use a faster stereo rotation effect.
Not passing this parameter will cause no panning to be done.
More informations on the different panning methods can be read later in this document.

/normwith peak|rms|r128
Set the method used to normalize audio.
A value of peak will use peak normalization. The normalization values will not be very representative of the overall level and loudness (quiet sound with no risk of clipping).
A value of rms will use RMS normalization. The normalization values will be representative of the overall level but not of the loudness (clipping beware).
A value of r128 (the default) will use loudness normalization. The normalization values will be representative of the perceived loudness (clipping beware). Fallbacks to RMS normalization for values lower than -70.

/proxyhost hostname_or_ip
Proxy host for online voices, useful if you get rate limited.

/proxyport number
Proxy port for online voices, useful if you get rate limited.

/showstats
Enable displaying audio statistics during the generation.
Those are the duration, peak, RMS and R128 loudness of the files.
It can slow down the generation process for long subliminals.

/workdir path\to\directory
Path to a directory where cbsubgen will generate its temporary files.
Be careful when setting this, because this folder will be emptied on every run.
By default it uses the tmp folder in its own folder, which should be good unless it is run from a readonly folder.

/keepworkdir
If present, cbsubgen will not empty the folder specified in /workdir after its processing.
It allows for debugging the overall generation process (we will see that below).


I.2. Examples of invokations of cbsubgen
----------------------------------------

I.2.a. Single layer subliminal
------

1-layer subliminal without background :
cbsubgen.bat /aff1 file.txt /outputfile subliminal.wav

1-layer subliminal video without background :
cbsubgen.bat /aff1 file.txt /outputvideo subliminal.mp4 /outputvideoimage path\to\images\folder

1-layer subliminal with background :
cbsubgen.bat /aff1 file.txt /background music.wav /outputfile subliminal.wav

1-layer subliminal with background and fade (1 second) :
cbsubgen.bat /aff1 file.txt /background music.wav /fade 1 /outputfile subliminal.wav

1-layer subliminal with background and fade (1 second, also on background loop) :
cbsubgen.bat /aff1 file.txt /background music.wav /fade 1 /fadeloop /outputfile subliminal.wav

1-layer subliminal with 100hz frequency normalized to -20dB :
cbsubgen.bat /aff1 file.txt /leftfreq 100 /freqnorm -20 /outputfile subliminal.wav

1-layer subliminal with 100hz isochronic tones normalized to -20dB :
cbsubgen.bat /aff1 file.txt /leftfreq 100 /freqnorm -20 /freqpulse 0.05 /freqgap 0.05 /outputfile subliminal.wav

1-layer subliminal with 20hz binaural beats normalized to -20dB :
cbsubgen.bat /aff1 file.txt /leftfreq 100 /rightfreq 120 /freqnorm -20 /outputfile subliminal.wav

1-layer subliminal with brown noise normalized to -20dB :
cbsubgen.bat /aff1 file.txt /noise brown /noisenorm -20 /outputfile subliminal.wav

1-layer subliminal with specific voice
cbsubgen.bat /aff1 file.txt /affvoice1 Zira /outputfile subliminal.wav

1-layer subliminal with google voice
cbsubgen.bat /aff1 file.txt /affvoice1 bal4web:google:en-US:female /outputfile subliminal.wav

1-layer subliminal with specific voice settings
cbsubgen.bat /aff1 file.txt /affvoice1 Zira /affspeed1 1.5 /afftempo1 1.5 /affpitch1 400 /affnorm1 -20 /outputfile subliminal.wav

1-layer subliminal with affirmations played three times (once plus 2 repeats)
cbsubgen.bat /aff1 file.txt /affloop1 2 /outputfile subliminal.wav

1-layer subliminal with affirmations looped for 2 minutes
cbsubgen.bat /aff1 file.txt /autofillduration 120 /outputfile subliminal.wav

1-layer ultrasonic subliminal with a carrier frequency at 17500hz
cbsubgen.bat /aff1 file.txt /affultrasonic1 17500 /outputfile subliminal.wav


I.2.b. Multiple layers subliminal
------

3-layers subliminal without background :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /outputfile subliminal.wav

3-layers subliminal video without background :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /outputvideo subliminal.mp4 /outputvideoimage path\to\images\folder

3-layers subliminal with background :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /background music.wav /outputfile subliminal.wav

3-layers subliminal with background and fade (1 second) :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /background music.wav /fade 1 /outputfile subliminal.wav

3-layers subliminal with background and fade (1 second, also on background loop) :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /background music.wav /fade 1 /fadeloop /outputfile subliminal.wav

3-layer subliminal with 100hz frequency normalized to -20dB :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /leftfreq 100 /freqnorm -20 /outputfile subliminal.wav

3-layer subliminal with 100hz isochronic tones normalized to -20dB :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /leftfreq 100 /freqnorm -20 /freqpulse 0.05 /freqgap 0.05 /outputfile subliminal.wav

3-layer subliminal with 20hz binaural beats normalized to -20dB :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /leftfreq 100 /rightfreq 120 /freqnorm -20 /outputfile subliminal.wav

3-layers subliminal with brown noise normalized to -20dB :
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /noise brown /noisenorm -20 /outputfile subliminal.wav

3-layers subliminal with specific voice (same in all channels)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affvoice1 Zira /outputfile subliminal.wav

3-layer subliminal with google voice (same in all channels)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affvoice1 bal4web:google:en-US:female /outputfile subliminal.wav

3-layers subliminal with specific voice settings (same in all channels)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affvoice1 Zira /affspeed1 1.5 /afftempo1 1.5 /affpitch1 400 /affnorm1 -20 /outputfile subliminal.wav

3-layers subliminal with specific voice (different per channel)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affvoice1 Zira /affvoice2 Hazel /affvoice3 Mark /outputfile subliminal.wav

3-layer subliminal with google/microsoft/amazon voices (different per channel)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affvoice1 bal4web:google:en-US:female /affvoice2 bal4web:microsoft:en-US:female:JennyNeural /affvoice3 bal4web:amazon:en-GB:male:Brian /outputfile subliminal.wav

3-layers subliminal with specific voice settings (different per channel)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affvoice1 Zira /affspeed1 1.5 /afftempo1 1.5 /affpitch1 400 /affnorm1 -20 /affvoice2 Hazel /affspeed2 1 /afftempo2 1 /affpitch2 -400 /affnorm2 -30 /affvoice3 Mark /affspeed3 2 /afftempo3 2 /affpitch3 600 /affnorm3 -10 /outputfile subliminal.wav

3-layers subliminal with affirmations played three times in every channel (once plus 2 repeats)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affloop1 2 /outputfile subliminal.wav

3-layers subliminal with affirmations played three times in the center (once plus 2 repeats)
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affloop1 0 /affloop2 2 /affloop3 0 /outputfile subliminal.wav

3-layers subliminal with affirmations played at least three times and the short layers being autofilled to fit the longest layer
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /affloop1 2 /outputfile subliminal.wav

3-layers subliminal with affirmations looped for 2 minutes
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /autofillduration 120 /outputfile subliminal.wav

4-layers subliminal with 1 ultrasonic layer at a carrier frequency of 17500hz
cbsubgen.bat /aff1 left.txt /aff2 center.txt /aff3 right.txt /aff4 ultra.txt /affultrasonic4 17500 /outputfile subliminal.wav


I.3. Layer positioning (panning)
--------------------------------

Positioning layers is a technique to prevent affirmations from overlapping each other.
It can be enabled with the /pan parameter.

Layer positioning is automatic, the first layer being on the left and the last on the right.
Other layers will be placed in-between, separated by an equal distance (depending on the panning method).
If there is only a single layer, it will be positioned in the center.

Different groups of layers are treated separately.
For example, you can have three layers from a first group on the left / center / right, and one ultrasonic layer from another group on the center.
Or you can have three normal layers from a first group on the left / center / right, and again three sped-up layers from another group on the left / center / right, and another ultrasonic layer from a third group on the center.

The only special case is when generating a ultrasonic version of the subliminal using /outputfileultrasonic ; in that case all the layers are made ultrasonic so they are all positioned according to each other, without the groups being taken into account.


I.3.a. Positioning with the panlaw-0 (panlaw minus 0) pan-method (/pan panlaw-0)
------

This method pans the layers and does not reduce the volume in the center.
In old versions it was called the Audacity pan method because it works similarly as the pan sliders in the Audacity software.
Layers are fully audible in the channel they are associated with, and are less and less audible in the other channel depending on their distance to the center.
As a result, the audio is louder overall than when using the other pan methods, and the center is louder than the rest.


I.3.b. Positioning with the panlaw-3 (panlaw minus 3) pan-method (/pan panlaw-3)
------

This method pans the layers and reduces the volume in the center by 3dB.
Under normal stereo listening conditions, it leaves the center marginally lower than the rest (but that's not really noticeable).
Under mono listening conditions (phone speaker..), the center will be higher by 3dB than the rest.
It is implemented using the constant power pan law described at https://www.cs.cmu.edu/~music/icm-online/readings/panlaws/panlaws.pdf.


I.3.c. Positioning with the panlaw-4.5 (panlaw minus 4.5) pan-method (/pan panlaw-4.5)
------

This method pans the layers and reduces the volume in the center by 4.5dB.
It is a middleground between the panlaw-6 and the panlaw-3 methods.
Under normal stereo listening conditions, it leaves the center a tad lower than the rest (more than panlaw-3 but less than panlaw-6).
Under mono listening conditions (phone speaker..), the center will be higher by 1.5dB than the rest.
It is implemented using the compromise pan law described at https://www.cs.cmu.edu/~music/icm-online/readings/panlaws/panlaws.pdf.


I.3.d. Positioning with the panlaw-6 (panlaw minus 6) pan-method (/pan panlaw-6)
------

This method pans the layers and reduces the volume in the center by 6dB.
Under normal stereo listening conditions, it leaves the center at a volume perceptibly lower than the rest.
Under mono listening conditions (phone speaker..), the center will have the same volume as the rest.
It is implemented using the linear pan law described at https://www.cs.cmu.edu/~music/icm-online/readings/panlaws/panlaws.pdf.


I.3.e. Positioning with the rotation, rotation-slow and rotation-fast pan-methods (/pan rotation, /pan rotation-slow, /pan rotation-fast)
------

The various rotation methods pan the layers from left to right initially in a somewhat similar fashion to the panlaw-0 method.
The layers are then rotated in the stereo field at a speed depending on the method used, either slow (rotation-slow method), medium (rotation method) or fast (rotation-fast method).
Under mono listening conditions there will be no audible rotation effect and the volume will be a tad louder.


I.3.f. Positioning without panning (no /pan parameter)
------

If you do not pass the /pan parameter, then no panning of the layers will be done.
Every layer will be positioned in the middle, and both left and right channels will have the same audio (dual mono).
This allows to make subliminals that work even with a single headphone.
Remember to use different voices and/or pitches for every layer if you go this way.
If you don't, the layers may overlap, possibly making the affirmations broken.


I.3.f. Testing and comparing the pan methods
------

You can compare the panlaws methods using the example in the workshop\examples\compare-panlaws folder.
Run the file called run-example.bat, and wait for its termination.
It will create different audio files (one for each method), allowing you to compare them and find the one you prefer.
You can also compare the rotation methods using the example in the workshop\examples\compare-panrotations folder.

It is easier to distinguish the differences between the files if you are using headphones.


I.4. Layer groups, settings inheritance and panning
---------------------------------------------------

Using or not-using layer groups changes the way inheritance and layer panning is processed.


I.4.a. Using the tool without layer groups (no /affgroupN specified)
------

If you do not pass any /affgroupN parameter, then every layer will inherit their settings from the first layer if they are empty.

All the layers will also be panned together.
For example, if we have 6 layers they will be panned as L1 on the left, L6 on the right, and L2 L3 L4 L5 in between from left to right.


I.4.b. Using the tool with layer groups (at least one /affgroupN specified)
------

If there's at least one /affgroupN parameter, then the settings inheritance changes as the following :
- All layers without a /affgroupN parameter will be considered as part of the same group (the default group)
- All layers sharing the same /affgroupN parameter will be considered as part of the same group
- The first layer of a group inherits its settings from the global first layer if they are empty
- The subsequent layers of a group inherit their settings from the first layer of the group if they are empty

The layers of a same group will be panned together.
Once all the groups have been panned, they will be mixed (merged) together.
For example, if L1, L2 and L3 are part of a group, and L4, L5, and L6 part of another, the result will be L1 and L4 on the left, L2 and L5 in the middle, L3 and L6 on the right.


I.5. Default parameters of cbsubgen
-----------------------------------

If you know bat programming, you are welcome to edit the file called config.bat which is located nearby cbsubgen.bat.
A backup is available in config.bat.default.


II. cbsubgen-workshop
*********************

cbsubgen-workshop is a script contained in the workshop folder of cbsubgen.

It reads some settings from a configuration file, and runs cbsubgen using the relevant parameters. Think of it as a way to use the subliminal generator without writing the entire command yourself.

This comes with a few advantages compared to calling the generator yourself :
- Your subliminal settings are saved into a file. No need to remember them !
- The file can be reused for multiple subs. Formula anyone ?
- The file can be located anywhere on your hard drive. By default the script uses the one in its own folder, but you can totally have a subliminal specific folder, containing the configuration file, the affirmations etc !

Given it is merely a launcher for cbsubgen, it shares the same features :
- Generation of both audio and still-image video subliminals
- Use of up to 50 layers
- Use of different voices, speeds, pitches etc.
- Automatic panning from left to right of the layers in order to maximize the audio space
- ...and everything else, see the section related to cbsubgen for more informations

Under its default configuration, it generates 2 audio files and 1 video file :
An audio file containing the affirmations without the background music (so you can verify how they sound).
Another audio file containing both the affirmations and the background.
A video file containing both the affirmations and the background, that can be uploaded to your favourite online service (ex : YouTube).

It can also be configured to generate more files :
- A video file containing the affirmations without the background music.
- An audio file containing the affirmations as silent ultrasonics, without the background music.
- A video file containing the affirmations as silent ultrasonics, without the background music.


II.1. Listing and installing voices
-----------------------------------

Before we begin, it is important that your computer has voices installed in the language of the subliminal you intend to make.
Internally cbsubgen uses the SAPI5/Microsoft Speech Platform voices that are installed on your computer.

To check the voices installed you can run (double click) the list-local-voices.bat script contained in the workshop folder.
It will display the voices, but won't specify the language for some of them.

To check the languages and install new voices (assuming you are running Windows 10), open the Settings app, and go to Time and Language > Voice.
Scroll to the bottom, you'll see a list of the voices and a button to install new ones.
Install everything you need, and use list-local-voices.bat to ensure the voices are detected correctly.
You might have to reboot your computer for the voices to be detected.


II.2. Overview of the workshop files
------------------------------------

Let's start with some explanations about the files contained in the workshop folder.

> cbsubgen-workshop.bat : This is the workshop script. You can run it by double clicking it, or by dragging and dropping a configuration file onto it.

> workshop-config.bat : This is the configuration file used when the workshop is started by a double click. You will have to edit it to customize your subliminals according to your needs.

> workshop-config.bat.default : This is a backup of the default configuration file. Use it to restore workshop-config.bat if you break it during your edits.

> affirmationsN.txt : This is the text of the Nth layer of affirmations. By default only 10 files are provided, but you can go up to 50.

> background.mp3 : This is the background noise or music of the subliminal. You can replace it by your own.

> video-images : This is the folder containing the images used when generating videos. You can replace its content by your own.

> list-local-voices.bat : Script listing the voices installed on your computer.

> list-bal4web-voices.bat : Script listing the internet voices you can use instead of the locally installed voices (non-recommended).

There is also a folder called "examples" which contains various demo configuration files.


II.3. Generating a subliminal : a walkthrough
---------------------------------------------

Now let's make your first subliminal, progressively diving into the various settings available.


II.3.a. The affirmations
-------

Open the affirmations01.txt file, and replace its content by some affirmations of your choice. This is the first layer.
Do the same for affirmations02.txt (second layer), and for affirmations03.txt (third layer).

Double click on cbsubgen-workshop.bat and look at the console window that appears, it will tell you that your subliminal is being generated and will display a few statistics.

Once the generation is done, open the subliminal-affsonly.wav file and turn up the volume of your PC to hear the affirmations.
You'll hear the first layer on the left, the second layer on the center, and the third layer on the right.
Ensure the affirmations are pronounced correctly.

If the result pleases you, you will find the complete subliminal audio in subliminal-complete.wav and the video in subliminal-complete.mp4.


II.3.b. The audio background
-------

The default background noise can be replaced by your own music file.
Just replace the background.mp3 file by your own.

Double click on cbsubgen-workshop.bat and let the subliminal be generated again.


II.3.c. The video image(s)
-------

It's equally easy to change the video image.
Do you see the video-images folder ? It contains the images that are used to make the video.
Just replace the video.jpg file in there by your own set of image files.
The dimensions of the image(s) directly impact the dimensions of the video.
In consequence, it's recommended to provide image(s) with a 16/9 aspect ratio, and a height of at least 720 pixels.

Double click on cbsubgen-workshop.bat and let the subliminal be generated again.


II.3.d. The voice
-------

To change the voice you have to edit the workshop-config.bat in a text editor like notepad. Either right click it and click the Edit menu item, or run the notepad (in the start menu, search notepad) and open it from the File menu.

The file is a BAT file (a windows script), so :
- Lines starting with :: are comments and are ignored
- There must be no space before or after equal signs (CONFIG_aff=file.txt is ok, but CONFIG_aff = file.txt is NOT ok)
- There must be no space at the end of the lines either (otherwise the spaces are considered as part of the values)

The voice layer settings are defined on lines starting by "set CONFIG_affvoiceN=", where N is the layer number.
Edit those lines for the layers 1 to 3, by adding the name of the voice right after the equal sign.
For example : set CONFIG_affvoice1=Zira.
To get the list of voice names available on your computer, run the list-local-voices.bat script.

Save the file, double click on cbsubgen-workshop.bat and let the subliminal be generated again.

Use the subliminal-affsonly.wav file to check how the sound has changed.


II.3.e. The layer settings (speed, pitch, volume)
-------

Once again, open and edit the workshop-config.bat file.

There are a lot of settings allowing you to tweak each layer, here's a list :
- CONFIG_affspeedN : changes the speed of the voice and also changes the pitch (faster means a higher pitch, slower means a lower pitch)
- CONFIG_afftempoN : changes the speed of the voice but doesn't change the pitch (high values can cause words to be "eaten")
- CONFIG_affpitchN : changes the pitch of the voice by the given number of 1/100th of semitone (100 means adding 1 semitone, -100 means removing 1 semitone). Can also be auto for automatic calculation of different pitches for layers using the same voice.
- CONFIG_affbandpassN : apply a bandpass filter which will only keep the audio in the given frequency (hz) range (ex : 3500 10000). Values must be between 0 and 24000.
- CONFIG_affbandrejectN : apply a bandreject filter which will delete the audio in the given frequency (hz) range (ex : 12000 24000). Values must be between 0 and 24000.
- CONFIG_affnormN : changes the normalization (the volume) of the voice to the given value between 0 (high volume) and -100 (low volume).
- CONFIG_affloopN : changes the amount of times the affirmations are looped. 1 means they are looped once so played twice in total.
- CONFIG_affultrasonicN : translates the sound of the layer to the given high frequency to make it inaudible. The result is a layer that does emit sound at a potentially audible volume but at a frequency that cannot be heard. Recommended value : 17500.
- CONFIG_affgroupN : sets the group the layer is part of. It impacts setting inheritance and left-to-right panning.

Note that settings are inherited from the global first layer or from the first layer of the group (if CONFIG_affgroupN is used).
That means that if CONFIG_affspeed2 is empty, it will get its value from CONFIG_affspeed1.

Play with the values to get a grasp at what they do, then save the file, double click on cbsubgen-workshop.bat and let the subliminal be generated again.

Use the subliminal-affsonly.wav file to check how the sound has changed.


II.3.f. The frequency settings
-------

Let's start with some definitions :
In cbsubgen frequencies are either plain frequencies, isochronic tones or binaural beats.
Plain frequencies mean that the same continuous beep is emitted in the left and right ear.
Isochronic tones mean that the same non-continuous pulses (beep then silence then beep etc.) are emitted in the left and right ear.
Binaural beats mean that two different beeps are emitted in the left and right ear.

As usual, open and edit the workshop-config.bat file.

The relevant settings are :
- CONFIG_leftfreq : Frequency emitted in the left ear (in hertz).
- CONFIG_rightfreq : Frequency emitted in the right ear (in hertz).
- CONFIG_freqnorm : Normalization (volume) of the frequencies from 0 (high volume) to -100 (low volume).
- CONFIG_freqpulse : For isochronic tones, duration of a pulse (in seconds).
- CONFIG_freqgap : For isochronic tones, duration of the gap between pulses (in seconds).
- CONFIG_freqfade : Duration (seconds) of the fade effect applied to the beginning and the end of the entire frequency (plain frequencies and binaural beats) or of each pulse (isochronic tones).
- CONFIG_freqrotation : Apply an optional stereo rotation effect of the given speed (normal slow fast) to the frequencies.

Try making a plain frequency (same value from CONFIG_leftfreq and CONFIG_rightfreq, empty CONFIG_freqpulse and CONFIG_freqgap), an isochronic tone (same value from CONFIG_leftfreq and CONFIG_rightfreq, non-empty CONFIG_freqpulse and CONFIG_freqgap) and a binaural beat (different value from CONFIG_leftfreq and CONFIG_rightfreq).

Use the subliminal-complete.wav file to check how the sound has changed.


II.3.g. The background settings
-------

As usual, open and edit the workshop-config.bat file.

Apart from replacing the background.mp3 file, a few settings are available to customize it.

First, you can change the volume of the background. It is done using the CONFIG_backgroundnorm setting, using a value from 0 (high volume) to -100 (low volume).

Next, you can add a fade effect to the background.
The fade effect duration (in seconds) can be set using CONFIG_fade.
Additionally, you can enable the fade effect to be applied everytime the background is looped. To do that set CONFIG_fadeloop to 1.

You can also add a stereo rotation effect to the background by setting CONFIG_backgroundrotation to normal, fast or slow.

Save the file, double click on cbsubgen-workshop.bat and let the subliminal be generated again.

Use the subliminal-complete.wav file to check how the sound has changed.


II.3.h. Noise settings
-------

Sometimes you do not want to use a background audio file but to generate random noise instead, or you want to insert additional noise to the background audio.

Edit the workshop-config.bat file as usual.

The noise type is given in CONFIG_noise.
Set it to white to generate some white noise (random noise equally loud across all frequencies).
Set it to pink to generate some pink noise (random noise louder at the lower frequencies).
Set it to brown to generate some brown noise (random noise even louder at the lower frequencies).

The noise volume is set in CONFIG_noisenorm, using a value from 0 (high volume) to -100 (low volume).

A fade effect can be added to the noise by setting CONFIG_noisefade to the given duration (in seconds).

An example comparing the noise types is available in the examples folder of the workshop.


II.3.i. Other settings
-------

Among the other settings, a few are simple enough to be mentioned here.
Open and edit the workshop-config.bat file.

The first is CONFIG_autofillduration.
This setting defines a target duration (in seconds) for the subliminal.
The layers will be looped in order for the result duration to be as close as possible to it.

The second is CONFIG_pad.
This setting defines one or two durations (in seconds) of silence which will be added to the beginning and the end of the subliminal.
For example, CONFIG_pad=1 will add 1 second of silence at the beginning and the end, and CONFIG_pad=1 2 will add 1 second of silence at the beginning and 2 seconds at the end.

The third is CONFIG_pan.
This setting defines how the layers are panned from left to right.
Set it to panlaw-0 for panning them in a similar fashion as the popular submaker tool called Audacity.
Set it to rotation for a fancy stereo rotation effect.
Leave it empty to disable panning altogether.

The last is CONFIG_showstats.
Setting it to 1 causes statistics to be displayed when audio or video files are generated.
Those statistics are useful to check durations and audio volumes, but can take a long time to compute on lengthy subliminals.
Leave it empty to disable displaying and computing statistics.


II.3.j. Audio output settings
-------

You can tweak the audio output settings to only output the files that you need.
Open and edit the workshop-config.bat file.

The first output files you can generate are the complete subliminals, containing both the affirmations and the background. Their settings are :
- CONFIG_outputfile_complete : specifies the complete audio subliminal path. Commenting that line (adding :: at the beginning) disables the generation of that file.

The next output files are the affirmations-only subliminals. Their settings are :
- CONFIG_outputfile_nobg : specifies the affirmations-only audio subliminal path. Commenting that line (adding :: at the beginning) disables the generation of that file.

The last output files are the ultrasonic affirmations subliminals. Their settings are :
- CONFIG_outputfile_ultrasonic : specifies the ultrasonic affirmations audio subliminal path. Commenting that line (adding :: at the beginning) disables the generation of that file.
- CONFIG_outputfile_ultrasonic_freq : sets the base high frequency (in hertz) of the ultrasonic affirmations subliminal. Recommended value : 17500.
- CONFIG_outputfile_ultrasonic_norm : changes the normalization (volume) of the ultrasonic affirmations subliminal from 0 (high volume) to -100 (low volume).

Note that for cbsubgen to work there must be at least one output audio or video file specified.


II.3.k. Video output settings
-------

In a similar fashion, you can tweak the video output settings to only output the files that you need.
Open and edit the workshop-config.bat file.

The first video files you can generate are the complete subliminals, containing both the affirmations and the background. Their settings are :
- CONFIG_outputvideo_complete : specifies the complete video subliminal path. Commenting that line (adding :: at the beginning) disables the generation of that file.
- CONFIG_outputvideo_complete_image : specifies the image(s) that are used to create the video. Either the path to an image file, or to a folder containing multiple images.

The next video files are the affirmations-only subliminals. Their settings are :
- CONFIG_outputvideo_nobg : specifies the affirmations-only video subliminal path. Commenting that line (adding :: at the beginning) disables the generation of that file.
- CONFIG_outputvideo_nobg_image : specifies the image(s) that are used to create the video. Fallbacks to the value of CONFIG_outputvideo_complete_image.

The last video files are the ultrasonic affirmations subliminals. Their settings are :
- CONFIG_outputvideo_ultrasonic : specifies the ultrasonic affirmations video subliminal path. Commenting that line (adding :: at the beginning) disables the generation of that file.
- CONFIG_outputvideo_ultrasonic_image : specifies the image(s) that are used to create the video. Fallbacks to the value of CONFIG_outputvideo_complete_image.

Note that for cbsubgen to work there must be at least one output audio or video file specified.

By default videos use lossy audio compression (AAC CBR 320kbps) since that's the most compatible setup.
You can change that to FLAC or ALAC, which are lossless but less compatible (but still uploadable to YouTube).
For FLAC use :
- CONFIG_videoaudioencoding=flac
And for ALAC use :
- CONFIG_videoaudioencoding=alac
Leave it empty for the default AAC encoding.


II.4. Recommended subliminal folder structure
---------------------------------------------

Modifying the files located in the workshop folder isn't a viable solution once you start making a medium amount of subliminals.
You need to keep a backup of everything in case your uploaded videos get deleted, or a user asks you for a similar subliminal but with a different music for example.

That's why the following folder structure is recommended for each and every of your subliminals projects :

+ My Subliminal Name folder
|__ affirmationsN.txt
|__ background.mp3
|__ workshop-config.bat
|__ subliminal-affsonly.wav
|__ subliminal-complete.wav
|__ subliminal-complete.mp4
|__ video-images folder
   |__ image01.png
   |__ image02.png  

This gives you an easy to backup folder, which can be reused very easily to recreate a subliminal.
You can simply drag and drop the workshop-config.bat file from that folder over the cbsubgen-workshop.bat file and the workshop will work using this file instead of the default one from its own folder.


III. Other tools
****************

III.1. List the installed voices with list-local-voices.bat
-----------------------------------------------------------

A script called list-local-voices.bat is located in the workshop folder.
It allows you to list the voices that are installed on your computer.
You can use them in workshop-config.bat or when calling cbsubgen.bat on the command-line.
Just double-click on the script to run it, and it will display the voice names.

Note that you do not have to use the full names of the voices.
For example, if the voice is called Microsoft David Desktop, you can simply use David.
Another example, if the voice is called Microsoft Server (en-GB, Hazel), you can simply use Hazel.


III.2. List the online voices with list-bal4web-voices.bat
----------------------------------------------------------

Another script called list-bal4web-voices.bat is located in the workshop folder.
It allows you to list the voices that are available from various online services.
You can use them in workshop-config.bat or when calling cbsubgen.bat on the command-line.
Just double-click on the script to run it, and it will display the voice names.

The script will remind you of the syntax, but for the sake of being complete, here are a few examples :
- bal4web:google:en-US:female
- bal4web:google:en-US
- bal4web:microsoft:en-US:female:JennyNeural
- bal4web:microsoft:::JennyNeural
- bal4web:amazon:en-GB:male:Brian
- bal4web:amazon:::Brian


IV. Credits
***********

cbsubgen is made by ChloeB.

It uses :
- Balabolka console (http://www.cross-plus-a.com/bconsole.htm)
- Balabolka online TTS utility (http://www.cross-plus-a.com/bweb.htm)
- SoX (http://sox.sourceforge.net/sox.html)
- Nyquist (https://www.cs.cmu.edu/~music/nyquist/)
- loudness-scanner (https://github.com/jiixyj/loudness-scanner)
- LAME (https://lame.sourceforge.io)
- FFmpeg (https://ffmpeg.org/)
- Bc for Windows (http://gnuwin32.sourceforge.net/packages/bc.htm)
- sha1sum (http://code.kliu.org/misc/hashutils/)
