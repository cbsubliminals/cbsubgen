;;::::::::::::::::::::::::::::::::::::::::::::::
;; cbsubgen                                   ::
;; Distributed according to the GPLv3 License ::
;;::::::::::::::::::::::::::::::::::::::::::::::


;; ultrasonic.lsp
;; Converts the audio from a WAV file to ultrasonics
;; Parameters :
;; *input-file* Path to the input file (wav)
;; *output-file* Path to the output file (wav)
;; *carrier* Ultrasonic carrier frequency


;; Constants

(setf *DEFAULT_MAX_LEN* 1000000000000)


;; Helper for working on multichannel sound (vector of sounds)
;; Source for this part : Audacity

(defun multichan-expand (fn &rest args)
  (let (len newlen result) ; len is a flag as well as a count
    (dolist (a args)
        (cond ((arrayp a)
           (setf newlen (length a))
           (cond ((and len (/= len newlen))
              (error (format nil "In ~A, two arguments are vectors of differing length." fn))))
           (setf len newlen))))
    (cond (len
       (setf result (make-array len))
       ; for each channel, call fn with args
       (dotimes (i len)
           (setf (aref result i)
             (apply fn
            (mapcar
                #'(lambda (a)
                ; take i'th entry or replicate:
                (cond ((arrayp a) (aref a i))
                      (t a)))
                args))))
       result)
      (t
       (apply fn args)))))


;; The transformation process
;; Inspired from : https://forum.audacityteam.org/viewtopic.php?t=58548

;; We have two Nyquist frequencies, *carrier*/2 and *sound-srate*/2.
;; The CUTOFF is the maximum allowed frequency in the modulator.
;; It must not be greater than *carrier*/2, but also not greater than
;; the difference between the *carrier* and *sound-srate*/2, because
;; otherwise the modulated carrier aliases.

(defun cut (function sound frequency)
  (dotimes (ignore 10 sound)
    (setf sound (funcall function sound frequency))))

(defun ultrasonic (sound)
  (setf sound-srate (snd-srate sound))
  (setf nyquist-srate (/ sound-srate 2.0))
  (setf carrier (max 15000 (min *carrier* (- nyquist-srate 3000))))
  (setf cutoff (min (/ carrier 2.0) (- nyquist-srate carrier)))
  (setf sound-duration (/ (snd-length sound *DEFAULT_MAX_LEN*) sound-srate))
  (setf sinusoid (stretch (1+ sound-duration) (hzosc carrier)))
  (let ((result (mult 2 (cut #'lowpass8 (hp sound 80) cutoff)
                        sinusoid)))
    (cut #'highpass8 result carrier)))


;; Our file processing

(defun fileexists (file)
  (setf fp (open-binary file :direction :input))
  (if fp (progn
    (close fp)
    T
  ) nil)
)

(defun getaudiochannelcount (file)
  (setf sound (s-read file))
  (unless sound (return-from getaudiochannelcount nil))
  (nth 1 *rslt*)
)

(defun getaudiosamplerate (file)
  (setf sound (s-read file))
  (unless sound (return-from getaudiosamplerate nil))
  (nth 5 *rslt*)
)

(defun getaudioduration (file)
  (setf sound (s-read file))
  (unless sound (return-from getaudioduration nil))
  (nth 6 *rslt*)
)

(defun calculatechunkduration (channelcount samplerate)
  ;; TODO : Find a reliable way to calculate the duration so the memory footprint never exceeds 1gb according to the channel count and the sample rate
  ;; In the mean time, 600 has been tested for stereo 96000
  600
)

(defun processchunk (input-file output-file offset duration)
  (setf sound (s-read input-file :time-offset offset :dur duration))
  (setf sound (multichan-expand #'ultrasonic sound))
  (setf format snd-head-Wave)
  (when (string= (subseq (string-downcase output-file) (- (length output-file) 4)) ".w64") (setf format snd-head-W64))
  (if (> offset 0) 
    (s-add-to sound *DEFAULT_MAX_LEN* output-file offset)
    (s-save sound *DEFAULT_MAX_LEN* output-file :format format :mode snd-mode-pcm :bits 16)
  )
)

(defun processfile (input-file output-file) 
  (unless (fileexists input-file) (exit))
  (setf samplerate (getaudiosamplerate input-file))
  (unless (>= samplerate 44100) (exit))
  (setf channelcount (getaudiochannelcount input-file))
  (unless channelcount (exit))
  (setf duration (getaudioduration input-file))
  (unless duration (exit))
  (setf basechunkduration (calculatechunkduration channelcount samplerate))
  (setf offset 0)
  (loop 
    (when (>= offset duration) (return))
    (setf chunkduration basechunkduration)
    (when (>= (+ offset chunkduration) duration) (setf chunkduration (- duration offset)))
    (errset (processchunk input-file output-file offset chunkduration))
    (setf offset (+ offset chunkduration))
  )
)

(errset (processfile *input-file* *output-file*))


;; All done !

(print "Ultrasonic.lsp terminated")
(exit)
