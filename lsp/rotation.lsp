;;::::::::::::::::::::::::::::::::::::::::::::::
;; cbsubgen                                   ::
;; Distributed according to the GPLv3 License ::
;;::::::::::::::::::::::::::::::::::::::::::::::


;; rotation.lsp
;; Applies a rotation effect to a stereo audio file
;; Parameters :
;; *input-file* Path to the input file (wav)
;; *output-file* Path to the output file (wav)
;; *rate* LFO rate (hz) from 0.02 to 20 
;; *phase* LFO starting phase (degrees) (-180..180)
;; *left* Leftmost pan position (percent) 0..100
;; *right* Rightmost pan position (percent) 0..100


;; Constants

(setf *DEFAULT_MAX_LEN* 1000000000000)


;; The transformation process
;; Based on the Panning [LFO] 2a Audacity plugin by David R. Sky, see https://old.audacityteam.org/nyquist/panlfo2a.ny

;; function to pan stereo audio
;; by Dominic Mazzoni
;; 'where' can be a number or signal, from 0 to +1, inclusive
;; 0 left channel, 0.5 center pan position, 1.0 right channel
(defun pan2 (sound where)
  (vector (mult (aref sound 0) (sum 1 (mult -1 where)))
  (mult (aref sound 1) where)))

;; function to return LFO waveform for panning
(defun get-lfo (offset sign range rate duration waveform phase)
  (sum offset (mult range (sum 0.5 (mult sign 0.5 
  (lfo rate duration waveform phase))))))

;; function to process a sound object
(defun rotation (sound)
  ; calculate duration of selected audio
  (setf sound-srate (snd-srate (aref sound 0)))
  (setf dur (/ (snd-length (aref sound 0) *DEFAULT_MAX_LEN*) sound-srate))

  ;; setting chosen Nyquist waveform
  ;; not useful since we do not allow choosing but kept for reference
  (setf waveform *sine-table*)

  ;; if inverted saw is chosen, set sign value to -1
  ;; not useful since we do not allow inverted saw waveforms but kept for reference
  (setf sign (if nil -1.0 1.0))

  ;; calculate range - how far LFO sweeps
  (setf range (* 0.01 (abs (- *left* *right*))))

  ;; offset - how far right of the left channel
  ;; the LFO sweep takes place
  ;; for default values of 20% and 80%, range is 60%
  ;; and offset becomes 20%
  (setf offsetlfo (* 0.01 (min *left* *right*)))

  ;; determine scaling factor to use after audio has been panned
  ;; this is because left and right channels may have had maximum amplitude
  ;; before panning, so closer to middle pan position after panning,
  ;; volume will sound reduced
  (setf scale-factor (* 2 (- 1.0 
    (* 0.01 (max (abs (- 50 *left*))
    (abs (- 50 *right*)))) )))
  
  (pan2 
    ;; following lines convert stereo audio into mono-sounding audio 
    ;; for proper panning effect
    (mult 0.5 scale-factor (vector (sum (aref sound 0) (aref sound 1)) (sum (aref sound 1) (aref sound 0))))
    (get-lfo offsetlfo sign range *rate* (+ dur 1) waveform *phase*))
)


;; Our file processing

(defun fileexists (file)
  (setf fp (open-binary file :direction :input))
  (if fp (progn
    (close fp)
    T
  ) nil)
)

(defun getaudiochannelcount (file)
  (setf sound (s-read file))
  (unless sound (return-from getaudiochannelcount nil))
  (nth 1 *rslt*)
)

(defun getaudiosamplerate (file)
  (setf sound (s-read file))
  (unless sound (return-from getaudiosamplerate nil))
  (nth 5 *rslt*)
)

(defun getaudioduration (file)
  (setf sound (s-read file))
  (unless sound (return-from getaudioduration nil))
  (nth 6 *rslt*)
)

(defun calculatechunkduration (channelcount samplerate lforate)
  ;; TODO : Find a reliable way to calculate the duration so the memory footprint never exceeds 1gb according to the channel count and the sample rate and LFO rate
  ;; In the mean time, 600 has been tested for stereo 96000
  ;; We should also ensure that the given LFO rate causes a complete rotation cycle (so a chunk doesn't end in the middle of a rotation)
  600
)

(defun processchunk (input-file output-file offset duration)
  (setf sound (s-read input-file :time-offset offset :dur duration))
  (setf sound (rotation sound))
  (setf format snd-head-Wave)
  (when (string= (subseq (string-downcase output-file) (- (length output-file) 4)) ".w64") (setf format snd-head-W64))
  (if (> offset 0) 
    (s-add-to sound *DEFAULT_MAX_LEN* output-file offset)
    (s-save sound *DEFAULT_MAX_LEN* output-file :format format :mode snd-mode-pcm :bits 16)
  )
)

(defun processfile (input-file output-file) 
  (unless (fileexists input-file) (exit))
  (setf samplerate (getaudiosamplerate input-file))
  (unless samplerate (exit))
  (setf channelcount (getaudiochannelcount input-file))
  (unless (>= channelcount 2) (exit))
  (setf duration (getaudioduration input-file))
  (unless duration (exit))
  (setf basechunkduration (calculatechunkduration channelcount samplerate *rate*))
  (setf offset 0)
  (loop 
    (when (>= offset duration) (return))
    (setf chunkduration basechunkduration)
    (when (>= (+ offset chunkduration) duration) (setf chunkduration (- duration offset)))
    (errset (processchunk input-file output-file offset chunkduration))
    (setf offset (+ offset chunkduration))
  )
)

(errset (processfile *input-file* *output-file*))


;; All done !

(print "Rotation.lsp terminated")
(exit)
