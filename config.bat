:: cbsubgen configuration

:: Most settings can be overloaded via command line arguments
:: If no argument is given, the value given in this file will be used
:: In case you break something, the default configuration file is available in config.bat.default

:: This is a BAT file, so DO NOT PUT A SPACE BEFORE OR AFTER THE = SYMBOL !
:: set ARG_xxx=value is good
:: set ARG_xxx = value is NOT good

:: Available constants :
:: %SCRIPT_DIR% : Directory of the cbsubgen script, without the trailing antislash (ex : c:\path\cbsubgen)
:: + all default Windows constants

::::::::::::
:: Layers ::
::::::::::::

:: Voice of the first affirmations layer
:: Either the name of a voice installed on your computer, find it under your Windows TTS settings
:: Parameter : /affvoice1 Zira
:: Either an online voice written as bal4web:<service>:<language>:<gender>:<voicename>
:: Services are google, microsoft and amazon
:: Languages are under the form en-US, en-GB, fr-FR, fr-CA, etc
:: Genders are male and female
:: Some values can be omitted, do not write anything between the colons (:)
:: Parameter : /affvoice1 bal4web:google:en-US:female
:: Parameter : /affvoice1 bal4web:microsoft:en-US:female:JennyNeural
:: Parameter : /affvoice1 bal4web:amazon:en-GB:male:Brian
:: Use the list-local-voices.bat and list-bal4web-voices.bat scripts in the workshop folder to get a list of the voices available
:: Default value : (empty, use system default)
set ARG_affvoice1=

:: Voice speed multiplier of the first affirmations layer.
:: It changes both the speed and the pitch of the voice.
:: Parameter : /affspeed1 1.5
:: Default value : (empty, means no change)
set ARG_affspeed1=

:: Voice tempo multiplier of the first affirmations layer.
:: It changes the speed but keeps the pitch of the voice.
:: Parameter : /afftempo1 1.5
:: Default value : (empty, means no change)
set ARG_afftempo1=

:: Voice pitch of the first affirmations layer
:: Can be a positive or negative number, in that case it represents the number of 1/100th of semitones that get added or removed
:: Can also be auto, in that case it will be automatically calculated so multiple layers with the same voice have different pitches
:: Can also be auto followed by a number (ex : auto100), in that case it will be automatically calculated with the given interval between pitches
:: Parameter : /affpitch1 400
:: Parameter : /affpitch1 auto
:: Parameter : /affpitch1 auto200
:: Default value : (empty, means no change)
set ARG_affpitch1=

:: Frequency range of the bandpass filter of the first affirmations layer.
:: Must be under the form lowfreq highfreq, for example 3500 10000 for a filter that only keeps audio in the 3500hz-10000hz range.
:: Values range from 0 to 24000.
:: Parameter : /affbandpass1 1000 5000
:: Default value : (empty, means no change)
set ARG_affbandpass1=

:: Frequency range of the bandreject filter of the first affirmations layer
:: Must be under the form lowfreq highfreq, for example 12000 24000 for a filter that deletes audio in the 12000hz-24000hz range.
:: Values range from 0 to 24000.
:: Parameter : /affbandreject1 1000 5000
:: Default value : (empty, means no change)
set ARG_affbandreject1=

:: Voice normalization (volume from 0 to -100) of the first affirmations layer.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Parameter : /affnorm1 -50
:: Default value : -60
set ARG_affnorm1=-60

:: Number of times the affirmations of the first layer will be repeated.
:: Parameter : /affloop1 3
:: Default value : (empty, means 0)
set ARG_affloop1=

:: Carrier frequency (hz) used if the first layer is ultrasonic.
:: Must be between 15000 and 20000.
:: Leave empty or set to 0 if it is not an ultrasonic layer.
:: Parameter : /affultrasonic1 15000
:: Default value : (empty, means non-ultrasonic layer)
set ARG_affultrasonic1=

:: Name identifying the group whose the first layer belongs to.
:: Layers in the same group are panned together (if /pan is provided), and inherit the settings of the first layer of the group if they aren't provided.
:: Leave empty to use the default group.
:: Parameter : /affgroup1 groupname
:: Default value : (empty, means the default group)
set ARG_affgroup1=

:: Settings for the 2nd affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice2=
set ARG_affspeed2=
set ARG_afftempo2=
set ARG_affpitch2=
set ARG_affbandpass2=
set ARG_affbandreject2=
set ARG_affnorm2=
set ARG_affloop2=
set ARG_affultrasonic2=
set ARG_affgroup2=

:: Settings for the 3rd affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice3=
set ARG_affspeed3=
set ARG_afftempo3=
set ARG_affpitch3=
set ARG_affbandpass3=
set ARG_affbandreject3=
set ARG_affnorm3=
set ARG_affloop3=
set ARG_affultrasonic3=
set ARG_affgroup3=

:: Settings for the 4th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice4=
set ARG_affspeed4=
set ARG_afftempo4=
set ARG_affpitch4=
set ARG_affbandpass4=
set ARG_affbandreject4=
set ARG_affnorm4=
set ARG_affloop4=
set ARG_affultrasonic4=
set ARG_affgroup4=

:: Settings for the 5th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice5=
set ARG_affspeed5=
set ARG_afftempo5=
set ARG_affpitch5=
set ARG_affbandpass5=
set ARG_affbandreject5=
set ARG_affnorm5=
set ARG_affloop5=
set ARG_affultrasonic5=
set ARG_affgroup5=

:: Settings for the 6th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice6=
set ARG_affspeed6=
set ARG_afftempo6=
set ARG_affpitch6=
set ARG_affbandpass6=
set ARG_affbandreject6=
set ARG_affnorm6=
set ARG_affloop6=
set ARG_affultrasonic6=
set ARG_affgroup6=

:: Settings for the 7th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice7=
set ARG_affspeed7=
set ARG_afftempo7=
set ARG_affpitch7=
set ARG_affbandpass7=
set ARG_affbandreject7=
set ARG_affnorm7=
set ARG_affloop7=
set ARG_affultrasonic7=
set ARG_affgroup7=

:: Settings for the 8th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice8=
set ARG_affspeed8=
set ARG_afftempo8=
set ARG_affpitch8=
set ARG_affbandpass8=
set ARG_affbandreject8=
set ARG_affnorm8=
set ARG_affloop8=
set ARG_affultrasonic8=
set ARG_affgroup8=

:: Settings for the 9th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice9=
set ARG_affspeed9=
set ARG_afftempo9=
set ARG_affpitch9=
set ARG_affbandpass9=
set ARG_affbandreject9=
set ARG_affnorm9=
set ARG_affloop9=
set ARG_affultrasonic9=
set ARG_affgroup9=

:: Settings for the 10th affirmations layer. Refer to the first layer settings for informations
:: They will be inherited from the settings of the first layer of their group or from the global first layer if they are left empty
set ARG_affvoice10=
set ARG_affspeed10=
set ARG_afftempo10=
set ARG_affpitch10=
set ARG_affbandpass10=
set ARG_affbandreject10=
set ARG_affnorm10=
set ARG_affloop10=
set ARG_affultrasonic10=
set ARG_affgroup10=

:: The software supports up to 50 layers
:: For the sake of keeping the configuration file small enough, not all the settings for every possible layer were included
:: Feel free to duplicate and rename the existing settings to add more layers (ARG_affvoice11 etc)

:::::::::::::::
:: Frequency ::
:::::::::::::::

:: Frequency (hz) to be generated in the left channel.
:: Parameter : /leftfreq 100
:: Default value : (empty, means none)
set ARG_leftfreq=

:: Frequency (hz) to be generated in the right channel.
:: Parameter : /rightfreq 100
:: Default value : (empty, fallbacks to the left channel settings)
set ARG_rightfreq=

:: Normalization (volume from 0 to -100) of the frequency.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Parameter : /freqnorm -20
:: Default value : -20
set ARG_freqnorm=-20

:: Duration (s) of a pulse in the case of an intermittent frequency (isochronic tone).
:: Parameter : /freqpulse 2
:: Default value : (empty, means non-intermittent frequency)
set ARG_freqpulse=

:: Gap (s) between pulses in the case of an intermittent frequency (isochronic tone).
:: Parameter : /freqgap 1
:: Default value : (empty, means non-intermittent frequency)
set ARG_freqgap=

:: Fade duration (s) applied to the beginning and the end of the frequency or of each pulse.
:: Parameter : /freqfade 0.5
:: Default value : (empty, means no fade)
set ARG_freqfade=

:: Speed of the stereo rotation effect applied to the frequency.
:: Parameter : /freqrotation normal|slow|fast
:: Default value : (empty, means no rotation effect)
set ARG_freqrotation=

:::::::::::
:: Noise ::
:::::::::::

:: Noise type to be generated.
:: A value of white will generate white noise.
:: A value of pink will generate pink noise.
:: A value of brown will generate brown noise.
:: Parameter : /noise white|pink|brown
:: Default value : (empty, means do not generate noise)
set ARG_noise=

:: Normalization (volume from 0 to -100) of the generated noise.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Parameter : /noisenorm -20
:: Default value : -20
set ARG_noisenorm=-20

:: Duration (seconds) for the fade effect at the beginning and the end of the generated noise.
:: Parameter : /noisefade 3
:: Default value : (empty, which means no fade)
set ARG_noisefade=

::::::::::::::::
:: Background ::
::::::::::::::::

:: Background music file.
:: Parameter : /background file.wav
:: Default value : (empty, which means no background)
set ARG_background=

:: Normalization (volume from 0 to -100) of the background music.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Parameter : /backgroundnorm -30
:: Default value : (empty, means no normalization)
set ARG_backgroundnorm=

:: Duration (seconds) for the fade effect at the beginning and the end of the background music.
:: Parameter : /fade 3
:: Default value : (empty, which means no fade)
set ARG_fade=

:: Enable fade when the background music is looped ? Any value different than 0 means yes.
:: Parameter : /fadeloop
:: Default value : (empty, which means do not fade on loop)
set ARG_fadeloop=

:: Speed of the stereo rotation effect applied to the background.
:: Parameter : /backgroundrotation normal|slow|fast
:: Default value : (empty, means no rotation effect)
set ARG_backgroundrotation=

::::::::::::::::::::
:: Other settings ::
::::::::::::::::::::

:: Set how to loop the affirmations of the shorter layers so they are as long as the duration given in /autofillduration.
:: A value of simple will use a very fast method which may cut in the middle of an affirmation
:: A value of safe (the default) will use a fast method which will not cut but may miss at most one repeat
:: A value of silencedetect will use a slower experimental silence detection method which will not cut and will not miss any repeat
:: Parameter : /autofill simple|safe|silencedetect
:: Default value : (empty, which is the same as safe)
set ARG_autofill=

:: Duration (seconds) for the autofill function.
:: The layers shorter than the given duration will be looped so they are nearly as long.
:: If empty, the duration will be calculated from the longest layer.
:: Parameter : /autofillduration 120
:: Default value : (empty, which means the duration of the longest layer)
set ARG_autofillduration=

:: Add some silence before and after the affirmations. The first duration (seconds) is the silence before, and the other is the silence after. If the other is missing it will be the same as the first.
:: Parameter : /pad duration1 duration2
:: Default value : (empty, which means no padding)
set ARG_pad=

:: Enable panning the layers from left to right so they do not overlap each other.
:: A value of panlaw-0 will use a neutral pan law that does not reduce the volume in the center.
:: A value of panlaw-3 will use a constant power pan law that reduces by 3dB the volume in the center.
:: A value of panlaw-4.5 will use a middleground pan law that reduces by 4.5dB the volume in the center.
:: A value of panlaw-6 will use a linear pan law that reduces by 6dB the volume in the center.
:: A value of rotation will use a stereo rotation effect.
:: A value of rotation-slow will use a slower stereo rotation effect.
:: A value of rotation-fast will use a faster stereo rotation effect.
:: Parameter : /pan panlaw-0|panlaw-3|panlaw-4.5|panlaw-6|rotation|rotation-slow|rotation-fast
:: Default value : (empty, which means no panning)
set ARG_pan=

:: Set the method used to normalize the audio.
:: A value of peak will use peak normalization. The normalization values (in decibels full-scale dBFS) will not be representative of the overall level or loudness (quiet sound, no risk of clipping).
:: A value of rms will use RMS normalization. The normalization values (in decibels full-scale dBFS) will be representative of the overall level but not of the perceived loudness (beware of clipping).
:: A value of r128 (the default) will use loudness normalization. The normalization values (in loudness units full-scale LUFS) will be representative of the overall perceived loudness (beware of clipping).
:: Parameter : /normwith peak|rms|r128
:: Default value : (empty, which is the same as r128)
set ARG_normwith=

:: Set the width used in the bandpass and bandreject filters (/affbandpass1, /affbandreject1...)
:: Ranges from 1 to 10000, with 1 being very steep (cutoff-like) and 100000 a soft roll-off.
:: Parameter : /bandfilterwidth 1
:: Default value : 1
set ARG_bandfilterwidth=1

:: Proxy host for online voices (bal4web), useful if you get rate limited
:: Parameter : /proxyhost 127.0.0.1
:: Default value : (empty, means no proxy)
set ARG_proxyhost=

:: Proxy port for online voices (bal4web), useful if you get rate limited
:: Parameter : /proxyport 8080
:: Default value : (empty, means no proxy)
set ARG_proxyport=

:: Enable displaying audio statistics during the generation.
:: Those are the duration, peak, RMS and R128 loudness of the files.
:: Parameter : /showstats
:: Default value : (empty, means do not display stats)
set ARG_showstats=

::::::::::::::::::
:: Audio output ::
::::::::::::::::::

:: Carrier frequency (hz) used in the ultrasonic resulting subliminal
:: Must be between 15000 and 20000
:: Parameter : /outputfileultrasonicfreq 15000
:: Default value : 17500
set ARG_outputfileultrasonicfreq=17500

:: Normalization (volume from 0 to -100) of the affirmations ultrasonic subliminal.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Parameter : /outputfileultrasonicnorm -30
:: Default value : (empty, which means no change in normalization)
set ARG_outputfileultrasonicnorm=

:: Normalization (volume from 0 to -100) of the affirmations test audio files.
:: 0 means full volume, -100 means extremely low volume.
:: The exact meaning and unit of this value depends of ARG_normwith.
:: Parameter : /outputfiletestaffsnorm -10
:: Default value : (empty, which means no change in normalization)
set ARG_outputfiletestaffsnorm=

::::::::::::::::::
:: Video output ::
::::::::::::::::::

:: Set the size of the video.
:: A value of auto (the default) will give it a height and an aspect ratio based on the source image.
:: A value of image will give it a target size equal to that of the source image.
:: A value of 240p will give it a target size of 426x240.
:: A value of 360p will give it a target size of 640x360.
:: A value of 480p will give it a target size of 854x480.
:: A value of 720p will give it a target size of 1280x720.
:: A value of 1080p will give it a target size of 1920x1080.
:: A value of 1440p will give it a target size of 2560x1440.
:: A value of 2160p will give it a target size of 3840x2160.
:: Parameter : /videosize auto|image|240p|360p|480p|720p|1080p|1440p|2160p
:: Default value : (empty, which is the same as auto)
set ARG_videosize=

:: Set the sizing mode of the video.
:: A value of auto (the default) will give it an aspect ratio similar to the source image.
:: A value of contain will fit it into the target aspect ratio and add black bars if necessary.
:: A value of cover will crop it into the target aspect ratio, potentially causing parts of the side to be left out.
:: A value of stretch will stretch it to the target aspect ratio.
:: Parameter : /videosizemode auto|contain|cover|stretch
:: Default value : (empty, which is the same as auto)
set ARG_videosizemode=

:: Set the audio encoding of the video.
:: A value of aac (the default and web standard) will use AAC CBR 320 audio (lossy compression, compatible with everything).
:: A value of flac will use FLAC audio (lossless compression, good compatibility).
:: A value of alac will use ALAC audio (lossless compression, compatible with YouTube and VLC).
:: Parameter : /videoaudioencoding aac|alac|flac
:: Default value : (empty, which is the same as aac)
set ARG_videoaudioencoding=

:: Set the delay (in seconds) before moving to the next image in videos.
:: Must be greater than 0.02.
:: Parameter : /slideshowdelay 1
:: Default value : 5
set ARG_slideshowdelay=5

::::::::::::::
:: Internal ::
::::::::::::::

:: Directory where cbsubgen will put temporary generated files.
:: Warning : the content of this folder will be DELETED on every run, so be careful
:: Parameter : /workdir path\to\directory
:: Default value : %SCRIPT_DIR%\tmp
set ARG_workdir=%SCRIPT_DIR%\tmp

:: Keep the content of the work directory after running ?
:: Any value different than 0 means yes.
:: Parameter : /keepworkdir
:: Default value : (empty, which means do not keep)
set ARG_keepworkdir=
