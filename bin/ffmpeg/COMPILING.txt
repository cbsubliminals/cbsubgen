This build of ffmpeg has been compiled by ChloeB using Media Autobuild Suite from https://github.com/m-ab-s/media-autobuild_suite/.

To update it you can use it like this :

- Download the master archive of MAS from https://github.com/m-ab-s/media-autobuild_suite/, and uncompress it somewhere with a short path (like c:\users\username\mas)
- Copy the ffmpeg_options.txt, media-autobuild_suite.ini and mpv_options.txt from this folder to the build/ folder of MAS
- Run media-autobuild_suite.bat from the MAS folder
- Once the process is finished, you can grab the files from MAS\local32\bin-video
