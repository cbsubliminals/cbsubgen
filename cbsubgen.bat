@echo off
setlocal EnableDelayedExpansion 

::::::::::::::::::::::::::::::::::::::::::::::::
:: cbsubgen v0.19 by ChloeB                   ::
:: Distributed according to the GPLv3 License ::
::::::::::::::::::::::::::::::::::::::::::::::::

:: Script path
set SCRIPT_DIR=%~dp0
if "%SCRIPT_DIR:~-1%"=="\" set SCRIPT_DIR=%SCRIPT_DIR:~0,-1%

:: Other path
set NYQUIST_DIR=%SCRIPT_DIR%\bin\nyquist
set EXE_BALCON=%SCRIPT_DIR%\bin\balcon\balcon.exe
set EXE_BAL4WEB=%SCRIPT_DIR%\bin\bal4web\bal4web.exe
set EXE_SOX=%SCRIPT_DIR%\bin\sox\sox.exe
set EXE_FFMPEG=%SCRIPT_DIR%\bin\ffmpeg\ffmpeg.exe
set EXE_FFPROBE=%SCRIPT_DIR%\bin\ffmpeg\ffprobe.exe
set EXE_LAME=%SCRIPT_DIR%\bin\lame\lame.exe
set EXE_NYQUIST=%SCRIPT_DIR%\bin\nyquist\nyquist.exe
set EXE_BC=%SCRIPT_DIR%\bin\bc\bc.exe
set EXE_LOUDNESS_SCANNER=%SCRIPT_DIR%\bin\loudness-scanner\loudness.exe
set EXE_SHA1SUM=%SCRIPT_DIR%\bin\hashutils\sha1sum.exe
set BC_FUNCTIONS=%SCRIPT_DIR%\bc\functions.bc
set JS_SPLIT=%SCRIPT_DIR%\js\split.js
set LSP_ULTRASONIC=%SCRIPT_DIR%\lsp\ultrasonic.lsp
set LSP_ROTATION=%SCRIPT_DIR%\lsp\rotation.lsp
set IMG_DEFAULT1080P=%SCRIPT_DIR%\img\default1080p.png

:: Other constants
set MAX_LAYER_COUNT=50
set ROTATION_SLOW=0.1
set ROTATION_NORMAL=0.5
set ROTATION_FAST=2

:: Macros
set EXECUTE_SOX="%EXE_SOX%" -V1 --multi-threaded --buffer 1048576
set EXECUTE_SOXI="%EXE_SOX%" --i -V1

:: Load default configuration
call "%SCRIPT_DIR%\config.bat"

:: Parse command line parameters
call :parse_command_line_parameters %*

:: Show usage information if needed
set showUsage=0
if "%ARG_help%"=="1" set showUsage=1
if not "%showUsage%"=="1" (
  if "%ARG_aff1%"=="" (
    echo MISSING PARAMETER : /aff1
    set showUsage=1
  )
  if "%ARG_outputfile%"=="" (
    if "%ARG_outputfilenobg%"=="" (
      if "%ARG_outputfileultrasonic%"=="" (
        if "%ARG_outputfiletestaffs%"=="" (
          if "%ARG_outputvideo%"=="" (
            if "%ARG_outputvideonobg%"=="" (
              if "%ARG_outputvideoultrasonic%"=="" (
                echo MISSING PARAMETER : at least one of /outputfile, /outputfilenobg, /outputfileultrasonic, /outputfiletestaffs, /outputvideo, /outputvideonobg, /outputvideoultrasonic
                set showUsage=1
              )
            )
          )
        )
      )
    )
  )
  if "!showUsage!"=="1" echo:
)
IF "%showUsage%"=="0" goto usage_done
echo Usage : cbsubgen.bat /aff1 file.txt [more options]
echo:
echo Required affirmations layers parameters :
echo /aff1 file.txt : Text/Audio file with affirmations.
echo:
echo Optional affirmations layers parameters :
echo (Except /affN, parameters are inherited from the first layer of the group or the global first layer if they are missing.
echo N varies between 1 and %MAX_LAYER_COUNT%, for example /affvoice1 or /affspeed%MAX_LAYER_COUNT%.)
echo /affN file.txt : Text/Audio file with affirmations of the Nth layer.
echo /affvoiceN name : Name (or part) of the voice used for the Nth layer. Find it in your Windows TTS settings (ex : Zira).
echo /affspeedN speed : Speed multiplier for the voice used for the Nth layer. Changes both the speed and the pitch.
echo /afftempoN tempo : Tempo multiplier for the voice used for the Nth layer. Changes the speed but keeps the pitch.
echo /affpitchN number^|auto[number] : Pitch for the voice used for the Nth layer. Either a number representing an amount of 1/100th of semitones, or automatically calculated with the possibility of specifying the interval between pitches.
echo /affbandpassN number number : Apply a bandpass filter to the Nth layer, which will only keep the audio in the given frequency (hz) range (ex : 3500 10000). Values must be between 0 and 24000.
echo /affbandrejectN number number : Apply a bandreject filter to the Nth layer, which will delete the audio in the given frequency (hz) range (ex : 12000 24000). Values must be between 0 and 24000.
echo /affnormN normalization : Normalization (from 0 to -100) of the voice used for the Nth layer. The meaning and unit depends of the value of /normwith.
echo /affloopN number : Number of times the Nth layer affirmations will be repeated.
echo /affultrasonicN number : Carrier frequency if the Nth layer is ultrasonic.
echo /affgroupN groupname : Name of the group the Nth layer belongs to. Layers in the same group share inheritance of settings and are panned together.
echo:
echo Optional frequency settings :
echo /leftfreq number : Frequency (hz) to be generated in the left channel.
echo /rightfreq number : Frequency (hz) to be generated in the right channel. Fallbacks to left channel settings.
echo /freqnorm number : Normalization (from 0 to -100) of the frequency. The meaning and unit depends of the value of /normwith.
echo /freqpulse number : Duration (s) of a pulse in case of an intermittent frequency (isochronic tone).
echo /freqgap number : Gap (s) between pulses in case of an intermittent frequency (isochronic tone).
echo /freqfade duration : Duration (s) for the fade effect at the beginning and the end of the frequency or of each pulse.
echo /freqrotation normal^|slow^|fast : Apply a stereo rotation effect of the given speed to the frequency.
echo:
echo Optional noise generation settings :
echo /noise white^|pink^|brown : Generate the given type of noise.
echo /noisenorm normalization : Normalization (from 0 to -100) of the noise. The meaning and unit depends of the value of /normwith.
echo /noisefade duration : Duration (seconds) for the fade effect at the beginning and the end of the noise.
echo:
echo Optional background parameters :
echo /background file.wav : Audio file which will be used as a background.
echo /backgroundnorm normalization : Normalization (from 0 to -100) of the background music. The meaning and unit depends of the value of /normwith.
echo /fade duration : Duration (seconds) for the fade effect at the beginning and the end of the background music.
echo /fadeloop : If present, also fade the background music when it is looped.
echo /backgroundrotation normal^|slow^|fast : Apply a stereo rotation effect of the given speed to the background music.
echo:
echo Optional audio output parameters :
echo /outputfile file.wav : WAV/FLAC/MP3 file which will contain the resulting subliminal. WAV files have a size limitation of around 4gib, a FLAC file will be generated instead if it is reached.
echo /outputfilenobg affsonly.wav : WAV/FLAC/MP3 file which will contain the resulting subliminal without background audio
echo /outputfileultrasonic ultrasonic.wav : WAV/FLAC/MP3 file which will contain the resulting subliminal without background at ultrasonic frequencies.
echo /outputfileultrasonicfreq number : Carrier frequency (hz, use 15000+) used in the ultrasonic resulting subliminal.
echo /outputfileultrasonicnorm normalization : Normalization (from 0 to -100) of the ultrasonic resulting subliminal. The meaning and unit depends of the value of /normwith.
echo /outputfiletestaffs testaffs.wav : WAV/FLAC/MP3 file which will contain audio for testing the affirmations. Several files will be generated (one per layer), following the pattern name1.wav, name2.wav, etc
echo /outputfiletestaffsnorm normalization : Normalization (from 0 to -100) of the affirmations test audio files. The meaning and unit depends of the value of /normwith.
echo:
echo Optional video output parameters :
echo /outputvideo subliminal.mp4 : MP4 file which will contain the resulting subliminal video.
echo /outputvideoimage video.jpg : JPEG/PNG image(s) used as background for the resulting subliminal video. Either the path to an image file, or to a folder. Fallbacks to a black 1080p image.
echo /outputvideonobg affsonly.mp4 : MP4 file which will contain the resulting subliminal video without background audio.
echo /outputvideonobgimage video.jpg : JPEG/PNG image(s) used as background for the resulting subliminal video without background audio. Either the path to an image file, or to a folder. Fallbacks to the image set in /outputvideoimage.
echo /outputvideoultrasonic ultrasonic.mp4 : MP4 file which will contain the resulting subliminal video without background at ultrasonic frequencies.
echo /outputvideoultrasonicimage video.jpg : JPEG/PNG image(s) used as background for the resulting subliminal video without background at ultrasonic frequencies. Either the path to an image file, or to a folder. Fallbacks to the image set in /outputvideoimage.
echo /videosize auto^|image^|240p^|360p^|480p^|720p^|1080p^|1440p^|2160p : Set the size of the videos. Either automatically calculated from the source image (default), equal to the size of the source image, or with the given height (240p..2160p).
echo /videosizemode auto^|contain^|cover^|stretch : Set the sizing mode of the videos. Either automatically from the source image aspect ratio (default), or contained into the target aspect ratio with black bars, cropped into the target aspect ratio or stretched into the target aspect ratio.
echo /videoaudioencoding aac^|alac^|flac : Set the audio encoding of the videos. Either AAC (the default, compatible with everything but lossy compressed), or FLAC or ALAC (less compatible but losslessly compressed).
echo /slideshowdelay duration : Duration (seconds) before moving to the next image in the videos. Must be greater than 0.02.
echo:
echo Optional other parameters :
echo /autofill simple^|safe^|silencedetect : Set how to loop the affirmations of the shorter layers so they are as long as the duration given in /autofillduration. Either using a simple method which may cut in the middle of an affirmation, using a safe method which won't but which will miss at most one repeat (the default), or using a slower experimental silence detection method which won't. If a layer is based on an audio file, it will always use the simple or safe method.
echo /autofillduration duration : Duration (seconds) used by /autofill. If empty, it will be calculated from the longest layer.
echo /pad duration1 duration2 : Add some silence before and after the affirmations. The first duration (seconds) is the silence before, and the other is the silence after. If the other is missing it will be the same as the first.
echo /pan panlaw-0^|panlaw-3^|panlaw-4.5^|panlaw-6^|rotation^|rotation-slow^|rotation-fast : Enable panning the layers from left to right so they do not overlap each other. Uses either a pan law reducing the volume of the center by 0dB, 3dB, 4.5dB or 6dB, or a stereo rotation effect.
echo /normwith peak^|rms^|r128 : Set the method used when normalizing audio. Either using peak (quieter audio, no clipping), RMS (louder audio, potentially clipping), or loudness (the default, louder audio, potentially clipping).
echo /bandfilterwidth number : Set the width for the band filters (see /affbandpass1, /affbandreject1). Ranges from 1 (the default, steep cut-off like filter) to 100000 (soft roll-off filter).
echo /proxyhost 127.0.0.1 : Proxy host for online voices, useful if you get rate limited
echo /proxyport 8080 : Proxy port for online voices, useful if you get rate limited
echo /showstats : Display audio statistics during the generation.
echo /workdir path\to\directory : Path to the directory where cbsubgen will generate temporary files. Warning : the content of this folder will be deleted on every run.
echo /keepworkdir : If present, do not empty the workdir folder after processing.
goto :eof
:usage_done

:: Ensure we have a work directory, and clean it
if "%ARG_workdir:~-1%"=="\" set ARG_workdir=%ARG_workdir:~0,-1%
if "%ARG_keepworkdir%"=="0" set ARG_keepworkdir=
if "%ARG_workdir%"=="" (
  echo FATAL ERROR : Empty work directory /workdir setting.
  goto :eof
)
if not exist "%ARG_workdir%" (
  mkdir "%ARG_workdir%" 2> NUL
  if not exist "%ARG_workdir%" (
    echo FATAL ERROR : The work directory %ARG_workdir% does not exist.
    goto :eof
  )
)
call :cleanup_workdir

:: Fix some parameters
for /L %%i in (1,1,%MAX_LAYER_COUNT%) do (
  set group=!ARG_affgroup%%i!
  call :get_string_sha1 "!group!" group
  set group=g!group!
  set ARG_affgroup%%i=!group!
  call set groupCount=%%!group!_count%%
  if "!groupCount!"=="" (
    set !group!_count=1
    if not "%%i"=="1" (
      if "!ARG_affvoice%%i!"=="" set ARG_affvoice%%i=!ARG_affvoice1!
      if "!ARG_affspeed%%i!"=="" set ARG_affspeed%%i=!ARG_affspeed1!
      if "!ARG_afftempo%%i!"=="" set ARG_afftempo%%i=!ARG_afftempo1!
      if "!ARG_affpitch%%i!"=="" set ARG_affpitch%%i=!ARG_affpitch1!
      if "!ARG_affbandpass%%i!"=="" set ARG_affbandpass%%i=!ARG_affbandpass1!
      if "!ARG_affbandreject%%i!"=="" set ARG_affbandreject%%i=!ARG_affbandreject1!
      if "!ARG_affnorm%%i!"=="" set ARG_affnorm%%i=!ARG_affnorm1!
      if "!ARG_affloop%%i!"=="" set ARG_affloop%%i=!ARG_affloop1!
      if "!ARG_affultrasonic%%i!"=="" set ARG_affultrasonic%%i=!ARG_affultrasonic1!
    )
    set !group!_affvoice=!ARG_affvoice%%i!
    set !group!_affspeed=!ARG_affspeed%%i!
    set !group!_afftempo=!ARG_afftempo%%i!
    set !group!_affpitch=!ARG_affpitch%%i!
    set !group!ARG_affbandpass=!ARG_affbandpass%%i!
    set !group!ARG_affbandreject=!ARG_affbandreject%%i!
    set !group!_affnorm=!ARG_affnorm%%i!
    set !group!_affloop=!ARG_affloop%%i!
    set !group!_affultrasonic=!ARG_affultrasonic%%i!
  ) else (
    set /a "!group!_count=!groupCount!+1"
    if "!ARG_affvoice%%i!"=="" call set ARG_affvoice%%i=%%!group!_affvoice%%
    if "!ARG_affspeed%%i!"=="" call set ARG_affspeed%%i=%%!group!_affspeed%%
    if "!ARG_afftempo%%i!"=="" call set ARG_afftempo%%i=%%!group!_afftempo%%
    if "!ARG_affpitch%%i!"=="" call set ARG_affpitch%%i=%%!group!_affpitch%%
    if "!ARG_affbandpass%%i!"=="" call set ARG_affbandpass%%i=%%!group!_affbandpass%%
    if "!ARG_affbandreject%%i!"=="" call set ARG_affbandreject%%i=%%!group!_affbandreject%%
    if "!ARG_affnorm%%i!"=="" call set ARG_affnorm%%i=%%!group!_affnorm%%
    if "!ARG_affloop%%i!"=="" call set ARG_affloop%%i=%%!group!_affloop%%
    if "!ARG_affultrasonic%%i!"=="" call set ARG_affultrasonic%%i=%%!group!_affultrasonic%%
  )
  if "!ARG_affspeed%%i!"=="0" set ARG_affspeed%%i=
  if "!ARG_affspeed%%i!"=="1" set ARG_affspeed%%i=
  if "!ARG_afftempo%%i!"=="0" set ARG_afftempo%%i=
  if "!ARG_afftempo%%i!"=="1" set ARG_afftempo%%i=
  if "!ARG_affpitch%%i!"=="0" set ARG_affpitch%%i=
  if "!ARG_affbandpass%%i!"=="0" set ARG_affbandpass%%i=
  if not "!ARG_affbandpass%%i!"=="" (
    call :split_string "!ARG_affbandpass%%i!" " " bandpass
    if "!bandpass[2]!"=="" set ARG_affbandpass%%i=
  )
  if "!ARG_affbandreject%%i!"=="0" set ARG_affbandreject%%i=
  if not "!ARG_affbandreject%%i!"=="" (
    call :split_string "!ARG_affbandreject%%i!" " " bandreject
    if "!bandreject[2]!"=="" set ARG_affbandreject%%i=
  )
  if not "!ARG_affnorm%%i!"=="" (
    if not "!ARG_affnorm%%i:~0,1!"=="-" set ARG_affnorm%%i=0
  )
  if "!ARG_affloop%%i!"=="0" set ARG_affloop%%i=
  if "!ARG_affultrasonic%%i!"=="0" set ARG_affultrasonic%%i=
)
if "%ARG_leftfreq%"=="0" set ARG_leftfreq=
if "%ARG_rightfreq%"=="" set ARG_rightfreq=%ARG_leftfreq%
if "%ARG_rightfreq%"=="0" set ARG_rightfreq=
if not "%ARG_freqnorm%"=="" (
  if not "%ARG_freqnorm:~0,1%"=="-" set ARG_freqnorm=0
)
if "%ARG_freqpulse%"=="0" set ARG_freqpulse=
if "%ARG_freqgap%"=="0" set ARG_freqgap=
if "%ARG_freqfade%"=="0" set ARG_freqfade=
if not "%ARG_noisenorm%"=="" (
  if not "%ARG_noisenorm:~0,1%"=="-" set ARG_noisenorm=0
)
if "%ARG_noisefade%"=="0" set ARG_noisefade=
if "%ARG_fade%"=="0" set ARG_fade=
if "%ARG_fadeloop%"=="0" set ARG_fadeloop=
if not "%ARG_outputfileultrasonicnorm%"=="" (
  if not "%ARG_outputfileultrasonicnorm:~0,1%"=="-" set ARG_outputfileultrasonicnorm=0
)
if "%ARG_autofillduration%"=="0" set ARG_autofillduration=
if "%ARG_pad%"=="0" set ARG_pad=
if "%ARG_pad%"=="0 0" set ARG_pad=
if "%ARG_bandfilterwidth%"=="" set ARG_bandfilterwidth=1
if "%ARG_showstats%"=="0" set ARG_showstats=
if not "%ARG_outputfiletestaffsnorm%"=="" (
  if not "%ARG_outputfiletestaffsnorm:~0,1%"=="-" set ARG_outputfiletestaffsnorm=0
)
if "%ARG_outputvideoimage%"=="" set ARG_outputvideoimage=%IMG_DEFAULT1080P%
if "%ARG_outputvideonobgimage%"=="" set ARG_outputvideonobgimage=%ARG_outputvideoimage%
if "%ARG_outputvideoultrasonicimage%"=="" set ARG_outputvideoultrasonicimage=%ARG_outputvideoimage%
if not "%ARG_slideshowdelay%"=="" (
  call :compare_decimal "%ARG_slideshowdelay%" 0.02
  if !errorlevel! LSS 0 set ARG_slideshowdelay=0.02
)

:: Generate the layers
set WAV_LAYERS=%ARG_workdir%\layers.w64
set WAV_LAYERS_ULTRASONIC=%ARG_workdir%\preconvertultrasonic.w64
if "%ARG_outputfileultrasonic%"=="" (
  if "%ARG_outputvideoultrasonic%"=="" set WAV_LAYERS_ULTRASONIC=
)
set layerCount=0
for /L %%i in (1,1,%MAX_LAYER_COUNT%) do (
  if not "!ARG_aff%%i!"=="" (
    set /a "layerCount=!layerCount!+1"
    set affs[!layerCount!]=!ARG_aff%%i!
    set affVoices[!layerCount!]=!ARG_affvoice%%i!
    set affSpeeds[!layerCount!]=!ARG_affspeed%%i!
    set affTempos[!layerCount!]=!ARG_afftempo%%i!
    set affPitches[!layerCount!]=!ARG_affpitch%%i!
    set affBandPasses[!layerCount!]=!ARG_affbandpass%%i!
    set affBandRejects[!layerCount!]=!ARG_affbandreject%%i!
    set affNorms[!layerCount!]=!ARG_affnorm%%i!
    set affLoops[!layerCount!]=!ARG_affloop%%i!
    set affUltrasonics[!layerCount!]=!ARG_affultrasonic%%i!
    set affGroups[!layerCount!]=!ARG_affgroup%%i!
  )
)
call :process_layers "%WAV_LAYERS%" "%WAV_LAYERS_ULTRASONIC%" "%ARG_outputfileultrasonicfreq%" "%ARG_outputfileultrasonicnorm%" "%ARG_autofill%" "%ARG_autofillduration%" "%ARG_pad%" "%ARG_pan%" "%ARG_normwith%" "%ARG_bandfilterwidth%" "%ARG_outputfiletestaffs%" "%ARG_outputfiletestaffsnorm%" affs affVoices affSpeeds affTempos affPitches affBandPasses affBandRejects affNorms affLoops affUltrasonics affGroups

:: Calculate the duration of the complete subliminal
:: It is either the duration of the layers or the duration of the autofill, whatever is the highest
:: This allows for autofilled subliminals to have an exact duration
:: The duration is rounded to the closest higher value because it allows videos to be cut closer to the actual duration of the audio
call :get_audio_file_duration "%WAV_LAYERS%" LAYERS_DURATION
set COMPLETE_DURATION=%LAYERS_DURATION%
call :ceil_decimal "%COMPLETE_DURATION%" COMPLETE_DURATION
if not "%ARG_autofillduration%"=="" (
  call :compare_decimal "%ARG_autofillduration%" "%COMPLETE_DURATION%"
  if !errorlevel! GTR 0 set COMPLETE_DURATION=%ARG_autofillduration%
)

:: Generate the frequencies
set WAV_FREQS=%ARG_workdir%\freqs.w64
set hasFreqs=
if not "%ARG_leftfreq%"=="" set hasFreqs=1
if not "%ARG_rightfreq%"=="" set hasFreqs=1
if "%hasFreqs%"=="1" (
  call :process_frequencies "%WAV_FREQS%" "%COMPLETE_DURATION%" "%ARG_leftfreq%" "%ARG_rightfreq%" "%ARG_freqnorm%" "%ARG_normwith%" "%ARG_freqpulse%" "%ARG_freqgap%" "%ARG_freqfade%" "%ARG_freqrotation%"
  call :process_display_audio_stats "%WAV_FREQS%" "Frequency"
)

:: Generate the noise
set WAV_NOISE=%ARG_workdir%\noise.w64
set hasNoise=
if not "%ARG_noise%"=="" set hasNoise=1
if "%hasNoise%"=="1" (
  call :process_noise "%WAV_NOISE%" "%ARG_noise%" "%COMPLETE_DURATION%" "%ARG_noisenorm%" "%ARG_normwith%" "%ARG_noisefade%"
  call :process_display_audio_stats "%WAV_NOISE%" "Noise"
)

:: Generate the background audio
set WAV_BG=%ARG_workdir%\bg.w64
set hasBg=
if not "%ARG_background%"=="" set hasBg=1
if "%hasBg%"=="1" (
  call :process_background "%WAV_BG%" "%ARG_background%" "%COMPLETE_DURATION%" "%ARG_backgroundnorm%" "%ARG_normwith%" "%ARG_fade%" "%ARG_fadeloop%" "%ARG_backgroundrotation%"
  call :process_display_audio_stats "%WAV_BG%" "Background"
)

:: Create the no-background output file and video if needed
if not "%ARG_outputfilenobg%"=="" (
  call :process_export_audio_file "%ARG_outputfilenobg%" "%WAV_LAYERS%"
  call :process_display_audio_stats "%ARG_outputfilenobg%" "Affirmations-only audio subliminal"
)
if not "%ARG_outputvideonobg%"=="" (
  call :process_video_output "%ARG_outputvideonobg%" "%WAV_LAYERS%" "%ARG_outputvideonobgimage%" "%ARG_videosize%" "%ARG_videosizemode%" "%ARG_videoaudioencoding%" "%ARG_slideshowdelay%"
  call :process_display_video_stats "%ARG_outputvideonobg%" "Affirmations-only video subliminal"
)

:: Create the ultrasonic output file and video if needed
if not "%WAV_LAYERS_ULTRASONIC%"=="" (
  if not "%ARG_outputfileultrasonic%"=="" (
    call :process_export_audio_file "%ARG_outputfileultrasonic%" "%WAV_LAYERS_ULTRASONIC%"
    call :process_display_audio_stats "%ARG_outputfileultrasonic%" "Ultrasonic affirmations audio subliminal"
  )
  if not "%ARG_outputvideoultrasonic%"=="" (
    call :process_video_output "%ARG_outputvideoultrasonic%" "%WAV_LAYERS_ULTRASONIC%" "%ARG_outputvideoultrasonicimage%" "%ARG_videosize%" "%ARG_videosizemode%" "%ARG_videoaudioencoding%" "%ARG_slideshowdelay%"
    call :process_display_video_stats "%ARG_outputvideoultrasonic%" "Ultrasonic affirmations video subliminal"
  )
  call :maybe_delete_workfile "%WAV_LAYERS_ULTRASONIC%"
)

:: Create the complete output file and video
set mixParams="%WAV_LAYERS%"
if "%hasFreqs%"=="1" set mixParams=%mixParams% "%WAV_FREQS%"
if "%hasNoise%"=="1" set mixParams=%mixParams% "%WAV_NOISE%"
if "%hasBg%"=="1" set mixParams=%mixParams% "%WAV_BG%"
set WAV_PRECONVERT=%ARG_workdir%\preconvert.w64
call :mix_audio_files "%WAV_PRECONVERT%" %mixParams%
call :maybe_delete_workfile "%WAV_BG%"
call :maybe_delete_workfile "%WAV_NOISE%"
call :maybe_delete_workfile "%WAV_FREQS%"
call :maybe_delete_workfile "%WAV_LAYERS%"
if not "%ARG_outputfile%"=="" (
  call :process_export_audio_file "%ARG_outputfile%" "%WAV_PRECONVERT%"
  call :process_display_audio_stats "%ARG_outputfile%" "Complete audio subliminal"
)
if not "%ARG_outputvideo%"=="" (
  call :process_video_output "%ARG_outputvideo%" "%WAV_PRECONVERT%" "%ARG_outputvideoimage%" "%ARG_videosize%" "%ARG_videosizemode%" "%ARG_videoaudioencoding%" "%ARG_slideshowdelay%"
  call :process_display_video_stats "%ARG_outputvideo%" "Complete video subliminal"
)
call :maybe_delete_workfile "%WAV_PRECONVERT%"

:: Cleanup the work directory if needed
:maybe_cleanup_workdir_and_exit
if "%ARG_keepworkdir%"=="" call :cleanup_workdir

:: This is the end, beautiful friends
goto :eof

:::::::::::::
:: Process ::
:::::::::::::

:: Process all the layers of affirmations
:: process_layers "path\output.w64" "path\outputultrasonic.w64" outputUltrasonicFreq outputUltrasonicNorm autofill autofilldurationSec  padding panmethod normwith bandfilterwidth testaffsOutput testaffsNorm affArray voiceArray speedArray tempoArray pitchArray bandpassArray bandrejectArray normArray loopArray ultrasonicArray groupsArray
:process_layers
setlocal EnableDelayedExpansion
set output=%~1
set outputUltrasonic=%~2
set outputUltrasonicFreq=%~3
set outputUltrasonicNorm=%~4
set autofill=%~5
set autofillduration=%~6
set padding=%~7
set panmethod=%~8
set normwith=%~9
shift
set bandfilterwidth=%~9
shift
set testaffsOutput=%~9
shift
set testaffsNorm=%~9
shift
set affArray=%~9
shift
set voiceArray=%~9
shift
set speedArray=%~9
shift
set tempoArray=%~9
shift
set pitchArray=%~9
shift
set bandpassArray=%~9
shift
set bandrejectArray=%~9
shift
set normArray=%~9
shift
set loopArray=%~9
shift
set ultrasonicArray=%~9
shift
set groupsArray=%~9

call :split_string "%padding%" " " padding
set paddingStart=%padding[1]%
set paddingEnd=%padding[2]%
if "%paddingStart%"=="" set paddingStart=0
if "%paddingEnd%"=="" set paddingEnd=%paddingStart%
if not "%autofillduration%"=="" (
  call :calculate "!autofillduration!-%paddingStart%-%paddingEnd%" autofillduration
  call :compare_decimal "!autofillduration!" 0
  if !errorlevel! LSS 0 set autofillduration=
)

call :get_array_length %affArray% layerCount

call :process_calculate_autopitch %layerCount% %voiceArray% %pitchArray% %pitchArray%

set canApplyEffectsEarly=1
if "%autofill%"=="silencedetect" set canApplyEffectsEarly=
if "%canApplyEffectsEarly%"=="1" (
  call :_process_layers_early_effects %*
) else (
  call :_process_layers_late_effects %*
)

endlocal
exit /b

:: Internal part of process_layers, do not call directly
:: Slow implementation used when the effects cannot be applied early (eg silencedetect is used)
:_process_layers_late_effects

for /L %%i in (1,1,%layerCount%) do (
  set affoutput=%ARG_workdir%\layergen%%i.w64
  set afftext=!%affArray%[%%i]!
  set affvoice=!%voiceArray%[%%i]!
  call :process_generate_affirmations "!affoutput!" "!afftext!" "!affvoice!"
  set baseLayers[%%i]=!affoutput!
)

for /L %%i in (1,1,%layerCount%) do (
  set affinput=!baseLayers[%%i]!
  set affoutput=%ARG_workdir%\layerloop%%i.w64
  set affloops=!%loopArray%[%%i]!
  call :process_affirmations_loop "!affoutput!" "!affinput!" "!affloops!"
  set loopedLayers[%%i]=!affoutput!
)

call :process_calculate_autofill_duration_at_normal_speed loopedLayers "%speedArray%" "%tempoArray%" "%autofillduration%" autofillduration
for /L %%i in (1,1,%layerCount%) do (
  set baseLayer=!baseLayers[%%i]!
  set loopedLayer=!loopedLayers[%%i]!
  set layerSpeed=!%speedArray%[%%i]!
  if "!layerSpeed!"=="" set layerSpeed=1
  set layerTempo=!%tempoArray%[%%i]!
  if "!layerTempo!"=="" set layerTempo=1
  call :calculate "%autofillduration%*!layerSpeed!*!layerTempo!" layerAutofillDuration
  set affoutput=%ARG_workdir%\layerautofill%%i.w64
  set autofillmethod=%autofill%
  set afftext=!%affArray%[%%i]!
  call :string_ends_with "!afftext!" ".txt" isTXT
  if not "!isTXT!"=="1" call :string_ends_with "!afftext!" ".TXT" isTXT
  if not "!isTXT!"=="1" (
    if "!autofillmethod!"=="silencedetect" set autofillmethod=safe
  )
  call :process_affirmations_autofill "!affoutput!" "!baseLayer!" "!loopedLayer!" "!layerAutofillDuration!" "%%i" "!autofillmethod!"
  call :maybe_delete_workfile "!baseLayer!"
  call :maybe_delete_workfile "!loopedLayer!"
  set autofilledLayers[%%i]=!affoutput!
)

for /L %%i in (1,1,%layerCount%) do (
  set affinput=!autofilledLayers[%%i]!
  set affoutput=%ARG_workdir%\layer%%i.w64
  set affspeed=!%speedArray%[%%i]!
  set afftempo=!%tempoArray%[%%i]!
  set affpitch=!%pitchArray%[%%i]!
  set affbandpass=!%bandpassArray%[%%i]!
  set affbandreject=!%bandrejectArray%[%%i]!
  set affnorm=!%normArray%[%%i]!
  call :process_affirmations_effects "!affoutput!" "!affinput!" "!affspeed!" "!afftempo!" "!affpitch!" "!affbandpass!" "!affbandreject!" "!affnorm!" "%normwith%" "%bandfilterwidth%" "%paddingStart%" "%paddingEnd%"
  call :maybe_delete_workfile "!affinput!"
  set processedLayers[%%i]=!affoutput!
)

for /L %%i in (1,1,%layerCount%) do (
  set affinput=!processedLayers[%%i]!
  if not "%testaffsOutput%"=="" (
    call :process_affirmations_test_output "%testaffsOutput%" "!affinput!" "%testaffsNorm%" "%normwith%" "%%i"
  )
)

for /L %%i in (1,1,%layerCount%) do (
  set affinput=!processedLayers[%%i]!
  set affoutput=%ARG_workdir%\layer%%iu.w64
  set affnorm=!%normArray%[%%i]!
  set affultrasonic=!%ultrasonicArray%[%%i]!
  if not "!affultrasonic!"=="" (
    call :process_affirmations_ultrasonic "!affoutput!" "!affinput!" "!affultrasonic!" "!affnorm!" "%normwith%"
    set ultrasonicLayers[%%i]=!affoutput!
  )
)

for /L %%i in (1,1,%layerCount%) do (
  set affultrasonic=!%ultrasonicArray%[%%i]!
  if not "!affultrasonic!"=="" (
    set affinput=!ultrasonicLayers[%%i]!
    call :process_display_audio_stats "!affinput!" "Layer %%i ^^^(ultrasonic^^^)"
  ) else (
    set affinput=!processedLayers[%%i]!
    call :process_display_audio_stats "!affinput!" "Layer %%i"
  )
)

if not "%outputUltrasonic%"=="" (
  set layerList=
  for /L %%i in (1,1,%layerCount%) do set layerList=!layerList! "!processedLayers[%%i]!"
  set affoutput=%ARG_workdir%\layerswithoutultrasonics.w64
  call :generate_panned_audio_file "!affoutput!" "%panmethod%" !layerList!
  call :process_ultrasonic_output "%outputUltrasonic%" "!affoutput!" "%outputUltrasonicFreq%" "%outputUltrasonicNorm%" "%normwith%"
  call :maybe_delete_workfile "!affoutput!"
)

set groupCount=0
for /L %%i in (1,1,%layerCount%) do (
  set group=!%groupsArray%[%%i]!
  set affinputs=affinputs_!group!
  set affoutputs=affoutputs_!group!
  call :get_array_length !affinputs! countPerGroup
  if "!countPerGroup!"=="0" (
    set /a "groupCount=!groupCount!+1"
    set groups[!groupCount!]=!group!
  )
  set /a "countPerGroup=!countPerGroup!+1"
  set affultrasonic=!%ultrasonicArray%[%%i]!
  if not "!affultrasonic!"=="" (
    set affinput=!ultrasonicLayers[%%i]!
    call :maybe_delete_workfile "!processedLayers[%%i]!"
  ) else (
    set affinput=!processedLayers[%%i]!
    call :maybe_delete_workfile "!ultrasonicLayers[%%i]!"
  )
  set !affinputs![!countPerGroup!]=!affinput!
  if "!countPerGroup!"=="1" set !affoutputs!=%ARG_workdir%\group!groupCount!.w64
)
for /L %%i in (1,1,%groupCount%) do (
  set group=!groups[%%i]!
  set affinputs=affinputs_!group!
  set affoutputs=affoutputs_!group!
  call :get_array_length !affinputs! countPerGroup
  set layerList=
  for /L %%j in (1,1,!countPerGroup!) do (
    call set affinput=%%!affinputs![%%j]%%
    set layerList=!layerList! "!affinput!"
  )
  call set affoutput=%%!affoutputs!%%
  call :generate_panned_audio_file "!affoutput!" "%panmethod%" !layerList!
  if not "!groupCount!"=="1" call :process_display_audio_stats "!affoutput!" "Group %%i"
  for /L %%j in (1,1,!countPerGroup!) do (
    call set affinput=%%!affinputs![%%j]%%
    call :maybe_delete_workfile "!affinput!"
  )
)
set layerList=
for /L %%i in (1,1,%groupCount%) do (
  set group=!groups[%%i]!
  set affoutputs=affoutputs_!group!
  call set affoutput=%%!affoutputs!%%
  set layerList=!layerList! "!affoutput!"
)
call :mix_audio_files "%output%" %layerList%
for /L %%i in (1,1,%groupCount%) do (
  set group=!groups[%%i]!
  set affoutputs=affoutputs_!group!
  call set affoutput=%%!affoutputs!%%
  call :maybe_delete_workfile "!affoutput!"
)

exit /b

:: Internal part of process_layers, do not call directly
:: Fast implementation used when the effects can be applied early (eg silencedetect is not used)
:_process_layers_early_effects

for /L %%i in (1,1,%layerCount%) do (
  set affoutput=%ARG_workdir%\layergen%%i.w64
  set afftext=!%affArray%[%%i]!
  set affvoice=!%voiceArray%[%%i]!
  call :process_generate_affirmations "!affoutput!" "!afftext!" "!affvoice!"
  set baseLayers[%%i]=!affoutput!
)

for /L %%i in (1,1,%layerCount%) do (
  set affinput=!baseLayers[%%i]!
  set affoutput=%ARG_workdir%\layerearlyeffects%%i.w64
  set affspeed=!%speedArray%[%%i]!
  set afftempo=!%tempoArray%[%%i]!
  set affpitch=!%pitchArray%[%%i]!
  set affbandpass=!%bandpassArray%[%%i]!
  set affbandreject=!%bandrejectArray%[%%i]!
  set affnorm=!%normArray%[%%i]!
  call :process_affirmations_effects "!affoutput!" "!affinput!" "!affspeed!" "!afftempo!" "!affpitch!" "!affbandpass!" "!affbandreject!" "!affnorm!" "%normwith%" "%bandfilterwidth%"
  call :maybe_delete_workfile "!affinput!"
  set baseLayers[%%i]=!affoutput!

  set affinput=!baseLayers[%%i]!
  set affoutput=%ARG_workdir%\layerearlyeffects%%iu.w64
  set affnorm=!%normArray%[%%i]!
  set affultrasonic=!%ultrasonicArray%[%%i]!
  if not "!affultrasonic!"=="" (
    call :process_affirmations_ultrasonic "!affoutput!" "!affinput!" "!affultrasonic!" "!affnorm!" "%normwith%"
    set baseLayersU[%%i]=!affoutput!
  )

  set affinput=!baseLayers[%%i]!
  set affoutput=%ARG_workdir%\layerearlyeffects%%iuo.w64
  if not "%outputUltrasonic%"=="" (
    call :process_affirmations_ultrasonic "!affoutput!" "!affinput!" "%outputUltrasonicFreq%" "%outputUltrasonicNorm%" "%normwith%"
    set baseLayersUO[%%i]=!affoutput!
  )
)

for /L %%i in (1,1,%layerCount%) do (
  set affloops=!%loopArray%[%%i]!
  
  set affinput=!baseLayers[%%i]!
  set affoutput=%ARG_workdir%\layerloop%%i.w64
  if not "!affinput!"=="" (
    call :process_affirmations_loop "!affoutput!" "!affinput!" "!affloops!"
    set loopedLayers[%%i]=!affoutput!
  )

  set affinput=!baseLayersU[%%i]!
  set affoutput=%ARG_workdir%\layerloop%%iu.w64
  if not "!affinput!"=="" (
    call :process_affirmations_loop "!affoutput!" "!affinput!" "!affloops!"
    set loopedLayersU[%%i]=!affoutput!
  )

  set affinput=!baseLayersUO[%%i]!
  set affoutput=%ARG_workdir%\layerloop%%iuo.w64
  if not "!affinput!"=="" (
    call :process_affirmations_loop "!affoutput!" "!affinput!" "!affloops!"
    set loopedLayersUO[%%i]=!affoutput!
  )
)

call :process_calculate_autofill_duration_at_normal_speed loopedLayers "" "" "%autofillduration%" autofillduration
for /L %%i in (1,1,%layerCount%) do (
  set layerAutofillDuration=%autofillduration%
  set autofillmethod=%autofill%
  set afftext=!%affArray%[%%i]!
  call :string_ends_with "!afftext!" ".txt" isTXT
  if not "!isTXT!"=="1" call :string_ends_with "!afftext!" ".TXT" isTXT
  if not "!isTXT!"=="1" (
    if "!autofillmethod!"=="silencedetect" set autofillmethod=safe
  )

  set baseLayer=!baseLayers[%%i]!
  set loopedLayer=!loopedLayers[%%i]!
  set affoutput=%ARG_workdir%\layerautofill%%i.w64
  if not "!loopedLayer!"=="" (
    call :process_affirmations_autofill "!affoutput!" "!baseLayer!" "!loopedLayer!" "!layerAutofillDuration!" "%%i" "!autofillmethod!"
    call :maybe_delete_workfile "!baseLayer!"
    call :maybe_delete_workfile "!loopedLayer!"
    set autofilledLayers[%%i]=!affoutput!
  )
  
  set baseLayer=!baseLayersU[%%i]!
  set loopedLayer=!loopedLayersU[%%i]!
  set affoutput=%ARG_workdir%\layerautofill%%iu.w64
  if not "!loopedLayer!"=="" (
    call :process_affirmations_autofill "!affoutput!" "!baseLayer!" "!loopedLayer!" "!layerAutofillDuration!" "%%iu" "!autofillmethod!"
    call :maybe_delete_workfile "!baseLayer!"
    call :maybe_delete_workfile "!loopedLayer!"
    set autofilledLayersU[%%i]=!affoutput!
  )

  set baseLayer=!baseLayersUO[%%i]!
  set loopedLayer=!loopedLayersUO[%%i]!
  set affoutput=%ARG_workdir%\layerautofill%%iuo.w64
  if not "!loopedLayer!"=="" (
    call :process_affirmations_autofill "!affoutput!" "!baseLayer!" "!loopedLayer!" "!layerAutofillDuration!" "%%iuo" "!autofillmethod!"
    call :maybe_delete_workfile "!baseLayer!"
    call :maybe_delete_workfile "!loopedLayer!"
    set autofilledLayersUO[%%i]=!affoutput!
  )
)

for /L %%i in (1,1,%layerCount%) do (
  set affspeed=
  set afftempo=
  set affpitch=
  set affbandpass=
  set affbandreject=
  set affnorm=
  
  set affinput=!autofilledLayers[%%i]!
  set affoutput=%ARG_workdir%\layer%%i.w64
  if not "!affinput!"=="" (
    call :process_affirmations_effects "!affoutput!" "!affinput!" "!affspeed!" "!afftempo!" "!affpitch!" "!affbandpass!" "!affbandreject!" "!affnorm!" "%normwith%" "%bandfilterwidth%" "%paddingStart%" "%paddingEnd%"
    call :maybe_delete_workfile "!affinput!"
    set processedLayers[%%i]=!affoutput!
  )

  set affinput=!autofilledLayersU[%%i]!
  set affoutput=%ARG_workdir%\layer%%iu.w64
  if not "!affinput!"=="" (
    call :process_affirmations_effects "!affoutput!" "!affinput!" "!affspeed!" "!afftempo!" "!affpitch!" "!affbandpass!" "!affbandreject!" "!affnorm!" "%normwith%" "%bandfilterwidth%" "%paddingStart%" "%paddingEnd%"
    call :maybe_delete_workfile "!affinput!"
    set processedLayersU[%%i]=!affoutput!
  )

  set affinput=!autofilledLayersUO[%%i]!
  set affoutput=%ARG_workdir%\layer%%iuo.w64
  if not "!affinput!"=="" (
    call :process_affirmations_effects "!affoutput!" "!affinput!" "!affspeed!" "!afftempo!" "!affpitch!" "!affbandpass!" "!affbandreject!" "!affnorm!" "%normwith%" "%bandfilterwidth%" "%paddingStart%" "%paddingEnd%"
    call :maybe_delete_workfile "!affinput!"
    set processedLayersUO[%%i]=!affoutput!
  )

  if not "!processedLayersU[%%i]!"=="" (
    call :process_display_audio_stats "!processedLayersU[%%i]!" "Layer %%i ^^^(ultrasonic^^^)"
  ) else (
    call :process_display_audio_stats "!processedLayers[%%i]!" "Layer %%i"
  )
)

for /L %%i in (1,1,%layerCount%) do (
  set affinput=!processedLayers[%%i]!
  if not "%testaffsOutput%"=="" (
    call :process_affirmations_test_output "%testaffsOutput%" "!affinput!" "%testaffsNorm%" "%normwith%" "%%i"
  )
)

if not "%outputUltrasonic%"=="" (
  set layerList=
  for /L %%i in (1,1,%layerCount%) do set layerList=!layerList! "!processedLayersUO[%%i]!"
  set affoutput=%ARG_workdir%\layerswithoutultrasonics.w64
  call :generate_panned_audio_file "!affoutput!" "%panmethod%" !layerList!
  for /L %%i in (1,1,%layerCount%) do call :maybe_delete_workfile "!processedLayersUO[%%i]!"
  if "%outputUltrasonicNorm%"=="" (
    call :move_file "!affoutput!" "%outputUltrasonic%"
  ) else (
    call :normalize_audio_file "!affoutput!" "%outputUltrasonic%" "%outputUltrasonicNorm%" "%normwith%"
  )
  call :maybe_delete_workfile "!affoutput!"
)

set groupCount=0
for /L %%i in (1,1,%layerCount%) do (
  set group=!%groupsArray%[%%i]!
  set affinputs=affinputs_!group!
  set affoutputs=affoutputs_!group!
  call :get_array_length !affinputs! countPerGroup
  if "!countPerGroup!"=="0" (
    set /a "groupCount=!groupCount!+1"
    set groups[!groupCount!]=!group!
  )
  set /a "countPerGroup=!countPerGroup!+1"
  set affultrasonic=!%ultrasonicArray%[%%i]!
  if not "!affultrasonic!"=="" (
    set affinput=!processedLayersU[%%i]!
    call :maybe_delete_workfile "!processedLayers[%%i]!"
  ) else (
    set affinput=!processedLayers[%%i]!
    call :maybe_delete_workfile "!processedLayersU[%%i]!"
  )
  set !affinputs![!countPerGroup!]=!affinput!
  if "!countPerGroup!"=="1" set !affoutputs!=%ARG_workdir%\group!groupCount!.w64
)
for /L %%i in (1,1,%groupCount%) do (
  set group=!groups[%%i]!
  set affinputs=affinputs_!group!
  set affoutputs=affoutputs_!group!
  call :get_array_length !affinputs! countPerGroup
  set layerList=
  for /L %%j in (1,1,!countPerGroup!) do (
    call set affinput=%%!affinputs![%%j]%%
    set layerList=!layerList! "!affinput!"
  )
  call set affoutput=%%!affoutputs!%%
  call :generate_panned_audio_file "!affoutput!" "%panmethod%" !layerList!
  if not "!groupCount!"=="1" call :process_display_audio_stats "!affoutput!" "Group %%i"
  for /L %%j in (1,1,!countPerGroup!) do (
    call set affinput=%%!affinputs![%%j]%%
    call :maybe_delete_workfile "!affinput!"
  )
)
set layerList=
for /L %%i in (1,1,%groupCount%) do (
  set group=!groups[%%i]!
  set affoutputs=affoutputs_!group!
  call set affoutput=%%!affoutputs!%%
  set layerList=!layerList! "!affoutput!"
)
call :mix_audio_files "%output%" %layerList%
for /L %%i in (1,1,%groupCount%) do (
  set group=!groups[%%i]!
  set affoutputs=affoutputs_!group!
  call set affoutput=%%!affoutputs!%%
  call :maybe_delete_workfile "!affoutput!"
)

exit /b

:: Generate an audio file from the affirmations contained in a text file
:: process_generate_affirmations "path\output.w64" "path\affs.wav"
:: process_generate_affirmations "path\output.w64" "path\affs.txt" "voicename"
:: process_generate_affirmations "path\output.w64" "path\affs.txt" "balcon:voicename"
:: process_generate_affirmations "path\output.w64" "path\affs.txt" "bal4web:google:en-US:female:voicename"
:process_generate_affirmations
setlocal EnableDelayedExpansion
set output=%~1
set input=%~2
set voiceSettings=%~3

if not "%voiceSettings%"=="" (
  call :string_starts_with "%voiceSettings%" "balcon:" hasPrefix
  if not "!hasPrefix!"=="1" call :string_starts_with "%voiceSettings%" "bal4web:" hasPrefix
  if not "!hasPrefix!"=="1" set voiceSettings=balcon:!voiceSettings!
)

set parse=%voiceSettings%
:prepare_parse_voice_args
set replacedParse=%parse%
if not "%replacedParse%"=="" set replacedParse=%replacedParse:::=:@@@@@:%
if not "%replacedParse%"=="%parse%" (
  set parse=%replacedParse%
  goto prepare_parse_voice_args
)
set parse=%replacedParse%

set voiceArgIndex=0
:parse_voice_args
for /F "tokens=1* delims=:" %%i in ("%parse%") do (
  set /a "voiceArgIndex=!voiceArgIndex!+1"
  if "%%i"=="@@@@@" (
    set voiceArg[!voiceArgIndex!]=
  ) else (
    set voiceArg[!voiceArgIndex!]=%%i
  )
  set parse=%%j
)
if not "%parse%"=="" goto parse_voice_args
set voiceArgCount=%voiceArgIndex%

set voiceArgs=
for /L %%i in (2,1,!voiceArgCount!) do set voiceArgs=!voiceArgs! "!voiceArg[%%i]!"

call :string_ends_with "%input%" ".txt" isTXT
if not "%isTXT%"=="1" call :string_ends_with "%input%" ".TXT" isTXT
if not "%isTXT%"=="1" (
  call :process_generate_sox_affirmations "%output%" "%input%" "%voiceArgs%"
) else (
  if "%voiceArg[1]%"=="bal4web" (
    call :process_generate_bal4web_affirmations "%output%" "%input%" %voiceArgs%
  ) else (
    call :process_generate_balcon_affirmations "%output%" "%input%" %voiceArgs%
  )
)

endlocal
exit /b

:: Generate an audio file from the affirmations contained in an audio file using sox
:: process_generate_sox_affirmations "path\output.w64" "path\affs.wav"
:process_generate_sox_affirmations
setlocal
set output=%~1
set input=%~2

call :process_import_audio_file "%output%" "%input%" 1 1

endlocal
exit /b

:: Generate an audio file from the affirmations contained in a text file using balcon
:: process_generate_balcon_affirmations "path\output.w64" "path\affs.txt" "voicename"
:process_generate_balcon_affirmations
setlocal
set output=%~1
set input=%~2
set voice=%~3

set outputtemp=%ARG_workdir%\balcontemp.wav
set makeWavCommand="%EXE_BALCON%" -f "%input%" -w "%outputtemp%" -enc "utf8" -e 250 -a 250 -fr 48 -bt 16 -ch 1
if not "%voice%"=="" set makeWavCommand=%makeWavCommand% -n "%voice%"
%makeWavCommand%

%EXECUTE_SOX% "%outputtemp%" -r 48000 -b 16 -c 1 "%output%"
call :maybe_delete_workfile "%outputtemp%"

endlocal
exit /b

:: Generate an audio file from the affirmations contained in a text file using bal4web
:: process_generate_bal4web_affirmations "path\output.w64" "path\affs.txt" "google" "en-US" "female" "voicename"
:process_generate_bal4web_affirmations
setlocal
set output=%~1
set input=%~2
set service=%~3
set language=%~4
set gender=%~5
set voice=%~6

set outputtemp=%ARG_workdir%\bal4webtemp.wav
set makeWavCommand="%EXE_BAL4WEB%" -f "%input%" -w "%outputtemp%" -enc "utf8" -e 250
if not "%service%"=="" set makeWavCommand=%makeWavCommand% -s "%service%"
if not "%language%"=="" set makeWavCommand=%makeWavCommand% -l "%language%"
if not "%gender%"=="" set makeWavCommand=%makeWavCommand% -g "%gender%"
if not "%voice%"=="" set makeWavCommand=%makeWavCommand% -n "%voice%"
if not "%ARG_proxyhost%"=="" set makeWavCommand=%makeWavCommand% -host "%ARG_proxyhost%"
if not "%ARG_proxyport%"=="" set makeWavCommand=%makeWavCommand% -port "%ARG_proxyport%"
%makeWavCommand%

%EXECUTE_SOX% "%outputtemp%" -r 48000 -b 16 -c 1 "%output%"
call :maybe_delete_workfile "%outputtemp%"

endlocal
exit /b

:: Calculate the automatic pitch values of the affirmations
:: process_calculate_autopitch layerCount voiceArray pitchArray outPitchArray
:process_calculate_autopitch
setlocal EnableDelayedExpansion
set layerCount=%~1
set voiceArray=%~2
set pitchArray=%~3
set resultArray=%~4

for /L %%i in (1,1,%layerCount%) do (
  set pitch=!%pitchArray%[%%i]!
  call :string_starts_with "!pitch!" "auto" isAuto
  if "!isAuto!"=="1" (
    set voice=!%voiceArray%[%%i]!
    call :get_string_sha1 "!voice!" voice
    set channelArrayVar=channels!voice!
    call :get_array_length "!channelArrayVar!" channelCount
    if !channelCount!==0 (
      call :get_array_length "processableVoiceArray" voiceCount
      set /a "voiceCount=!voiceCount!+1"
      set processableVoiceArray[!voiceCount!]=!voice!
    )
    set /a "channelCount=!channelCount!+1"
    set !channelArrayVar![!channelCount!]=%%i
  )
)
call :get_array_length "processableVoiceArray" voiceCount

for /L %%i in (1,1,%voiceCount%) do (
  set voice=!processableVoiceArray[%%i]!
  set channelArrayVar=channels!voice!
  call set channel=%%!channelArrayVar![1]%%
  call set pitch=%%!pitchArray![!channel!]%%
  if "!pitch!"=="auto" (
    if !channelCount! LSS 4 (
      set pitch=200
    ) else (
      set /a "divider=!channelCount!%%2"
      if !divider!==0 (
        set divider=!channelCount!
      ) else (
        set /a "divider=!channelCount!-1"
      )
      set /a "pitch=1600/!divider!"
      if !channelCount! LSS 50 set /a "pitch=1400/!divider!"
      if !channelCount! LSS 40 set /a "pitch=1200/!divider!"
      if !channelCount! LSS 30 set /a "pitch=1000/!divider!"
      if !channelCount! LSS 20 set /a "pitch=800/!divider!"
      if !channelCount! LSS 10 set /a "pitch=600/!divider!"
      if !channelCount! LSS 5 set pitch=150
      if !pitch! LSS 1 set pitch=1
    )
    set pitch=auto!pitch!
  )
  set factorVar=factor!voice!
  set !factorVar!=!pitch:~4!
)

for /L %%i in (1,1,%voiceCount%) do (
  set voice=!processableVoiceArray[%%i]!
  set channelArrayVar=channels!voice!
  call :get_array_length "!channelArrayVar!" channelCount
  set calculatedArrayVar=calculated!voice!
  set factorVar=factor!voice!
  call set factor=%%!factorVar!%%
  
  set /a "centerChannel=!channelCount!/2+1"
  set minLeftChannel=1
  set /a "maxLeftChannel=!centerChannel!-1"
  set /a "minRightChannel=!centerChannel!+1"
  set maxRightChannel=!channelCount!

  set !calculatedArrayVar![!centerChannel!]=0
  set val=0
  for /L %%j in (!minRightChannel!,1,!maxRightChannel!) do (
    if %%j==!maxRightChannel! (
      set val=!factor!
    ) else (
      if !val!==0 (
        set /a "val=-2*!factor!"
      ) else (
        if !val! LSS 0 (
          set /a "val=!val!*-1+!factor!"
        ) else (
          set /a "val=!val!*-1-!factor!"
        )
      )
    )
    set !calculatedArrayVar![%%j]=!val!
  )
  set val=0
  for /L %%j in (!maxLeftChannel!,-1,!minLeftChannel!) do (
    if %%j==!minLeftChannel! (
      set /a "val=-1*!factor!"
    ) else (
      if !val!==0 (
        set /a "val=2*!factor!"
      ) else (
        if !val! LSS 0 (
          set /a "val=!val!*-1+!factor!"
        ) else (
          set /a "val=!val!*-1-!factor!"
        )
      )
    )
    set !calculatedArrayVar![%%j]=!val!
  )

  if !channelCount! GTR 3 (
    set /a "minLeftChannelPlusOne=!minLeftChannel!+1"
    set /a "maxRightChannelMinusOne=!maxRightChannel!-1"
    call set vl0=%%!calculatedArrayVar![!minLeftChannel!]%%
    call set vl1=%%!calculatedArrayVar![!minLeftChannelPlusOne!]%%
    call set vr0=%%!calculatedArrayVar![!maxRightChannel!]%%
    call set vr1=%%!calculatedArrayVar![!maxRightChannelMinusOne!]%%
    if !vl0! LSS 0 (
      if !vl1! LSS 0 (
        if !vr0! GTR 0 (
          if !vr1! GTR 0 (
            set !calculatedArrayVar![!minLeftChannel!]=!vr0!
            set !calculatedArrayVar![!maxRightChannel!]=!vl0!
          )
        )
      )
    )
    if !vl0! GTR 0 (
      if !vl1! GTR 0 (
        if !vr0! LSS 0 (
          if !vr1! LSS 0 (
            set !calculatedArrayVar![!minLeftChannel!]=!vr0!
            set !calculatedArrayVar![!maxRightChannel!]=!vl0!
          )
        )
      )
    )
  )
)

call :delete_file "%ARG_workdir%\autopitch.txt"
for /L %%i in (1,1,%layerCount%) do (
  call set pitch=%%!pitchArray![%%i]%%
  echo set %resultArray%[%%i]=!pitch!>> "%ARG_workdir%\autopitch.txt"
)
for /L %%i in (1,1,%voiceCount%) do (
  set voice=!processableVoiceArray[%%i]!
  set channelArrayVar=channels!voice!
  call :get_array_length "!channelArrayVar!" channelCount
  set calculatedArrayVar=calculated!voice!
  for /L %%j in (1,1,!channelCount!) do (
    call set channel=%%!channelArrayVar![%%j]%%
    call set pitch=%%!calculatedArrayVar![%%j]%%
    if "!pitch!"=="0" set pitch=
    echo set %resultArray%[!channel!]=!pitch!>> "%ARG_workdir%\autopitch.txt"
  )
)
endlocal
for /F "usebackq tokens=1 delims=" %%i in ("%ARG_workdir%\autopitch.txt") do (
  if not "%%i"=="" %%i
)
call :maybe_delete_workfile "%ARG_workdir%\autopitch.txt"
exit /b

:: Apply the given effects to an affirmation audio file
:: process_affirmations_effects "path\output.w64" "path\input.w64" speedFactor tempoFactor pitchAdded bandpassRange bandrejectRange normDB normWith bandfilterWidth paddingStartSec paddingEndSec
:process_affirmations_effects
setlocal EnableDelayedExpansion
set output=%~1
set input=%~2
set speed=%~3
set tempo=%~4
set pitch=%~5
set bandpassRange=%~6
set bandrejectRange=%~7
set norm=%~8
set normwith=%~9
shift
set bandfilterWidth=%~9
shift
set paddingStart=%~9
shift
set paddingEnd=%~9
set outputtemp=%ARG_workdir%\effectstemp.w64

call :split_string "%bandpassRange%" " " bandpass
set bandpassRange=
if "%bandpass[2]%"=="" set bandpass[1]=
if not "%bandpass[1]%"=="" (
  if !bandpass[1]! GEQ 24000 set bandpass[1]=23999
  if !bandpass[2]! GEQ 24000 set bandpass[2]=23999
  set bandpassRange=!bandpass[1]!-!bandpass[2]!
)

call :split_string "%bandrejectRange%" " " bandreject
set bandrejectRange=
if "%bandreject[2]%"=="" set bandreject[1]=
if not "%bandreject[1]%"=="" (
  if !bandreject[1]! GEQ 24000 set bandreject[1]=23999
  if !bandreject[2]! GEQ 24000 set bandreject[2]=23999
  set bandrejectRange=!bandreject[2]!-!bandreject[1]!
)

if "%paddingStart%"=="" set paddingStart=0
if "%paddingEnd%"=="" set paddingEnd=0
set hasPadding=
if not "%paddingStart%"=="0" set hasPadding=1
if not "%paddingEnd%"=="0" set hasPadding=1

set makeWavCommand=
if not "%tempo%"=="" set makeWavCommand=%makeWavCommand% tempo -s %tempo%
if not "%pitch%"=="" set makeWavCommand=%makeWavCommand% pitch %pitch%
if not "%speed%"=="" set makeWavCommand=%makeWavCommand% speed %speed%
if not "%bandpassRange%"=="" (
  set makeWavCommand=!makeWavCommand! sinc -a 180
  if not "%bandfilterWidth%"=="" set makeWavCommand=!makeWavCommand! -t !bandfilterWidth!
  set makeWavCommand=!makeWavCommand! !bandpassRange!
)
if not "%bandrejectRange%"=="" (
  set makeWavCommand=!makeWavCommand! sinc -a 180
  if not "%bandfilterWidth%"=="" set makeWavCommand=!makeWavCommand! -t !bandfilterWidth!
  set makeWavCommand=!makeWavCommand! !bandrejectRange!
)
if "%makeWavCommand%"=="" (
  call :copy_file "%input%" "%outputtemp%"
) else (
  %EXECUTE_SOX% "%input%" "%outputtemp%" %makeWavCommand%
)

set makeWavCommand=
if not "%norm%"=="" (
  if "%normwith%"=="peak" (
    set makeWavCommand=!makeWavCommand! loudness 0 gain -n %norm%
  ) else (
    if "%normwith%"=="rms" (
      call :get_audio_file_normalization_delta_rms "%outputtemp%" "%norm%" delta
      set makeWavCommand=!makeWavCommand! loudness 0 gain !delta!
    ) else (
      call :get_audio_file_normalization_delta_r128 "%outputtemp%" "%norm%" delta
      set makeWavCommand=!makeWavCommand! gain !delta!
    )
  )
)
if not "%hasPadding%"=="" set makeWavCommand=%makeWavCommand% pad %paddingStart% %paddingEnd%
if "%makeWavCommand%"=="" (
  call :move_file "%outputtemp%" "%output%"
) else (
  %EXECUTE_SOX% "%outputtemp%" "%output%" %makeWavCommand%
)

call :maybe_delete_workfile "%outputtemp%"
endlocal
exit /b

:: Repeat an affirmation audio file the given times
:: process_affirmations_loop "path\output.w64" "path\input.w64" loops
:process_affirmations_loop
setlocal
set output=%~1
set input=%~2
set loops=%~3

if "%loops%"=="" (
  call :copy_file "%input%" "%output%"
) else (
  call :repeat_audio_file "%input%" "%output%" "%loops%"
)

endlocal
exit /b

:: Calculate the autofill duration to be used in the process
:: process_calculate_autofill_duration_at_normal_speed inputArray speedArrayOrEmpty tempoArrayOrEmpty autofillDurationOrEmpty outResult
:process_calculate_autofill_duration_at_normal_speed
setlocal EnableDelayedExpansion
set inputArray=%~1
set speedArray=%~2
set tempoArray=%~3
set autofillduration=%~4
set resultVar=%~5

call :get_array_length %inputArray% fileCount
set longestIndex=
set longestDuration=
for /L %%i in (1,1,%fileCount%) do (
  set input=!%inputArray%[%%i]!
  call :get_audio_file_duration "!input!" duration
  set speed=!%speedArray%[%%i]!
  if "!speed!"=="" set speed=1
  set tempo=!%tempoArray%[%%i]!
  if "!tempo!"=="" set tempo=1
  call :calculate "!duration!/!speed!/!tempo!" duration
  if not "!duration!"=="" (
    if "!longestIndex!"=="" (
      set longestIndex=%%i
      set longestDuration=!duration!
    ) else (
      call :compare_decimal "!longestDuration!" "!duration!"
      if !errorlevel! LSS 0 (
        set longestIndex=%%i
        set longestDuration=!duration!
      )
    )
  )
)

if "%autofillduration%"=="" set autofillduration=%longestDuration%
call :compare_decimal "%autofillduration%" "%longestDuration%"
if %errorlevel% LSS 0 set autofillduration=%longestDuration%

endlocal & set "%resultVar%=%autofillduration%"
exit /b

:: Repeat an affirmation audio file until it reaches the given duration
:: process_affirmations_autofill "path\output.w64" "path\baseLayer.w64" "path\loopedLayer.w64" durationSec layerIndex "simple|safe|silencedetect"
:process_affirmations_autofill
setlocal EnableDelayedExpansion
set output=%~1
set baseLayer=%~2
set loopedLayer=%~3
set duration=%~4
set layerIndex=%~5
set method=%~6

call :get_audio_file_duration "%loopedLayer%" inputDuration
call :compare_decimal "%duration%" "%inputDuration%"
if %errorlevel% LEQ 0 (
  call :copy_file "%loopedLayer%" "%output%"
  goto process_affirmations_autofill_done
)

if "%method%"=="silencedetect" (
  call :process_affirmations_autofill_silencedetect "%output%" "%baseLayer%" "%loopedLayer%" %duration% %layerIndex%
) else (
  if "%method%"=="simple" (
    call :process_affirmations_autofill_simple "%output%" "%loopedLayer%" %duration%
  ) else (
    call :process_affirmations_autofill_safe "%output%" "%baseLayer%" "%loopedLayer%" %duration%
  )
)

:process_affirmations_autofill_done
endlocal
exit /b

:: Repeat an affirmation audio file until it reaches the given duration
:: It is a simple and fast implementation which repeats and trim the file until has the right duration, and it may cut in the middle of an affirmation
:: process_affirmations_autofill_simple "path\output.w64" "path\input.w64" durationSec
:process_affirmations_autofill_simple
setlocal
set output=%~1
set input=%~2
set duration=%~3

call :repeat_and_trim_audio_file "%input%" "%output%" %duration%

endlocal
exit /b

:: Repeat an affirmation audio file until it reaches the given duration
:: This implementation repeats the file without trimming it so it has at most the right duration
:: There will be at most 1 repeat lost
:: process_affirmations_autofill_safe "path\output.w64" "path\baseLayer.w64" "path\loopedLayer.w64" durationSec
:process_affirmations_autofill_safe
setlocal
set output=%~1
set baseLayer=%~2
set loopedLayer=%~3
set duration=%~4
set WAV_AUTOFILL_TEMP=%ARG_workdir%\autofilltemp.w64
set WAV_AUTOFILL_REPEAT=%ARG_workdir%\autofillrepeat.w64
set WAV_AUTOFILL_REPEAT2=%ARG_workdir%\autofillrepeat2.w64

call :repeat_audio_file_at_most "%loopedLayer%" "%WAV_AUTOFILL_REPEAT%" "%duration%"

call :get_audio_file_duration "%WAV_AUTOFILL_REPEAT%" repeatDuration
call :calculate "%duration%-%repeatDuration%" repeatDuration
call :floor_decimal "%repeatDuration%" repeatDuration
call :get_audio_file_duration "%baseLayer%" baseDuration
call :compare_decimal "%baseDuration%" "%repeatDuration%"
if %errorlevel% LEQ 0 (
  call :repeat_audio_file_at_most "%baseLayer%" "%WAV_AUTOFILL_REPEAT2%" "%repeatDuration%"
  call :concat_audio_files "%WAV_AUTOFILL_TEMP%" "%WAV_AUTOFILL_REPEAT%" "%WAV_AUTOFILL_REPEAT2%"
  call :move_file "%WAV_AUTOFILL_TEMP%" "%WAV_AUTOFILL_REPEAT%"
)

call :move_file "%WAV_AUTOFILL_REPEAT%" "%output%"

call :maybe_delete_workfile "%WAV_AUTOFILL_REPEAT%"
call :maybe_delete_workfile "%WAV_AUTOFILL_REPEAT2%"
call :maybe_delete_workfile "%WAV_AUTOFILL_TEMP%"
endlocal
exit /b

:: Repeat an affirmation audio file until it reaches the given duration
:: This implementation uses experimental silence detection in an attempt to never cut in the middle of an affirmation
:: process_affirmations_autofill_silencedetect "path\output.w64" "path\baseLayer.w64" "path\loopedLayer.w64" durationSec layerIndex
:process_affirmations_autofill_silencedetect
setlocal EnableDelayedExpansion
set output=%~1
set baseLayer=%~2
set loopedLayer=%~3
set duration=%~4
set layerIndex=%~5
set WAV_AUTOFILL_TEMP=%ARG_workdir%\autofilltemp.w64

:: We start from the result of the safe autofill method
set WAV_AUTOFILL_SAFE=%ARG_workdir%\autofillsafe.w64
call :process_affirmations_autofill_safe "%WAV_AUTOFILL_SAFE%" "%baseLayer%" "%loopedLayer%" "%duration%"

:: Create the split workfolder
set FOLDER_SPLIT=%ARG_workdir%\autofill%layerIndex%
mkdir "%FOLDER_SPLIT%" 2> NUL

:: Split the base layer affirmations
call :pad_audio_file "%baseLayer%" "%WAV_AUTOFILL_TEMP%" 1 1
%EXECUTE_SOX% "%WAV_AUTOFILL_TEMP%" "%FOLDER_SPLIT%\aff.w64" silence 1 0.001 0%% 1 0.001 0%% : newfile : restart

:: Add some padding to the splitted affirmations
for /f "usebackq tokens=*" %%i IN (`dir "%FOLDER_SPLIT%" /b /s`) do (
  call :pad_audio_file "%%i" "%WAV_AUTOFILL_TEMP%" 0.5
  call :move_file "%WAV_AUTOFILL_TEMP%" "%%i"
)

:: Build arrays from the splitted affirmations
set fileCount=0
for /f "usebackq tokens=*" %%i IN (`dir "%FOLDER_SPLIT%" /b /s`) do (
  call :get_audio_file_duration "%%i" fileDuration
  if "!fileDuration!"=="" set fileDuration=0
  if not "!fileDuration!"=="0" (
    set /a "fileCount=!fileCount!+1"
    set fileDurations[!fileCount!]=!fileDuration!
    set filePaths[!fileCount!]=%%i
  )
)

:: Batch the splitted affirmations to make processing faster
set batchSize=30
set batchCount=1
set batchIndex=1
set fileIndex=1
call :get_audio_file_duration "%WAV_AUTOFILL_SAFE%" resultDuration
:process_affirmations_autofill_silencedetect_batch
set filePath=!filePaths[%fileIndex%]!
set fileDuration=!fileDurations[%fileIndex%]!
call :calculate "%resultDuration%+%fileDuration%" newDuration
call :compare_decimal "%duration%" "%newDuration%"
if %errorlevel% LSS 0 goto process_affirmations_autofill_silencedetect_batch_done
set resultDuration=%newDuration%
set batchedFilePaths[%batchCount%][%batchIndex%]=%filePath%
set /a "fileIndex=%fileIndex%+1"
if %fileIndex% GTR %fileCount% set fileIndex=1
set /a "batchIndex=%batchIndex%+1"
if %batchIndex% GEQ %batchSize% (
  set /a "batchCount=!batchCount!+1"
  set batchIndex=1
)
goto process_affirmations_autofill_silencedetect_batch
:process_affirmations_autofill_silencedetect_batch_done

:: Concatenate the batched affirmations until we reach the duration
set batchIndex=1
call :move_file "%WAV_AUTOFILL_SAFE%" "%output%"
:process_affirmations_autofill_silencedetect_concat
call :get_array_length batchedFilePaths[%batchIndex%] batchLength
if %batchLength% LSS 1 goto process_affirmations_autofill_silencedetect_concat_done
set batchedFiles=
for /L %%i in (1,1,%batchLength%) do (
  set filePath=!batchedFilePaths[%batchIndex%][%%i]!
  set batchedFiles=!batchedFiles! "!filePath!"
)
%EXECUTE_SOX% "%output%" %batchedFiles% "%WAV_AUTOFILL_TEMP%"
call :move_file "%WAV_AUTOFILL_TEMP%" %output%
set /a "batchIndex=%batchIndex%+1"
goto process_affirmations_autofill_silencedetect_concat
:process_affirmations_autofill_silencedetect_concat_done

:: Delete the work files
call :maybe_delete_workfile "%WAV_AUTOFILL_SAFE%"
call :maybe_delete_workfile "%WAV_AUTOFILL_TEMP%"
call :maybe_delete_workfolder "%FOLDER_SPLIT%"

endlocal
exit /b

:: Output a test audio file for the given affirmations layer
:: process_affirmations_test_output "path\output.wav" "path\input.w64" normDB normWith layerIndex
:process_affirmations_test_output
setlocal EnableDelayedExpansion
set output=%~1
set input=%~2
set norm=%~3
set normwith=%~4
set layerIndex=%~5

call :get_file_path "%output%" outputpath
call :get_file_name "%output%" outputname
call :get_file_ext "%output%" outputext
set output=%outputpath%\%outputname%%layerIndex%.%outputext%


if "%norm%"=="" (
  call :process_export_audio_file "%output%" "%input%"
) else (
  set outputtemp=%ARG_workdir%\layertestpreconvert.w64
  call :normalize_audio_file "%input%" "!outputtemp!" "%norm%" "%normwith%"
  call :process_export_audio_file "%output%" "!outputtemp!"
  call :maybe_delete_workfile "!outputtemp!"
)

endlocal
exit /b

:: Turn the given affirmations into ultrasonic
:: process_affirmations_ultrasonic "path\output.w64" "path\input.w64" carrier normDB normWith
:process_affirmations_ultrasonic
setlocal
set output=%~1
set input=%~2
set carrier=%~3
set norm=%~4
set normwith=%~5

if "%norm%"=="" call :get_audio_file_normalization "%input%" "%normwith%" norm

call :generate_ultrasonic_audio_file "%output%" "%input%" "%carrier%" "%norm%" "%normwith%"

endlocal
exit /b

:: Process the ultrasonic output
:: process_ultrasonic_output "path\output.w64" "path\layers.w64" carrierFreq normDB normWith
:process_ultrasonic_output
setlocal
set output=%~1
set input=%~2
set carrier=%~3
set norm=%~4
set normwith=%~5

if "%norm%"=="" call :get_audio_file_normalization "%input%" "%normwith%" norm

call :generate_ultrasonic_audio_file "%output%" "%input%" "%carrier%" "%norm%" "%normwith%"

endlocal
exit /b

:: Process left and right frequencies
:: process_frequencies "path\output.w64" durationSec leftFreqHz rightFreqHz freqNormDB normWith freqPulseSec freqGapSec freqFadeSec "normal|slow|fast"
:process_frequencies
setlocal
set output=%~1
set duration=%~2
set leftFreq=%~3
set rightFreq=%~4
set freqNorm=%~5
set normwith=%~6
set freqPulse=%~7
set freqGap=%~8
set freqFade=%~9
shift
set freqRotation=%~9

set hasFreqs=0
if not "%leftFreq%"=="" set hasFreqs=1
if not "%rightFreq%"=="" set hasFreqs=1
if "%hasFreqs%"=="0" (
  call :generate_silent_audio_file "%output%" 1 2
) else (
  call :generate_frequency_audio_file "%output%" "%leftFreq%" "%rightFreq%" "%duration%" "%freqNorm%" "%normwith%" "%freqPulse%" "%freqGap%" "%freqFade%" "%freqRotation%"
)

endlocal
exit /b

:: Process the noise generation
:: process_noise "path\output.w64" noiseType durationSec noiseNormDB normWith noiseFadeSec
:process_noise
setlocal
set output=%~1
set noise=%~2
set duration=%~3
set noiseNorm=%~4
set normwith=%~5
set noiseFade=%~6

set hasNoise=0
if not "%noise%"=="" set hasNoise=1
if "%hasNoise%"=="0" (
  call :generate_silent_audio_file "%output%" 1 2
) else (
  call :generate_noise_audio_file "%output%" "%noise%" "%duration%" "%noiseNorm%" "%normwith%" "%noiseFade%" 2
)

endlocal
exit /b

:: Process the background
:: process_background "path\output.w64" "path\input.wav" durationSec normDB normWith fadeDurationSec fadeOnLoop "normal|slow|fast"
:process_background
setlocal
set output=%~1
set input=%~2
set duration=%~3
set norm=%~4
set normwith=%~5
set fadeDuration=%~6
set fadeOnLoop=%~7
set backgroundRotation=%~8
if "%backgroundRotation%"=="normal" (
  set backgroundRotation=%ROTATION_NORMAL%
) else (
  if "%backgroundRotation%"=="slow" (
    set backgroundRotation=%ROTATION_SLOW%
  ) else (
    if "%backgroundRotation%"=="fast" (
      set backgroundRotation=%ROTATION_FAST%
    ) else (
      set backgroundRotation=
    )
  )
)
set prevFile=
set curFile=

call :get_audio_file_channel_count "%input%" channelCount

:: Convert the background music to 48khz 16bit stereo w64
set curFile=%ARG_workdir%\bgstereo.w64
if "%input%"=="" (
  call :generate_silent_audio_file "%curFile%" 1 2
) else (
  call :process_import_audio_file "%curFile%" "%input%" 2 1
)
set prevFile=%curFile%

:: Normalize the background
set curFile=%ARG_workdir%\bgnorm.w64
if not "%input%"=="" (
  if not "%norm%"=="" (
    call :normalize_audio_file "%prevFile%" "%curFile%" "%norm%" "%normwith%"
    call :maybe_delete_workfile "%prevFile%"
    set prevFile=%curFile%
  )
)

:: Add a fade effect to the background (/fade with /fadeloop only)
set curFile=%ARG_workdir%\bgfade.w64
if not "%input%"=="" (
  if not "%fadeDuration%"=="" (
    if not "%fadeOnLoop%"=="" (
      call :fade_audio_file "%prevFile%" "%curFile%" "%fadeDuration%" "%fadeDuration%"
      call :maybe_delete_workfile "%prevFile%"
      set prevFile=%curFile%
    )
  )
)

:: Repeat and trim the background so its duration is the same as the affirmations
set curFile=%ARG_workdir%\bgrepeated.w64
if not "%input%"=="" (
  call :repeat_and_trim_audio_file "%prevFile%" "%curFile%" %duration%
  call :maybe_delete_workfile "%prevFile%"
  set prevFile=%curFile%
)

:: Add a fade effect to the background
:: /fade with /fadeloop will add the effect to the end only (since we already added it to the beginning previously)
:: /fade without /fadeloop will add the effect to the beginning and the end
set curFile=%ARG_workdir%\bgfade2.w64
if not "%input%"=="" (
  if not "%fadeDuration%"=="" (
    if not "%fadeOnLoop%"=="" (
      call :fade_audio_file "%prevFile%" "%curFile%" 0 "%fadeDuration%"
    ) else (
      call :fade_audio_file "%prevFile%" "%curFile%" "%fadeDuration%" "%fadeDuration%"
    )
    call :maybe_delete_workfile "%prevFile%"
    set prevFile=%curFile%
  )
)

:: Add a rotation effect to the background
if "%input%"=="" (
  call :copy_file "%prevFile%" "%output%"
) else (
  if "%backgroundRotation%"=="" (
    call :copy_file "%prevFile%" "%output%"
  ) else (
    if %channelCount% LSS 2 (
      call :generate_rotating_audio_file "%output%" "%prevFile%" "%backgroundRotation%" "-90"
    ) else (
      call :generate_rotating_audio_file "%output%" "%prevFile%" "%backgroundRotation%" "-180" "0"
    )
  )
)
call :maybe_delete_workfile "%prevFile%"

endlocal
exit /b

:: Import an internal processing file from an external file
:: It handles format conversion if necessary
:: process_import_audio_file "path\output.w64" "path\input.wav" channelCount enableRenorm
:process_import_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set input=%~2
set channelCount=%~3
set enableRenorm=%~4
if "%enableRenorm%"=="0" set enableRenorm=
set soxInput=

:: Get the parts of the input file complete path
call :get_file_ext "%input%" inputext

:: Pre-convert MP3 files to WAV using Lame
:: This is because SoX would add silence at the beginning and the end of the audio if we used it for the conversion
:: Note that if this step fails for some reason, SoX will be used instead
set isMP3=
if "%inputext%"=="mp3" set isMP3=1
if "%inputext%"=="MP3" set isMP3=1
if "%isMP3%"=="1" (
  set soxInput=%ARG_workdir%\lametemp.wav
  call :delete_file "!soxInput!"
  "%EXE_LAME%" --silent --decode "%input%" "!soxInput!"
  if not exist "!soxInput!" set soxInput=
)

:: Compute the source normalization if necessary
set norm=
if not "%enableRenorm%"=="" (
  if "%soxInput%"=="" (
    call :get_audio_file_normalization_peak "%input%" norm
  ) else (
    call :get_audio_file_normalization_peak "%soxInput%" norm
  )
)

:: Convert to the internal format using SoX
set makeWavCommand=%EXECUTE_SOX%
if "%soxInput%"=="" (
  set makeWavCommand=%makeWavCommand% "%input%"
) else (
  set makeWavCommand=%makeWavCommand% "%soxInput%"
)
set makeWavCommand=%makeWavCommand% -b 16 "%output%" rate 48000 
if not "%channelCount%"=="" set makeWavCommand=%makeWavCommand% channels %channelCount%
if not "%norm%"=="" set makeWavCommand=%makeWavCommand% gain -n %norm%
%makeWavCommand%
call :maybe_delete_workfile "%soxInput%"

endlocal
exit /b

:: Export a file from an internal processing file
:: It handles format conversion if necessary
:: process_export_audio_file "path\output.wav" "path\input.w64"
:process_export_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set input=%~2

:: Get the parts of the output file complete path
call :get_file_path "%output%" outputpath
call :get_file_name "%output%" outputname
call :get_file_ext "%output%" outputext

:: Use FLAC instead of WAV if the output is too large for a WAV file
:: Maximum size of a WAV file is 4.3gib, we'll use 12 hours as a reference duration for a mono file
set isWAV=
if "%outputext%"=="wav" set isWAV=1
if "%outputext%"=="WAV" set isWAV=1
if "%isWAV%"=="1" (
  call :get_audio_file_channel_count "%input%" channelCount
  call :get_audio_file_duration "%input%" duration
  set /a "durationAllowed=3600*12/!channelCount!"
  call :compare_decimal "!duration!" "!durationAllowed!"
  if !errorlevel! GTR 0 (
    set outputext=flac
    set output=%outputpath%\%outputname%.!outputext!
    set isFallbackFLAC=1
  )
)

:: Handle FLAC output
set isFLAC=
if "%outputext%"=="flac" set isFLAC=1
if "%outputext%"=="FLAC" set isFLAC=1
if "%isFLAC%"=="1" (
  if "%isFallbackFLAC%"=="1" (
    %EXECUTE_SOX% "%input%" -t flac -C 0 "%output%"
  ) else (
    %EXECUTE_SOX% "%input%" -t flac -C 4 "%output%"
  )
  goto process_export_audio_file_done
)

:: Handle MP3 output
set isMP3=
if "%outputext%"=="mp3" set isMP3=1
if "%outputext%"=="MP3" set isMP3=1
if "%isMP3%"=="1" (
  "%EXE_LAME%" --silent -b 320 -q 0 "%input%" "%output%"
  goto process_export_audio_file_done
)

:: Handle non-specific output by automatic conversion, including WAV and W64
%EXECUTE_SOX% "%input%" "%output%"

:process_export_audio_file_done
endlocal
exit /b

:: Process the given video output
:: process_video_output "path\output.mp4" "path\audio.w64" "path\image.jpg" auto|image|240p|360p|480p|720p|1080p|1440p|2160p auto|contain|cover|stretch aac|alac|flac delaySec
:: process_video_output "path\output.mp4" "path\audio.w64" "path\to\folder" auto|image|240p|360p|480p|720p|1080p|1440p|2160p auto|contain|cover|stretch aac|alac|flac delaySec
:process_video_output
setlocal
set output=%~1
set input=%~2
set image=%~3
set videosize=%~4
set videosizemode=%~5
set audioencoding=%~6
set delay=%~7

if "%delay%"=="" set delay=5

call :get_string_sha1 "%image%%videosize%%videosizemode%%delay%" slideshowID
set slideshow=%ARG_workdir%\slideshow-%slideshowID%.mp4
if not exist "%slideshow%" call :generate_video_slideshow "%slideshow%" "%image%" "%videosize%" "%videosizemode%" "%delay%"

call :get_audio_file_duration "%input%" duration

set audioParams=
if "%audioencoding%"=="alac" (
  set audioParams=-c:a alac
) else (
  if "%audioencoding%"=="flac" (
    set audioParams=-c:a flac -compression_level 4
  ) else (
    set audioParams=-c:a aac -b:a 320k
  )
)

call :get_video_file_framerate "%slideshow%" framerate

"%EXE_FFMPEG%" -v error -nostdin -y -stream_loop -1 -r %framerate% -i "%slideshow%" -i "%input%" -c:v libx264 -preset medium -tune stillimage -crf 18 %audioParams% -t %duration% -r %framerate% -movflags +faststart "%output%"

endlocal
exit /b

:: Display statistics for the given audio file
:: process_display_audio_stats "path\file.wav" "friendly name"
:process_display_audio_stats
setlocal EnableDelayedExpansion
set input=%~1
set name=%~2

if not "%ARG_showstats%"=="" (
  call :get_audio_file_duration "%input%" duration
  call :get_audio_file_normalization_peak "%input%" peak
  call :get_audio_file_normalization_rms "%input%" rms
  call :get_audio_file_normalization_r128 "%input%" r128
  echo [stats] %name% : duration=!duration! peak=!peak!dBFS rms=!rms!dBFS r128=!r128!LUFS
  echo.
)

endlocal
exit /b

:: Display statistics for the given video file
:: process_display_video_stats "path\file.mp4" "friendly name"
:process_display_video_stats
setlocal EnableDelayedExpansion
set input=%~1
set name=%~2

if not "%ARG_showstats%"=="" (
  call :get_video_file_dimensions "%input%" dimensions
  call :get_video_file_framerate "%input%" framerate
  call :get_video_file_duration "%input%" duration
  call :get_video_file_normalization_peak "%input%" peak
  call :get_video_file_normalization_rms "%input%" rms
  call :get_video_file_normalization_r128 "%input%" r128
  echo [stats] %name% : duration=!duration! peak=!peak!dBFS rms=!rms!dBFS r128=!r128!LUFS dimensions=!dimensions! fps=!framerate!
  echo.
)

endlocal
exit /b

:::::::::::::::
:: Functions ::
:::::::::::::::

:: Parse command line parameters :
:: - script.bat /name value to global variables ARG_name=value
:: - script.bat value to global variables PARAM_number=value
:: parse_command_line_parameters %*
:parse_command_line_parameters
set PARAM_0=0
set parameterName=
set parameterValue=
:parse_command_line_parameters_parse
if [%1]==[] goto parse_command_line_parameters_done
set parameter=%~1
set /a PARAM_0=%PARAM_0%+1
set PARAM_%PARAM_0%=%parameter%
if "%parameter%"=="" goto parse_command_line_parameters_parse_value
if "%parameter%"=="/" goto parse_command_line_parameters_parse_value
if "%parameter:~0,1%"=="/" goto parse_command_line_parameters_parse_name
goto parse_command_line_parameters_parse_value
:parse_command_line_parameters_parse_name
set parameterName=%parameter:~1%
set parameterValue=
set ARG_%parameterName%=1
shift
goto parse_command_line_parameters_parse
:parse_command_line_parameters_parse_value
if defined parameterName (
  if not defined parameterValue (
    set parameterValue=%parameter%
  ) else (
    set parameterValue=%parameterValue% %parameter%
  )
)
if defined parameterName (
  set ARG_%parameterName%=%parameterValue%
)
shift
goto parse_command_line_parameters_parse
:parse_command_line_parameters_done
set parameter=
set parameterName=
set parameterValue=
exit /b

:: Get the length of an array[]
:: get_array_length variableWithoutBrackets outResult
:get_array_length
setlocal
set array=%~1
set resultVar=%~2
set length=0
set index=1
:get_array_length_do
if not defined %array%[%index%] goto get_array_length_done
set length=%index%
set /a "index=%index%+1"
goto get_array_length_do
:get_array_length_done
endlocal & set "%resultVar%=%length%"
exit /b

:: Get the length of a string
:: get_string_length "string" outResult
:: Modified from https://stackoverflow.com/a/5841587
:get_string_length
setlocal EnableDelayedExpansion
set^ expression=%~1
set resultVar=%~2
set length=0
if defined expression (
  set length=1
  for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
    if "!expression:~%%P,1!" NEQ "" ( 
      set /a "length+=%%P"
      set "expression=!expression:~%%P!"
    )
  )
)
endlocal & set "%resultVar%=%length%"
exit /b

:: Extract part of a string
:: substr outResult "string to extract" startIndex [length] outResult
:substr
setlocal
set haystack=%~1
set startIndex=%~2
set length=%~3
set resultVar=%~4
if "%resultVar%"=="" (
  set length=
  set resultVar=%~3
)
set result=
if "%length%"=="" (
  call set result=%%haystack:~%startIndex%%%
) else (
  call set result=%%haystack:~%startIndex%,%length%%%
)
endlocal & set "%resultVar%=%result%"
exit /b

:: Test if a string starts with another string
:: string_starts_with "haystack" "needle" outResult
:string_starts_with
setlocal
set haystack=%~1
set needle=%~2
set resultVar=%~3
call :get_string_length "%needle%" needleLength
call :substr "%haystack%" 0 "%needleLength%" substring
set result=0
if "%substring%"=="%needle%" set result=1
endlocal & set "%resultVar%=%result%"
exit /b

:: Test if a string ends with another string
:: string_ends_with "haystack" "needle" outResult
:string_ends_with
setlocal
set haystack=%~1
set needle=%~2
set resultVar=%~3
call :get_string_length "%needle%" needleLength
call :substr "%haystack%" "-%needleLength%" substring
set result=0
if "%substring%"=="%needle%" set result=1
endlocal & set "%resultVar%=%result%"
exit /b

:: Split a string into an array
:: split_string "haystack" "needle" outResult
:split_string
for /F "usebackq tokens=1 delims=" %%i in (`cscript "%JS_SPLIT%" //e:jscript //nologo "%~1" "%~2" "%~3"`) do (
  if not "%%i"=="" %%i
)
exit /b

:: Calculate the SHA1 hash of a string
:: get_string_sha1 "source" outResult
:get_string_sha1
setlocal
set input=%~1
set resultVar=%~2
set outputtemp=%ARG_workdir%\stringsha1.txt
echo|set /P ="%input%" > "%outputtemp%"
call :get_file_sha1 "%outputtemp%" result
call :maybe_delete_workfile "%outputtemp%"
endlocal & set "%resultVar%=%result%"
exit /b

:: Clean a decimal number so it can be processed
:: clean_decimal number outResult
:clean_decimal
setlocal EnableDelayedExpansion
set n=%~1
set resultVar=%~2
if "%n%"=="" (
  set result=
  goto clean_decimal_done
)

:clean_decimal_remove_spaces
set replaced=%n%
if not "%replaced%"=="" set replaced=%replaced: =%
if not "%replaced%"=="%n%" (
  set n=%replaced%
  goto clean_decimal_remove_spaces
)
:clean_decimal_remove_commas
set replaced=%n%
if not "%replaced%"=="" set replaced=%replaced:,=%
if not "%replaced%"=="%n%" (
  set n=%replaced%
  goto clean_decimal_remove_commas
)
:clean_decimal_lowercase_exp
set replaced=%n%
if not "%replaced%"=="" set replaced=%replaced:E=e%
if not "%replaced%"=="%n%" (
  set n=%replaced%
  goto clean_decimal_lowercase_exp
)

set sign=
if "%n:~0,1%"=="-" (
  set sign=-
  set n=%n:~1%
) else (
  if "%n:~0,1%"=="+" set n=%n:~1%
)

set exp=
if "%n:e=%"=="%n%" goto clean_decimal_get_exp_done
set pos=0
:clean_decimal_get_exp
call set d=%%n:~%pos%,1%%
if not "%d%"=="e" (
  if not "%d%"=="" (
    set /a "pos=!pos!+1"
    goto clean_decimal_get_exp
  )
)
if not "%d%"=="e" goto clean_decimal_get_exp_done
if "%pos%"=="0" (
  set n=1%n%
  set pos=1
)
set /a "posPlusOne=!pos!+1"
call set exp=%%n:~%posPlusOne%%%
call :clean_decimal "%exp%" exp
call set n=%%n:~0,%pos%%%
:clean_decimal_get_exp_done

if "%n:~0,1%"=="." set n=0%n%
set int=
set dec=
for /f "tokens=1,2 delims=." %%a in ("%n%.0") do set int=%%a& set dec=%%b
if "%int%"=="" set int=0
if "%dec%"=="" set dec=0

if "%exp%"=="" goto clean_decimal_apply_exp_done
if "%exp%"=="0" goto clean_decimal_apply_exp_done
set signexp=
if "%exp:~0,1%"=="-" (
  set signexp=-
  set exp=%exp:~1%
)
for /L %%i in (1,1,%exp%) do (
  if "%signexp%"=="-" (
    set d=!int:~-1!
    if "!d!"=="" set d=0
    set int=!int:~0,-1!
    set dec=!d!!dec!
  ) else (
    set d=!dec:~0,1!
    if "!d!"=="" set d=0
    set int=!int!!d!
    set dec=!dec:~1!
  )
  if "!int!"=="" set int=0
  if "!dec!"=="" set dec=0
)
:clean_decimal_apply_exp_done

:clean_decimal_remove_leading_zeros
set d=
if not "%int%"=="" set d=%int:~0,1%
if "%d%"=="0" (
  set int=%int:~1%
  goto clean_decimal_remove_leading_zeros
)
:clean_decimal_remove_trailing_zeros
set d=
if not "%dec%"=="" set d=%dec:~-1%
if "%d%"=="0" (
  set dec=%dec:~0,-1%
  goto clean_decimal_remove_trailing_zeros
)

if "%int%"=="" set int=0
if "%dec%"=="0" set dec=
set result=%sign%%int%
if not "%dec%"=="" set result=%result%.%dec%
if "%result%"=="-0" set result=0

:clean_decimal_done
endlocal & set "%resultVar%=%result%"
exit /b

:: Truncate a decimal number either to an integer or to the given number of decimals
:: truncate_decimal number outResult
:: truncate_decimal number maxDecimals outResult
:truncate_decimal
setlocal EnableDelayedExpansion
set n=%~1
set maxDecimals=%~2
set resultVar=%~3
if "%resultVar%"=="" (
  set resultVar=%maxDecimals%
  set maxDecimals=0
)
if "%n%"=="" (
  set result=
  goto truncate_decimal_done
)

set int=
set dec=
for /f "tokens=1,2 delims=." %%a in ("%n%.0") do set int=%%a& set dec=%%b
if "%int%"=="" set int=0
if "%dec%"=="" set dec=0

if "%maxDecimals%"=="0" (
  call set dec=0
) else (
  call set dec=%%dec:~0,%maxDecimals%%%
)

if "%int%"=="" set int=0
if "%dec%"=="0" set dec=
set result=%int%
if not "%dec%"=="" set result=%result%.%dec%
call :clean_decimal "%result%" result

:truncate_decimal_done
endlocal & set "%resultVar%=%result%"
exit /b

:: Floor a decimal number to the lower closest integer
:: floor_decimal number outResult
:floor_decimal
setlocal EnableDelayedExpansion
set n=%~1
set resultVar=%~2
if "%n%"=="" (
  set result=
  goto floor_decimal_done
)

call :truncate_decimal "%n%" 0 trunc
if not "%n:~0,1%"=="-" (
  set result=%trunc%
  goto floor_decimal_done
)
if "%trunc%"=="%n%" (
  set result=%trunc%
  goto floor_decimal_done
)
set /a "result=%trunc%-1"

:floor_decimal_done
endlocal & set "%resultVar%=%result%"
exit /b

:: Ceil a decimal number to the higher closest integer
:: ceil_decimal number outResult
:ceil_decimal
setlocal EnableDelayedExpansion
set n=%~1
set resultVar=%~2
if "%n%"=="" (
  set result=
  goto ceil_decimal_done
)

call :truncate_decimal "%n%" 0 trunc
if "%n:~0,1%"=="-" (
  set result=%trunc%
  goto ceil_decimal_done
)
if "%trunc%"=="%n%" (
  set result=%trunc%
  goto ceil_decimal_done
)
set /a "result=%trunc%+1"

:ceil_decimal_done
endlocal & set "%resultVar%=%result%"
exit /b

:: Compare two decimal numbers
:: compare_decimal number number
:: Modified from https://gist.github.com/ZaifSenpai/518d30f5a815c21f38e0844af4736107
:compare_decimal
setlocal EnableDelayedExpansion
set n1=%~1
set n2=%~2
if "%n1:~0,1%"=="." set n1=0%n1%
if "%n2:~0,1%"=="." set n2=0%n2%
for /f "tokens=1,2 delims=." %%a in ("%n1%.0") do (
 set int1=%%a
 set dec1=%%b
)
for /f "tokens=1,2 delims=." %%a in ("%n2%.0") do (
 set int2=%%a
 set dec2=%%b
)
if "%int1%"=="" set int1=0
if "%int2%"=="" set int2=0
if "%dec1%"=="" set dec1=0
if "%dec2%"=="" set dec2=0
if %int1% EQU %int2% (
  set isNegative=0
  if "%int1:~0,1%"=="-" set isNegative=1
  call :get_string_length "!dec1!" declen1
  call :get_string_length "!dec2!" declen2
  if !declen1! LSS !declen2! (
    set /a "declendiff=!declen2!-!declen1!"
    for /L %%i in (1,1,!declendiff!) do set dec1=!dec1!0
  )
  if !declen2! LSS !declen1! (
    set /a "declendiff=!declen1!-!declen2!"
    for /L %%i in (1,1,!declendiff!) do set dec2=!dec2!0
  )
  if !dec1! EQU !dec2! (
    set result=0
  ) else (
    if !dec1! LSS !dec2! (
      if "!isNegative!"=="1" (
        set result=1
      ) else (
        set result=-1
      )
    ) else (
      if "!isNegative!"=="1" (
        set result=-1
      ) else (
        set result=1
      )
    )
  )
) else (
    if %int1% LSS %int2% (
      set result=-1
    ) else (
      set result=1
    )
)
endlocal & exit /b %result%

:: Perform a calculation
:: calculate "expression" outResult
:calculate
setlocal
set expression=%~1
set resultVar=%~2
for /F "usebackq tokens=1 delims=" %%i in (`"echo %expression%|"%EXE_BC%" -q -l "%BC_FUNCTIONS%""`) do set "result=%%i"
call :clean_decimal "%result%" result
endlocal & set "%resultVar%=%result%"
exit /b

:: Copy a file
:: copy_file "path\input.txt" "path\output.txt"
:copy_file
setlocal
set input=%~1
set output=%~2
copy /B /Y "%input%" "%output%" > NUL
endlocal
exit /b

:: Move a file
:: move_file "path\input.txt" "path\output.txt"
:move_file
setlocal
set input=%~1
set output=%~2
move /Y "%input%" "%output%" > NUL
endlocal
exit /b

:: Delete a file
:: delete_file "path\file.txt"
:delete_file
setlocal
set input=%~1
if exist "%input%" del "%input%" /s/q >NUL
endlocal
exit /b

:: Delete a folder
:: delete_folder "path\folder"
:delete_folder
setlocal
set input=%~1
if exist "%input%" rmdir "%input%" /s/q 2>NUL
endlocal
exit /b

:: Get the complete path to a file, without the trailing slash
:: get_file_path "path\file.txt" outResult
:get_file_path
setlocal
set value=%~dp1
set resultVar=%~2
if "%value:~-1%"=="\" set value=%value:~0,-1%
endlocal & set "%resultVar%=%value%"
exit /b

:: Get the name of a file, without the extension
:: get_file_name "path\file.txt" outResult
:get_file_name
setlocal
set value=%~n1
set resultVar=%~2
endlocal & set "%resultVar%=%value%"
exit /b

:: Get the extension of a file, without the leading dot
:: get_file_ext "path\file.txt" outResult
:get_file_ext
setlocal
set value=%~x1
set resultVar=%~2
if "%value:~0,1%"=="." set value=%value:~1%
endlocal & set "%resultVar%=%value%"
exit /b

:: Return the SHA1 hash of a file
:: get_file_sha1 "path\file.txt" outResult
:get_file_sha1
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getfilesha1temp.txt
set getInfoCommand="%EXE_SHA1SUM%" "%input%"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=1 delims= " %%i in ("%loopFile%") do set "result=%%i"
call :maybe_delete_workfile "%loopFile%"
endlocal & set "%resultVar%=%result%"
exit /b

:: Delete a file if ARG_keepworkdir allows it
:: maybe_delete_workfile "path\file.txt"
:maybe_delete_workfile
setlocal
set workfile=%~1
if "%ARG_keepworkdir%"=="" (
  if exist "%workfile%" del "%workfile%" /s/q >NUL
)
endlocal
exit /b

:: Delete a folder if ARG_keepworkdir allows it
:: maybe_delete_workfolder "path\folder"
:maybe_delete_workfolder
setlocal
set workfolder=%~1
if "%ARG_keepworkdir%"=="" (
  if exist "%workfolder%" rmdir "%workfolder%" /s/q 2>NUL
)
endlocal
exit /b

:: Clean the work directory
:: Modified from https://stackoverflow.com/a/16462274
:cleanup_workdir
setlocal
if not "%ARG_workdir%"=="" (
  if exist "%ARG_workdir%" (
    pushd "%ARG_workdir%"
    if %errorlevel%==0 (
      for /F "usebackq delims=" %%i in (`dir /b`) do (rmdir "%%i" /s/q 2>NUL || del "%%i" /s/q >NUL)
      popd
    )
  )
)
endlocal
exit /b

:: Return the duration of an audio file
:: get_audio_file_duration "path\file.w64" outResult
:get_audio_file_duration
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getaudiodurationtemp.txt
set getInfoCommand=%EXECUTE_SOX% "%input%" -n stats 2^>^&1 ^| findstr /C:"Length s"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=3" %%i in ("%loopFile%") do set "duration=%%i"
call :maybe_delete_workfile "%loopFile%"
call :clean_decimal "%duration%" duration
endlocal & set "%resultVar%=%duration%"
exit /b

:: Return the duration of an audio file
:: get_audio_file_channel_count "path\file.w64" outResult
:get_audio_file_channel_count
setlocal
set input=%~1
set resultVar=%~2
for /F "usebackq tokens=1 delims=" %%i in (`"%EXECUTE_SOXI% -c "%input%""`) do set "channels=%%i"
call :clean_decimal "%channels%" channels
endlocal & set "%resultVar%=%channels%"
exit /b

:: Return the normalization level of an audio file
:: get_audio_file_normalization "path\file.w64" normWith outResult
:get_audio_file_normalization
setlocal
set input=%~1
set normwith=%~2
set resultVar=%~3

set result=
if "%normwith%"=="peak" (
  call :get_audio_file_normalization_peak "%input%" result
) else (
  if "%normwith%"=="rms" (
    call :get_audio_file_normalization_rms "%input%" result
  ) else (
    call :get_audio_file_normalization_r128 "%input%" result
  )
)

endlocal & set "%resultVar%=%result%"
exit /b

:: Normalize an audio file to the given level
:: normalize_audio_file "path\input.w64" "path\output.w64" normDB normWith
:normalize_audio_file
setlocal
set input=%~1
set output=%~2
set norm=%~3
set normwith=%~4

if "%normwith%"=="peak" (
  call :normalize_audio_file_peak "%input%" "%output%" "%norm%"
) else (
  if "%normwith%"=="rms" (
    call :normalize_audio_file_rms "%input%" "%output%" "%norm%"
  ) else (
    call :normalize_audio_file_r128 "%input%" "%output%" "%norm%"
  )
)

endlocal
exit /b

:: Return the peak normalization level of an audio file
:: get_audio_file_normalization_peak "path\file.w64" outResult
:get_audio_file_normalization_peak
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getaudiofilenormalizationpeaktemp.txt
set getInfoCommand=%EXECUTE_SOX% "%input%" -n stats 2^>^&1 ^| findstr /C:"Pk lev dB"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=4" %%i in ("%loopFile%") do set "norm=%%i"
call :maybe_delete_workfile "%loopFile%"
call :clean_decimal "%norm%" norm
call :truncate_decimal "%norm%" 2 norm
endlocal & set "%resultVar%=%norm%"
exit /b

:: Normalize an audio file to the given peak level
:: normalize_audio_file_peak "path\input.w64" "path\output.w64" normDB
:normalize_audio_file_peak
setlocal
set input=%~1
set output=%~2
set norm=%~3
%EXECUTE_SOX% "%input%" "%output%" gain -n %norm%
endlocal
exit /b

:: Return the RMS normalization level of an audio file
:: get_audio_file_normalization_rms "path\file.w64" outResult
:get_audio_file_normalization_rms
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getaudiofilenormalizationrmstemp.txt
set getInfoCommand=%EXECUTE_SOX% "%input%" -n stats 2^>^&1 ^| findstr /C:"RMS lev dB"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=4" %%i in ("%loopFile%") do set "norm=%%i"
call :maybe_delete_workfile "%loopFile%"
call :clean_decimal "%norm%" norm
call :truncate_decimal "%norm%" 2 norm
endlocal & set "%resultVar%=%norm%"
exit /b

:: Return the difference between the RMS normalization level of an audio file and the given value
:: get_audio_file_normalization_delta_rms "path\file.w64" normDB outResult
:get_audio_file_normalization_delta_rms
setlocal
set input=%~1
set norm=%~2
set resultVar=%~3
call :get_audio_file_normalization_rms "%input%" rms
call :calculate "%norm%-(%rms%)" delta
endlocal & set "%resultVar%=%delta%"
exit /b

:: Normalize an audio file to the given RMS level
:: normalize_audio_file_rms "path\input.w64" "path\output.w64" normDB
:normalize_audio_file_rms
setlocal
set input=%~1
set output=%~2
set norm=%~3
call :get_audio_file_normalization_delta_rms "%input%" "%norm%" delta
%EXECUTE_SOX% "%input%" "%output%" gain %delta%
endlocal
exit /b

:: Return the R128 normalization level of an audio file
:: get_audio_file_normalization_r128 "path\file.w64" outResult
:get_audio_file_normalization_r128
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getaudiofilenormalizationr128temp.txt
set getInfoCommand="%EXE_LOUDNESS_SCANNER%" scan "%input%" 2^>NUL ^| findstr /C:" LUFS, "
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=1" %%i in ("%loopFile%") do set "norm=%%i"
call :maybe_delete_workfile "%loopFile%"
if "%norm%"=="-inf" call :get_audio_file_normalization_rms "%input%" norm
call :clean_decimal "%norm%" norm
call :truncate_decimal "%norm%" 2 norm
endlocal & set "%resultVar%=%norm%"
exit /b

:: Return the difference between the R128 normalization level of an audio file and the given value
:: get_audio_file_normalization_delta_r128 "path\file.w64" normDB outResult
:get_audio_file_normalization_delta_r128
setlocal
set input=%~1
set norm=%~2
set resultVar=%~3
call :get_audio_file_normalization_r128 "%input%" r128
call :calculate "%norm%-(%r128%)" delta
endlocal & set "%resultVar%=%delta%"
exit /b

:: Normalize an audio file to the given R128 level
:: normalize_audio_file_r128 "path\input.w64" "path\output.w64" normDB
:normalize_audio_file_r128
setlocal
set input=%~1
set output=%~2
set norm=%~3
call :get_audio_file_normalization_delta_r128 "%input%" "%norm%" delta
%EXECUTE_SOX% "%input%" "%output%" gain %delta%
endlocal
exit /b

:: Trim an audio file to the given duration
:: trim_audio_file "path\input.w64" "path\output.w64" durationSec
:trim_audio_file
setlocal
set input=%~1
set output=%~2
set duration=%~3
%EXECUTE_SOX% "%input%" "%output%" trim 0 %duration%
endlocal
exit /b

:: Repeat an audio file the given number of times
:: For example, one repeat means that the content of the file will be played twice.
:: repeat_audio_file "path\input.w64" "path\output.w64" number
:repeat_audio_file
setlocal
set input=%~1
set output=%~2
set times=%~3
if "%times%"=="" goto repeat_audio_file_done
if "%times%"=="0" (
  call :copy_file "%input%" "%output%"
) else (
  %EXECUTE_SOX% "%input%" "%output%" repeat %times%
)
:repeat_audio_file_done
endlocal
exit /b

:: Repeat an audio file so it has at most the given duration
:: repeat_audio_file_at_most "path\input.w64" "path\output.w64" durationSec
:repeat_audio_file_at_most
setlocal
set input=%~1
set output=%~2
set duration=%~3
call :get_audio_file_duration "%input%" inputDuration
call :calculate "%duration%/%inputDuration%" repeats
call :floor_decimal "%repeats%" repeats
set /a "repeats=%repeats%-1"
if %repeats% LSS 0 set repeats=0
call :repeat_audio_file "%input%" "%output%" "%repeats%"
endlocal
exit /b

:: Repeat an audio file so it has at least the given duration
:: repeat_audio_file_at_least "path\input.w64" "path\output.w64" durationSec
:repeat_audio_file_at_least
setlocal
set input=%~1
set output=%~2
set duration=%~3
call :get_audio_file_duration "%input%" inputDuration
call :calculate "%duration%/%inputDuration%" repeats
call :ceil_decimal "%repeats%" repeats
set /a "repeats=%repeats%-1"
if %repeats% LSS 0 set repeats=0
call :repeat_audio_file "%input%" "%output%" "%repeats%"
endlocal
exit /b

:: Repeat and trim an audio file so it has the exact given duration
:: repeat_and_trim_audio_file "path\input.w64" "path\output.w64" durationSec
:repeat_and_trim_audio_file 
setlocal
set input=%~1
set output=%~2
set duration=%~3
set outputtemp=%ARG_workdir%\repeattrimtemp.w64
call :repeat_audio_file_at_least "%input%" "%outputtemp%" %duration%
call :trim_audio_file "%outputtemp%" "%output%" %duration%
call :maybe_delete_workfile "%outputtemp%"
endlocal
exit /b

:: Add some padding (silence) to the given audio file, at the beginning and/or the end
:: pad_audio_file "path\input.w64" "path\output.w64" beginPaddingSec endPaddingSec
:pad_audio_file
setlocal
set input=%~1
set output=%~2
set beginPadding=%~3
set endPadding=%~4
if "%beginPadding%"=="" set beginPadding=0
if "%endPadding%"=="" set endPadding=0
%EXECUTE_SOX% "%input%" "%output%" pad %beginPadding% %endPadding%
endlocal
exit /b

:: Fade an audio file in the beginning and the end using the given durations
:: fade_audio_file "path\input.w64" "path\output.w64" beginFadeSec endFadeSec
:fade_audio_file
setlocal
set input=%~1
set output=%~2
set beginFade=%~3
set endFade=%~4
if "%beginFade%"=="" set beginFade=0
if "%endFade%"=="" set endFade=0
%EXECUTE_SOX% "%input%" "%output%" fade h %beginFade% -0 %endFade%
endlocal
exit /b

:: Generate a silent audio file
:: generate_silent_audio_file "path\output.w64" durationSec channelCount
:generate_silent_audio_file
setlocal
set output=%~1
set duration=%~2
if "%duration%"=="" set duration=1
set channelCount=%~3
if "%channelCount%"=="" set channelCount=1
%EXECUTE_SOX% -n -r 48000 -b 16 -c %channelCount% "%output%" trim 0 %duration%
endlocal
exit /b

:: Generate a stereo frequency audio file
:: generate_frequency_audio_file "path\output.w64" leftFreq rightFreq durationSec freqNormDb normWith freqPulseSec freqGapSec freqFadeSec "normal|slow|fast"
:generate_frequency_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set leftFreq=%~2
set rightFreq=%~3
set duration=%~4
set freqNorm=%~5
set normwith=%~6
set freqPulse=%~7
set freqGap=%~8
set freqFade=%~9
shift
set freqRotation=%~9
set isIsochronic=1
if "%freqPulse%"=="" set isIsochronic=0
if "%freqGap%"=="" (
  if "%freqFade%"=="" set isIsochronic=0
)
if "%freqRotation%"=="normal" (
  set freqRotation=%ROTATION_NORMAL%
) else (
  if "%freqRotation%"=="slow" (
    set freqRotation=%ROTATION_SLOW%
  ) else (
    if "%freqRotation%"=="fast" (
      set freqRotation=%ROTATION_FAST%
    ) else (
      set freqRotation=
    )
  )
)
set outputtemp=%ARG_workdir%\freqtemp.w64
set outputtemp2=%ARG_workdir%\freqtemp2.w64

if "%isIsochronic%"=="0" (
  %EXECUTE_SOX% -n -r 48000 -b 16 -c 2 "%outputtemp%" synth -n 30 sine %leftFreq% sine %rightFreq% gain -n -30
) else (
  %EXECUTE_SOX% -n -r 48000 -b 16 -c 2 "%outputtemp%" synth -n %freqPulse% sine %leftFreq% sine %rightFreq% gain -n -30
  if "%freqFade%"=="" (
    call :move_file "%outputtemp%" "%outputtemp2%"
  ) else (
    call :fade_audio_file "%outputtemp%" "%outputtemp2%" "%freqFade%" "%freqFade%"
  )
  %EXECUTE_SOX% "%outputtemp2%" "%outputtemp%" pad 0 %freqGap% repeat 30
)

call :normalize_audio_file "%outputtemp%" "%outputtemp2%" "%freqNorm%" "%normwith%"

call :repeat_and_trim_audio_file "%outputtemp2%" "%outputtemp%" %duration%
if "%freqFade%"=="" (
  call :move_file "%outputtemp%" "%outputtemp2%"
) else (
  if "%isIsochronic%"=="0" (
    call :fade_audio_file "%outputtemp%" "%outputtemp2%" "%freqFade%" "%freqFade%"
  ) else (
    call :fade_audio_file "%outputtemp%" "%outputtemp2%" 0 "%freqFade%"
  )
)

if "%freqRotation%"=="" (
  call :move_file "%outputtemp2%" "%output%"
) else (
  if "%leftFreq%"=="%rightFreq%" (
    call :generate_rotating_audio_file "%output%" "%outputtemp2%" "%freqRotation%" "-90"
  ) else (
    call :generate_rotating_audio_file "%output%" "%outputtemp2%" "%freqRotation%" "-180" "0"
  )
)

call :maybe_delete_workfile "%outputtemp%"
call :maybe_delete_workfile "%outputtemp2%"
endlocal
exit /b

:: Generate a noise audio file
:: generate_noise_audio_file "path\output.w64" white|pink|brown durationSec noiseNormDb normWith noiseFadeSec channelCount
:generate_noise_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set noise=%~2
set duration=%~3
set noiseNorm=%~4
set normwith=%~5
set noiseFade=%~6
set channelCount=%~7
if "%channelCount%"=="" set channelCount=1
set outputtemp=%ARG_workdir%\noisetemp.w64
set outputtemp2=%ARG_workdir%\noisetemp2.w64

set noiseType=
if "%noise%"=="white" set noiseType=whitenoise
if "%noise%"=="whitenoise" set noiseType=whitenoise
if "%noise%"=="pink" set noiseType=pinknoise
if "%noise%"=="pinknoise" set noiseType=pinknoise
if "%noise%"=="brown" set noiseType=brownnoise
if "%noise%"=="brownnoise" set noiseType=brownnoise

set makeWavCommand=%EXECUTE_SOX% -n -r 48000 -b 16 -c %channelCount% "%outputtemp%" synth -n %duration%
for /L %%i in (1,1,%channelCount%) do set makeWavCommand=!makeWavCommand! %noiseType%
set makeWavCommand=%makeWavCommand% gain -n -30
%makeWavCommand%

call :normalize_audio_file "%outputtemp%" "%outputtemp2%" "%noiseNorm%" "%normwith%"

if "%noiseFade%"=="" (
  call :move_file "%outputtemp2%" "%output%"
) else (
  call :fade_audio_file "%outputtemp2%" "%output%" "%noiseFade%" "%noiseFade%"
)

call :maybe_delete_workfile "%outputtemp%"
call :maybe_delete_workfile "%outputtemp2%"
endlocal
exit /b

:: Generate an ultrasonic audio file
:: generate_ultrasonic_audio_file "path\output.w64" "path\input.w64" carrierFreq normDB normWith
:generate_ultrasonic_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set input=%~2
set carrier=%~3
set norm=%~4
set normwith=%~5
set outputtemp=%ARG_workdir%\ultrasonictemp.w64
set outputtemp2=%ARG_workdir%\ultrasonictemp2.w64

%EXECUTE_SOX% "%input%" -b 16 "%outputtemp%" rate 96000

set slashedInput=%outputtemp:\=/%
set slashedOutputtemp=%outputtemp2:\=/%
set ultrasonicparamstemp=%ARG_workdir%\ultrasonicparams.lsp
type NUL > "%ultrasonicparamstemp%"
echo (setf *input-file* "%slashedInput%") >> "%ultrasonicparamstemp%"
echo (setf *output-file* "%slashedOutputtemp%") >> "%ultrasonicparamstemp%"
echo (setf *carrier* %carrier%) >> "%ultrasonicparamstemp%"
pushd "%NYQUIST_DIR%"
"%EXE_NYQUIST%" "%ultrasonicparamstemp%" "%LSP_ULTRASONIC%" > NUL
popd
call :maybe_delete_workfile "%ultrasonicparamstemp%"

%EXECUTE_SOX% "%outputtemp2%" -b 16 "%outputtemp%" rate 48000

if "%norm%"=="" (
  call :move_file "%outputtemp%" "%output%"
) else (
  call :normalize_audio_file "%outputtemp%" "%output%" "%norm%" "%normwith%"
)

call :maybe_delete_workfile "%outputtemp%"
call :maybe_delete_workfile "%outputtemp2%"
endlocal
exit /b

:: Generate a rotating audio file
:: The rate (hz) is a number like 1.0
:: The phase is a number between -180..0 defining the initial positioning from left to right
:: generate_rotating_audio_file "path\output.w64" "path\input.w64" rate phaseL phaseR
:generate_rotating_audio_file
setlocal
set output=%~1
set input=%~2
set rate=%~3
set phaseL=%~4
set phaseR=%~5
if "%phaseR%"=="" set phaseR=%phaseL%
set outputtemp=%ARG_workdir%\rotatingtemp.w64
set outputtemp2=%ARG_workdir%\rotatingtemp2.w64
set outputtemp3=%ARG_workdir%\rotatingtemp3.w64

%EXECUTE_SOX% "%input%" "%outputtemp%" remix -m 1 1
set phase=%phaseL%
call :_generate_rotating_audio_file_internal
call :get_audio_file_channel_count "%input%" count
if %count% LSS 2 goto generate_rotating_audio_file_done

call :move_file "%outputtemp2%" "%outputtemp3%"
%EXECUTE_SOX% "%input%" "%outputtemp%" remix -m 2 2
set phase=%phaseR%
call :_generate_rotating_audio_file_internal
call :mix_audio_files "%outputtemp%" "%outputtemp2%" "%outputtemp3%"
call :move_file "%outputtemp%" "%outputtemp2%"

:generate_rotating_audio_file_done
call :move_file "%outputtemp2%" "%output%"

call :maybe_delete_workfile "%outputtemp%"
call :maybe_delete_workfile "%outputtemp2%"
call :maybe_delete_workfile "%outputtemp3%"
endlocal
exit /b

:: Internal code of generate_rotating_audio_file, which works on a two channels stereo-ized mono-file, do not call directly
:_generate_rotating_audio_file_internal

set slashedInput=%outputtemp:\=/%
set slashedOutputtemp=%outputtemp2:\=/%
set rotatingparamstemp=%ARG_workdir%\rotatingparams.lsp
type NUL > "%rotatingparamstemp%"
echo (setf *input-file* "%slashedInput%") >> "%rotatingparamstemp%"
echo (setf *output-file* "%slashedOutputtemp%") >> "%rotatingparamstemp%"
echo (setf *rate* %rate%) >> "%rotatingparamstemp%"
echo (setf *phase* %phase%) >> "%rotatingparamstemp%"
echo (setf *left* 0) >> "%rotatingparamstemp%"
echo (setf *right* 100) >> "%rotatingparamstemp%"

pushd "%NYQUIST_DIR%"
"%EXE_NYQUIST%" "%rotatingparamstemp%" "%LSP_ROTATION%" > NUL
popd
call :maybe_delete_workfile "%rotatingparamstemp%"

exit /b

:: Generate a panned audio file using the given pan-method
:: generate_panned_audio_file "path\stereooutput.w64" "panlaw-0|panlaw-3|panlaw-4.5|panlaw-6|rotation|rotation-slow|rotation-fast" "path\channel1.w64" "path\channel2.w64" "path\channel3.w64" "path\channel4.w64" "path\channel5.w64" "path\channel6.w64" "path\channel7.w64"
:generate_panned_audio_file
setlocal
set output=%~1
set panmethod=%~2
shift
shift

set fileCount=0
:generate_panned_audio_file_get_file_list
if "%~1"=="" goto generate_panned_audio_file_get_file_list_done
set /a "fileCount=%fileCount%+1"
set fileList[%fileCount%]=%~1
shift
goto generate_panned_audio_file_get_file_list
:generate_panned_audio_file_get_file_list_done

if "%panmethod%"=="panlaw-0" (
  call :generate_panlaw_panned_audio_file "%output%" fileList "0"
) else (
  if "%panmethod%"=="panlaw-3" (
    call :generate_panlaw_panned_audio_file "%output%" fileList "-3"
  ) else (
    if "%panmethod%"=="panlaw-4.5" (
      call :generate_panlaw_panned_audio_file "%output%" fileList "-4.5"
    ) else (
      if "%panmethod%"=="panlaw-6" (
        call :generate_panlaw_panned_audio_file "%output%" fileList "-6"
      ) else (
        if "%panmethod%"=="rotation" (
          call :generate_rotation_panned_audio_file "%output%" fileList "%ROTATION_NORMAL%"
        ) else (
          if "%panmethod%"=="rotation-slow" (
            call :generate_rotation_panned_audio_file "%output%" fileList "%ROTATION_SLOW%"
          ) else (
            if "%panmethod%"=="rotation-fast" (
              call :generate_rotation_panned_audio_file "%output%" fileList "%ROTATION_FAST%"
            ) else (
              call :generate_unpanned_audio_file "%output%" fileList
            )
          )
        )
      )
    )
  )
)

endlocal
exit /b

:: Generate an unpanned (dual mono) audio file
:: generate_unpanned_audio_file "path\stereooutput.w64" fileArray
:generate_unpanned_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set fileArray=%~2

call :get_array_length "%fileArray%" fileCount
set fileList=
for /L %%i in (1,1,%fileCount%) do set fileList=!fileList! "!%fileArray%[%%i]!"

if "%fileCount%"=="1" (
  %EXECUTE_SOX% %fileList% "%output%" remix -m 1 1
) else (
  %EXECUTE_SOX% -M %fileList% "%output%" remix -m - -
)

endlocal
exit /b

:: Generate an audio file panned using the given pan law
:: Pan laws are either neutral -0dB, linear -6dB, constant power -3dB, or middleground -4.5dB
:: generate_panlaw_panned_audio_file "path\stereooutput.w64" fileArray "0|-3|-4.5|-6"
:: Assuming positions are given in radians in the [0;pi/2] interval, the equations are :
:: ** Neutral pan law (-0dB) : https://forum.audacityteam.org/viewtopic.php?t=109363
:: leftLoudnessFactor(position)=min(1, 1-(position-pi/4)/(pi/4))
:: rightLoudnessFactor(position)=min(1, 1-(pi/4-position)/(pi/4))
:: ** Constant power pan law (-3dB) : http://www.cs.cmu.edu/~music/icm-online/readings/panlaws/panlaws.pdf
:: leftVoltageFactor(position)=cos(position)
:: rightVoltageFactor(position)=sin(position)
:: ** Middleground pan law (-4.5dB) : http://www.cs.cmu.edu/~music/icm-online/readings/panlaws/panlaws.pdf
:: leftVoltageFactor(position)=sqrt((pi/2-position)*(2/pi)*cos(position))
:: rightVoltageFactor(position)=sqrt(position*(2/pi)*sin(position))
:: ** Linear pan law (-6dB) : http://www.cs.cmu.edu/~music/icm-online/readings/panlaws/panlaws.pdf
:: leftVoltageFactor(position)=(pi/2-position)*(2/pi)
:: rightVoltageFactor(position)=position*(2/pi)
:generate_panlaw_panned_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set fileArray=%~2
set panlaw=%~3

call :get_array_length "%fileArray%" fileCount
set fileList=
for /L %%i in (1,1,%fileCount%) do set fileList=!fileList! "!%fileArray%[%%i]!"

set channelCount=0
for /L %%i in (1,1,%fileCount%) do (
  call :get_audio_file_channel_count "!%fileArray%[%%i]!" count
  set /a "channelCount=!channelCount!+!count!"
)
set /a "channelCountMinusOne=%channelCount%-1"

if "%channelCount%"=="1" (
  set positions[1]=0.5*pi/2
  goto generate_panlaw_panned_audio_file_get_positions_done
)
set positions[1]=0*pi/2
set positions[%channelCount%]=1*pi/2
if "%channelCount%"=="2" goto generate_panlaw_panned_audio_file_get_positions_done
call :calculate "1/%channelCountMinusOne%" ratio
for /L %%i in (2,1,%channelCountMinusOne%) do (
  call :calculate "(%%i-1)*%ratio%" position
  set positions[%%i]=!position!*pi/2
)
:generate_panlaw_panned_audio_file_get_positions_done

for /L %%i in (1,1,%channelCount%) do (
  set position=!positions[%%i]!
  if "%panlaw%"=="-3" (
    call :calculate "c(!position!)" leftVoltage[%%i]
    call :calculate "s(!position!)" rightVoltage[%%i]
  ) else (
    if "%panlaw%"=="-4.5" (
      call :calculate "sqrt((pi/2-(!position!))*(2/pi)*c(!position!))" leftVoltage[%%i]
      call :calculate "sqrt((!position!)*(2/pi)*s(!position!))" rightVoltage[%%i]
    ) else (
      if "%panlaw%"=="-6" (
        call :calculate "(pi/2-(!position!))*(2/pi)" leftVoltage[%%i]
        call :calculate "(!position!)*(2/pi)" rightVoltage[%%i]
      ) else (
        call :calculate "min(1, 1-(!position!-(0.5*pi/2))/(0.5*pi/2))" leftLoudness[%%i]
        call :calculate "min(1, 1-((0.5*pi/2)-!position!)/(0.5*pi/2))" rightLoudness[%%i]
        call :calculate_voltage_factor_for_loudness_factor "!leftLoudness[%%i]!" leftVoltage[%%i]
        call :calculate_voltage_factor_for_loudness_factor "!rightLoudness[%%i]!" rightVoltage[%%i]
      )
    )
  )
  call :truncate_decimal "!leftVoltage[%%i]!" 3 leftVoltage[%%i]
  call :truncate_decimal "!rightVoltage[%%i]!" 3 rightVoltage[%%i]
)
if not "%channelCount%"=="1" (
  set leftVoltage[1]=1& set rightVoltage[1]=0
  set leftVoltage[%channelCount%]=0& set rightVoltage[%channelCount%]=1
)

set leftRemix=
set rightRemix=
for /L %%i in (1,1,%channelCount%) do (
  set v=!leftVoltage[%%i]!
  if not "!v!"=="0" (
    if not "!leftRemix!"=="" set leftRemix=!leftRemix!,
    set leftRemix=!leftRemix!%%i
    if not "!v!"=="1" set leftRemix=!leftRemix!v!v!
  )
  set v=!rightVoltage[%%i]!
  if not "!v!"=="0" (
    if not "!rightRemix!"=="" set rightRemix=!rightRemix!,
    set rightRemix=!rightRemix!%%i
    if not "!v!"=="1" set rightRemix=!rightRemix!v!v!
  )
)

if "%channelCount%"=="1" (
  %EXECUTE_SOX% %fileList% "%output%" remix -m %leftRemix% %rightRemix%
) else (
  %EXECUTE_SOX% -M %fileList% "%output%" remix -m %leftRemix% %rightRemix%
)

endlocal
exit /b

:: Calculate the sound voltage factor associated with the given loudness factor
:: calculate_voltage_factor_for_loudness_factor number outResult
:: Formula : dbChange=10*log2(loudnessRatio) ; voltageRatio=pow(10, dbChange/20)
:: Technical info and calculator : http://www.sengpielaudio.com/calculator-levelchange.htm
:: x1 loudness = 0 dB change = x1 volts
:: x0.75 loudness = -4.150 dB change = x0.620 V
:: x0.67 loudness = -5.778 dB change = x0.514 V
:: x0.5 loudness = -10 dB change = x0.316 V
:: x0.33 loudness = -15.995 dB change = x0.158 V
:: x0.25 loudness = -20 dB change = x0.1 V
:: x0 loudness = -infinity dB change = x0 V
:calculate_voltage_factor_for_loudness_factor
setlocal
set loudness=%~1
set resultVar=%~2

if "%loudness%"=="0" (
  set result=0
  goto calculate_voltage_factor_for_loudness_factor_done
)
if "%loudness%"=="1" (
  set result=1
  goto calculate_voltage_factor_for_loudness_factor_done
)
call :calculate "pow(10,(10*log2(%loudness%))/20)" result

:calculate_voltage_factor_for_loudness_factor_done
endlocal & set "%resultVar%=%result%"
exit /b

:: Generate an audio file panned using stereo rotation
:: generate_rotation_panned_audio_file "path\stereooutput.w64" fileArray rate
:generate_rotation_panned_audio_file
setlocal EnableDelayedExpansion
set output=%~1
set fileArray=%~2
set rate=%~3

call :get_array_length "%fileArray%" fileCount
for /L %%i in (1,1,%fileCount%) do set tempArray[%%i]=%ARG_workdir%\rotation%%i.w64

if "%fileCount%"=="1" (
  set phaseArray[1]=-90
  goto generate_rotation_panned_audio_file_perform
)

set /a "hops=%fileCount%-1"
call :calculate "1/%hops%" hopRatio
for /L %%i in (1,1,%fileCount%) do (
  call :calculate "-180+(%hopRatio%*180*(%%i-1))" "phaseArray[%%i]"
  call :truncate_decimal "!phaseArray[%%i]!" 3 phaseArray[%%i]
)

:generate_rotation_panned_audio_file_perform
for /L %%i in (1,1,%fileCount%) do (
  call :delete_file "!%tempArray%[%%i]!"
  call :generate_rotating_audio_file "!tempArray[%%i]!" "!%fileArray%[%%i]!" "%rate%" !phaseArray[%%i]!
)

set tempList=
for /L %%i in (1,1,%fileCount%) do set tempList=!tempList! "!tempArray[%%i]!"
call :mix_audio_files "%output%" %tempList%

for /L %%i in (1,1,%fileCount%) do call :maybe_delete_workfile "!tempArray[%%i]!"

endlocal
exit /b

:: Concatenate multiple audio files having the same number of channels
:: The result will be a file with the same number of channels, containing the concatenation of the audios
:: concat_audio_files "path\output.w64" "path\file1.w64" "path\file2.w64" "path\file3.w64" "path\file4.w64" "path\file5.w64" "path\file6.w64" "path\file7.w64"
:concat_audio_files
setlocal EnableDelayedExpansion
set output=%~1

set fileCount=0
set fileList=
:concat_audio_files_get_file_list
shift
if "%~1"=="" goto concat_audio_files_get_file_list_done
set /a "fileCount=%fileCount%+1"
set file%fileCount%=%~1
set fileList=%fileList% "%~1"
goto concat_audio_files_get_file_list
:concat_audio_files_get_file_list_done

if "%fileCount%"=="1" (
  %EXECUTE_SOX% "%file1%" "%output%"
  goto concat_audio_files_done
)
%EXECUTE_SOX% %fileList% "%output%"
:concat_audio_files_done

endlocal
exit /b

:: Merge together multiple audio files
:: The result will be a file with each channel containing the audio of each channel in the source files
:: merge_audio_files "path\output.w64" "path\file1.w64" "path\file2.w64" "path\file3.w64" "path\file4.w64" "path\file5.w64" "path\file6.w64" "path\file7.w64"
:merge_audio_files
setlocal EnableDelayedExpansion
set output=%~1

set fileCount=0
set fileList=
:merge_audio_files_get_file_list
shift
if "%~1"=="" goto merge_audio_files_get_file_list_done
set /a "fileCount=%fileCount%+1"
set file%fileCount%=%~1
set fileList=%fileList% "%~1"
goto merge_audio_files_get_file_list
:merge_audio_files_get_file_list_done

if "%fileCount%"=="1" (
  %EXECUTE_SOX% "%file1%" "%output%"
  goto merge_audio_files_done
)
%EXECUTE_SOX% -M %fileList% "%output%"
:merge_audio_files_done

endlocal
exit /b

:: Mix together multiple audio files having the same number of channels
:: The result will be a file with the same number of channels, with each channel containing the combination of the corresponding channels in the source files
:: mix_audio_files "path\output.w64" "path\file1.w64" "path\file2.w64" "path\file3.w64" "path\file4.w64" "path\file5.w64" "path\file6.w64" "path\file7.w64"
:mix_audio_files
setlocal EnableDelayedExpansion
set output=%~1

set fileCount=0
set fileList=
:mix_audio_files_get_file_list
shift
if "%~1"=="" goto mix_audio_files_get_file_list_done
set /a "fileCount=%fileCount%+1"
set file%fileCount%=%~1
set fileList=%fileList% "%~1"
goto mix_audio_files_get_file_list
:mix_audio_files_get_file_list_done

if "%fileCount%"=="1" (
  %EXECUTE_SOX% "%file1%" "%output%"
  goto mix_audio_files_done
)
call :get_audio_file_channel_count "%file1%" channelCount
set remixParams=
for /L %%i in (1,1,%channelCount%) do (
  if not "!remixParams!"=="" set remixParams=!remixParams! 
  for /L %%j in (1,1,%fileCount%) do (
    set /a "ch=(%%i)+(%%j-1)*%channelCount%"
    if not "%%j"=="1" set remixParams=!remixParams!,
    set remixParams=!remixParams!!ch!
  )
)
%EXECUTE_SOX% -M %fileList% "%output%" remix -m %remixParams%
:mix_audio_files_done

endlocal
exit /b

:: Return the duration of a video file
:: get_video_file_duration "path\video.mp4" outResult
:get_video_file_duration
setlocal
set input=%~1
set resultVar=%~2
for /F "usebackq tokens=1" %%i in (`""%EXE_FFPROBE%" -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "%input%""`) do set "duration=%%i"
call :clean_decimal "%duration%" duration
endlocal & set "%resultVar%=%duration%"
exit /b

:: Return the peak normalization level of a video file
:: get_video_file_normalization_peak "path\file.mp4" outResult
:get_video_file_normalization_peak
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getvideofilenormalizationpeaktemp.txt
set getInfoCommand="%EXE_FFMPEG%" -nostdin -hide_banner -nostats -i "%input%" -af volumedetect -f null - 2^>^&1 ^| findstr /C:"max_volume"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=5" %%i in ("%loopFile%") do set "norm=%%i"
call :maybe_delete_workfile "%loopFile%"
call :clean_decimal "%norm%" norm
call :truncate_decimal "%norm%" 2 norm
endlocal & set "%resultVar%=%norm%"
exit /b

:: Return the RMS normalization level of a video file
:: get_video_file_normalization_rms "path\file.mp4" outResult
:get_video_file_normalization_rms
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getvideofilenormalizationrmstemp.txt
set getInfoCommand="%EXE_FFMPEG%" -nostdin -hide_banner -nostats -i "%input%" -af volumedetect -f null - 2^>^&1 ^| findstr /C:"mean_volume"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=5" %%i in ("%loopFile%") do set "norm=%%i"
call :maybe_delete_workfile "%loopFile%"
call :clean_decimal "%norm%" norm
call :truncate_decimal "%norm%" 2 norm
endlocal & set "%resultVar%=%norm%"
exit /b

:: Return the R128 normalization level of a video file
:: get_video_file_normalization_r128 "path\file.mp4" outResult
:get_video_file_normalization_r128
setlocal
set input=%~1
set resultVar=%~2
set loopFile=%ARG_workdir%\getvideofilenormalizationr128temp.txt
set getInfoCommand="%EXE_FFMPEG%" -nostdin -hide_banner -nostats -i "%input%" -af loudnorm=print_format=summary -f null - 2^>^&1 ^| findstr /C:"Input Integrated:"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=3" %%i in ("%loopFile%") do set "norm=%%i"
call :maybe_delete_workfile "%loopFile%"
if "%norm%"=="-inf" call :get_video_file_normalization_rms "%input%" norm
call :clean_decimal "%norm%" norm
call :truncate_decimal "%norm%" 2 norm
endlocal & set "%resultVar%=%norm%"
exit /b

:: Return the dimensions (width and height) of the given video or image file
:: get_video_file_dimensions "path\image.jpg" outWidth outHeight
:get_video_file_dimensions
setlocal
set input=%~1
set resultVarWidth=%~2
set resultVarHeight=%~3
set width=
set height=
set loopFile=%ARG_workdir%\getvideofiledimensionstemp.txt
set getInfoCommand="%EXE_FFPROBE%" -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "%input%"
%getInfoCommand% > "%loopFile%"
for /F "usebackq tokens=1,2 delims=x" %%i in ("%loopFile%") do (
  set width=%%i
  set height=%%j
)
call :maybe_delete_workfile "%loopFile%"
call :clean_decimal "%width%" width
call :clean_decimal "%height%" height
if "%resultVarHeight%"=="" (
  set setResultCommand=set "%resultVarWidth%=%width%x%height%"
) else (
  set setResultCommand=set "%resultVarWidth%=%width%" ^& set "%resultVarHeight%=%height%"
)
endlocal & %setResultCommand%
exit /b

:: Return the framerate (fps) of the given video file
:: get_video_file_framerate "path\video.mp4" outResult
:get_video_file_framerate
setlocal
set input=%~1
set resultVar=%~2
for /F "usebackq tokens=1" %%i in (`""%EXE_FFPROBE%" -v error -select_streams v -of default=noprint_wrappers=1:nokey=1 -show_entries stream=r_frame_rate "%input%""`) do set "framerate=%%i"
call :calculate "%framerate%" framerate
endlocal & set "%resultVar%=%framerate%"
exit /b

:: Generate a video slideshow from either an image file or a folder of images
:: generate_video_slideshow "path\output.mp4" "path\image.jpg" auto|image|240p|360p|480p|720p|1080p|1440p|2160p auto|contain|cover|stretch delaySec
:: generate_video_slideshow "path\output.mp4" "path\to\folder" auto|image|240p|360p|480p|720p|1080p|1440p|2160p auto|contain|cover|stretch delaySec
:generate_video_slideshow
setlocal EnableDelayedExpansion
set output=%~1
set image=%~2
set videosize=%~3
set videosizemode=%~4
set delay=%~5

if "%image%"=="" set image=%IMG_DEFAULT1080P%
if exist "%image%\" (
  set empty=1
  for /F "usebackq tokens=*" %%i IN (`dir "%image%\*.jpg" "%image%\*.png" /b /s /o:n 2^>NUL`) do set empty=
  if "!empty!"=="1" set image=%IMG_DEFAULT1080P%
) else (
  if not exist "%image%" set image=%IMG_DEFAULT1080P%
)

if exist "%image%\" (
  set path=
  for /F "usebackq tokens=*" %%i IN (`dir "%image%\*.jpg" "%image%\*.png" /b /s /o:n`) do (
    if "!path!"=="" set path=%%i
  )
  call :get_video_file_dimensions "!path!" sourceWidth sourceHeight
) else (
  call :get_video_file_dimensions "%image%" sourceWidth sourceHeight
)

if "%videosize%"=="240p" (
  set "targetWidth=426" & set "targetHeight=240"
) else (
  if "%videosize%"=="360p" (
    set "targetWidth=640" & set "targetHeight=360"
  ) else (
    if "%videosize%"=="480p" (
      set "targetWidth=854" & set "targetHeight=480"
    ) else (
      if "%videosize%"=="720p" (
        set "targetWidth=1280" & set "targetHeight=720"
      ) else (
        if "%videosize%"=="1080p" (
          set "targetWidth=1920" & set "targetHeight=1080"
        ) else (
          if "%videosize%"=="1440p" (
            set "targetWidth=2560" & set "targetHeight=1440"
          ) else (
            if "%videosize%"=="2160p" (
              set "targetWidth=3840" & set "targetHeight=2160"
            ) else (
              if "%videosize%"=="image" (
                set "targetWidth=%sourceWidth%" & set "targetHeight=%sourceHeight%"
                set /a "n=!targetWidth!%%2"
                if not "!n!"=="0" set /a "targetWidth=!targetWidth!+1"
                set /a "n=!targetHeight!%%2"
                if not "!n!"=="0" set /a "targetHeight=!targetHeight!+1"
              ) else (
                set "targetWidth=426" & set "targetHeight=240"
                if %sourceHeight% GEQ 360 set "targetWidth=640" & set "targetHeight=360"
                if %sourceHeight% GEQ 480 set "targetWidth=854" & set "targetHeight=480"
                if %sourceHeight% GEQ 720 set "targetWidth=1280" & set "targetHeight=720"
                if %sourceHeight% GEQ 1080 set "targetWidth=1920" & set "targetHeight=1080"
                if %sourceHeight% GEQ 1440 set "targetWidth=2560" & set "targetHeight=1440"
                if %sourceHeight% GEQ 2160 set "targetWidth=3840" & set "targetHeight=2160"
              )
            )
          )
        )
      )
    )
  )
)

call :get_file_name "%output%" slideshowID
set FOLDER_CONVERT=%ARG_workdir%\%slideshowID%
call :delete_folder "%FOLDER_CONVERT%"
mkdir "%FOLDER_CONVERT%" 2> NUL

set filterContain=scale=%targetWidth%:%targetHeight%:force_original_aspect_ratio=decrease,pad=%targetWidth%:%targetHeight%:-1:-1:color=black
set filterCover=scale=%targetWidth%:%targetHeight%:force_original_aspect_ratio=increase,crop=%targetWidth%:%targetHeight%
set filterStretch=scale=%targetWidth%:%targetHeight%
set filterAuto=scale=-2:%targetHeight%
set resizeFilter=
if "%videosizemode%"=="contain" set resizeFilter=%filterContain%
if "%videosizemode%"=="cover" set resizeFilter=%filterCover%
if "%videosizemode%"=="stretch" set resizeFilter=%filterStretch%
if "%resizeFilter%"=="" (
  if exist "%image%\" (
    set resizeFilter=%filterContain%
  ) else (
    set resizeFilter=%filterAuto%
  )
)
if exist "%image%\" (
  set count=0
  for /F "usebackq tokens=*" %%i IN (`dir "%image%\*.jpg" "%image%\*.png" /b /s /o:n`) do (
    set /a "count=!count!+1"
    if !count! LEQ 2000000000 (
      set convert=0000000000!count!
      set convert=!convert:~-10!
      set convert=!convert!.png
      "%EXE_FFMPEG%" -v error -nostdin -y -i "%%i" -vf "%resizeFilter%" -f image2 "%FOLDER_CONVERT%\!convert!"
    )
  )
) else (
  "%EXE_FFMPEG%" -v error -nostdin -y -i "%image%" -vf "%resizeFilter%" -f image2 "%FOLDER_CONVERT%\image.png"
)

set concatScript=%ARG_workdir%\%slideshowID%.ffconcat
call :delete_file "%concatScript%"
set fileCount=0
for /F "usebackq tokens=*" %%i IN (`dir "%FOLDER_CONVERT%" /b /s`) do (
  set /a "fileCount=!fileCount!+1"
  set file=%%i
  set file=!file:'='\''!
  echo file '!file!'>>"%concatScript%"
  echo duration %delay% >>"%concatScript%"
)

if %fileCount% GTR 1 (
  call :truncate_decimal "%delay%" trunc
  call :calculate "%delay%-!trunc!" dec
  set framerate=60
  call :compare_decimal "!dec!" 0.033
  if !errorlevel! GEQ 0 set framerate=30
  call :compare_decimal "!dec!" 0.04
  if !errorlevel! GEQ 0 set framerate=25
  call :compare_decimal "!dec!" 0.041
  if !errorlevel! GEQ 0 set framerate=24
  call :compare_decimal "!dec!" 0
  if !errorlevel! EQU 0 set framerate=2
) else (
  set framerate=2
)
"%EXE_FFMPEG%" -v error -nostdin -y -f concat -safe 0 -i "%concatScript%" -c:v libx264 -preset medium -tune stillimage -crf 18 -r %framerate% -pix_fmt yuv420p -sws_flags spline+accurate_rnd+full_chroma_int -vf "colorspace=bt709:iall=bt601-6-625:fast=1" -color_range 1 -colorspace 1 -color_primaries 1 -color_trc 1 "%output%"

call :maybe_delete_workfile "%concatScript%"
call :maybe_delete_workfolder "%FOLDER_CONVERT%"

endlocal
exit /b
