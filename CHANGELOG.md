# cbsubgen & cbsubgen-workshop changelog


## [0.19] - 2023-04-05

### Changes
- Add the rotation, rotation-slow and rotation-fast pan methods, which rotate the layers at the given speed when panning
- Apply a stereo rotation effect of the given speed to the generated frequencies with /freqrotation normal|slow|fast
- Apply a stereo rotation effect of the given speed to the background music with /backgroundrotation normal|slow|fast
- Add examples for rotation related configurations
- Update bal4web binaries to version 1.49

### Bug fixes
- Empty double-quotes passed as command line parameters of cbsubgen.bat won't stop the parsing of arguments


## [0.18] - 2023-03-05

### Changes
- Generate background noise with /noise white|pink|brown
- Control noise normalization and fade with /noisenorm number and /noisefade duration
- Apply a bandpass filter to a layer by using /affbandpassN number number
- Apply a bandreject filter to a layer by using /affbandrejectN number number
- Control the band filters width using /bandfilterwidth number
- Update bal4web binaries to version 1.46

### Bug fixes
- Command line parameters of cbsubgen.bat can now be passed without double quotes (eg /pad 123 456 instead of /pad "123 456")

### Note on breaking changes
- The /pan audacity fallback has been removed, you have to use /pan panlaw-0 instead


## [0.17] - 2022-08-19

### Changes
- Use /freqfade duration to add a fade effect to the beginning and the end of the frequency (or of each isochronic pulse)
- Rename the audacity pan method into panlaw-0 (panlaw minus 0), which doesn't reduce the volume in the center when panning
- Add the panlaw-3 (panlaw minus 3) pan method, which reduces the volume in the center by 3dB when panning
- Add the panlaw-4.5 (panlaw minus 4.5) pan method, which reduces the volume in the center by 4.5dB when panning
- Add the panlaw-6 (panlaw minus 6) pan method, which reduces the volume in the center by 6dB when panning
- Remove the downmix pan method
- Add an example for comparing pan methods
- Change the colors of the workshop when it is run by double click

### Bug fixes
- Fix random ffmpeg errors "error parsing debug values" due to ffmpeg trying to read from stdin
- Marginally better accuracy of calculations in the panlaw-0 (previously audacity) pan method
- Do not perform any panning if a bogus pan setting is passed

### Note on breaking changes
- The audacity pan method has been renamed to panlaw-0. You can still use the old audacity name which will fallback to panlaw-0, but in the next versions this will change, so update your files accordingly.
- The downmix pan method has been removed. It wasn't as scalable as the others and didn't respect the equal distance between layers requirement.
- In previous versions passing a bogus parameter to /pan would cause the audacity pan method to be used. This isn't the case anymore, a bogus parameter will cause the audio to be unpanned.


## [0.16] - 2022-08-10

### Changes
- Generate slideshow video subliminals by passing a folder instead of a file to /outputvideoimage
- Use /slideshowdelay duration to set the duration between images in video slideshows
- Use /videoaudioencoding aac|flac|alac to switch the audio encoding of videos from lossy (AAC, the default) to lossless (FLAC or ALAC)
- Display the video framerate in statistics
- Reduce FLAC compression to speed up FLAC audio output generation (around 3x to 4x faster with files larger by 5%)
- Round the duration of complete subliminals to the closest higher integer (eg from 45.4sec to 46sec)
- Update the ffmpeg build to support slideshows and FLAC / ALAC audio

### Bug fixes
- Fix the colorspace of videos (use BT 709)


## [0.15.1] - 2022-08-05

### Changes
- Use /videosize to specify the size of the generated videos (auto, 1080p...)
- Use /videosizemode to specify the size mode of the generated videos (stretch, fit with black bars, cropped...)
- Support 240p, 360p and 480p sizes in addition to the already supported HD sizes
- Update the ffmpeg build to support cropping videos


## [0.15] - 2022-08-03

### Changes
- Generate still-image video subliminals (MP4 container, H.264 video, AAC CBR 320 audio) that can be uploaded to online services (ex : YouTube)
- Make /outputfile optional (as long another output setting is provided)
- Add a custom build of ffmpeg for generating videos
- Rewrite the workshop part of the readme

### Note on important changes
- In this version video generation has been added, and is enabled by default for the complete subliminal. It can take a long time especially for subliminals lasting more than 20 minutes, so feel free to disable it if you don't need it by commenting the relevant line in the workshop configuration file.


## [0.14] - 2022-07-27

### Changes
- Support up to 50 layers
- Use /affgroupN to create layer groups
- Update the layer settings inheritance with the new layer groups feature : either inherit from the first layer of the same group, or from the first layer in general
- Update the layer panning with the new layer groups feature : layers in the same group will be panned together
- Replace /affspeedmodeN by /afftempoN so both rate and tempo can be changed for the same layer
- Enable inheritance for /affultrasonicN
- Prevent disabling the autofill (use safe by default)
- Enable r128 normalization by default everywhere
- Allow the workshop to be started using a specific configuration file by drag-and-dropping it over the workshop BAT file
- Display the path to the configuration file being used in the workshop
- Add the WORKSHOP_CONFIG_DIR constant to the workshop configuration file which points to the directory containing the configuration file
- Add a few example workshop configuration files
- Update bal4web binaries to version 1.37
- Add LAME 3.100 binaries with libsndfile support for MP3 encoding and decoding

### Bug fixes
- Fix the unwanted silence added to the start and the end when using a MP3 background or layer
- Fix a warning displayed when a pitch setting is empty
- Fix the normalization of the output ultrasonic subliminal when safe/simple autofills are used

### Note on breaking changes
- In this version /affspeedmodeN has been removed. Instead you can use /afftempoN to specify the tempo setting, and /affspeedN to specify the rate setting. Both can be used at the same time.
- EBU R 128 normalization is now the default everywhere. If you used the default peak normalization before, you'll have to either recalculate the normalization values or use /normwith peak.
- Inheritance has been enabled for /affultrasonicN. Be careful if you use it in the first layer. You can specify /affultrasonicN 0 to avoid automatic inheritance in layers.


## [0.13] - 2022-06-24

### Changes
- Use /normwith peak|rms|r128 to set how audio is normalized (peak is what is used in the old releases and uses peak normalization, rms uses RMS level normalization and r128 uses EBU R 128 loudness normalization)
- Enable r128 normalization by default in the workshop
- Use /showstats to display statistics of the generated audios during the process (duration, peak, RMS and EBU R 128 loudness)
- Enable displaying statistics by default in the workshop
- Reduce the volume of the included background.mp3 file so it doesn't clip
- Change license to GNU General Public License V3 to comply with the dependencies
- Change author name from the alias Chloe Bunny to the actual ChloeB

### Bug fixes
- Fix bugs that prevented the tool to run from directories containing a space in their path

### Note on breaking changes
- In this version the default normalization method in the workshop has been changed to r128, while before it was set to peak (and couldn't be changed). In consequence, you might have to recalculate your normalization values if you want to use the r128 method, or switch back to peak if you don't want to. I recommend using r128, which should give an even loudness for different voices and pitches. Second choice is rms, and peak is there as a last resort.
- The volume of the included background.mp3 has been lowered in order to avoid clipping. If that's a problem you can still grab the old background.mp3 from a previous release and use it instead.


## [0.12] - 2022-05-30

### Changes
- Introducing the autopitch : use /affpitchN auto or /affpitchN auto100 to automatically calculate the pitches so the layers using the same voice do not overlap each other


## [0.11.2] - 2022-05-25

### Changes
- Fix floats precision when doing calculations by using Bc for Windows


## [0.11.1] - 2022-05-18

### Changes
- Significant speed increase when generating subliminals without the silencedetect autofill
- Increase speed by reducing compression when fallbacking to a FLAC file due to WAV size limitations
- Better positioning when using the audacity pan method


## [0.11] - 2022-05-17

### Changes
- Rename /freqduration to /freqpulse
- Support outputing FLAC files
- Fix the generation of longer subliminals (2 hours and more)
- Generate a FLAC file if the audio is too large for the 4gib limit of WAV files
- Use W64 files instead of WAV files internally to allow working with larger files


## [0.10.4] - 2022-05-09

### Changes
- Enable multithreading and a 1MiB buffer in SoX calls (better performance)
- Fix ultrasonic generation for long sounds (use chunks of 10 minutes)
- Delete existing targets when the workshop is started
- Disable ultrasonic subliminal generation by default in workshop
- Update Nyquist to v3.05
- Bug fixes


## [0.10.3] - 2022-05-07

### Changes
- Add the safe autofill method which is fast and does not cut in the middle of an affirmation but miss at most one entire repeat
- Optimize the silencedetect autofill method by basing it on the safe autofill method
- Enable the safe autofill method by default in the workshop


## [0.10.2] - 2022-05-07

### Changes
- Optimize the silencedetect autofill so it can handle longer durations


## [0.10.1] - 2022-05-05

### Changes
- Use /outputfiletestaffs and /outputfiletestaffsnorm to output affirmations test audio files
- Faster background audio processing in case no fade / fadeloop / norm has to be done


## [0.10] - 2022-05-01

### Changes
- Use /pad duration1 duration2 to add some silence before and after the affirmations
- Allow audio files to be passed as affirmations for /affN
- Add the simple autofill method which is fast but can cut in the middle of an affirmation
- Use /autofill simple|silencedetect to switch between the autofill methods
- Force the simple autofill method for affirmations based on an audio file
- Use the value of /autofillduration as the complete duration of the subliminal if available
- Rename /panmethod into /pan for consistency


## [0.9.1] - 2022-04-25

### Changes
- Support up to 9 layers
- Use /autofillduration to set a fixed duration for the autofill function


## [0.9] - 2022-04-24

### Changes
- Add support for ultrasonic subliminals and layers using Nyquist
- Workshop now generates an ultrasonic version of the sub (subliminal-ultrasonic.wav)


## [0.8.1] - 2022-04-22

### Changes
- Allow to use MP3 CBR 320 output files


## [0.8] - 2022-04-21

### Changes
- Add support for online voices from Google, Microsoft and Amazon using Bal4web
- Add /panmethod none, which disables any panning of the layers
- Add list-bal4web-voices.bat helper script


## [0.7] - 2022-04-16

### Changes
- Faster workshop (1 pass instead of 2)
- Add list-voices.bat to list the voices installed on the machine
- Add /outputnobg parameter to export a file with no background
- Add /affspeedmodeN parameter to switch between tempo and rate speed mode
- Fix /autofill so it can handle layers with different speeds
- Bugfix : compare_decimal should now handle negative numbers with decimals and numbers with different decimal lengths


## [0.6] - 2022-04-02

### Changes
Add the /autofill parameter


## [0.5] - 2022-03-28

### Changes
- Support up to 7 layers
- Move from /aff, /affleft, /affright to /aff1, ..., /aff7
- Add the /panmethod parameter with audacity and downmix methods


## [0.4] - 2022-03-22

### Changes
- Isochronic Tones generation
- Repeat audio optimization
- Bug fixes


## [0.3] - 2022-03-19

### Changes
- Frequency generation
- Binaural beats generation
- Delete the work file during the process to free hard drive space
- Bug fixes


## [0.2] - 2022-03-18

### Changes
- Add affirmation volume normalization
- Add background volume normalization
- Move affirmation speed and pitch handling from Balcon to SoX


## [0.1] - 2022-03-16

### Changes
- Initial release
